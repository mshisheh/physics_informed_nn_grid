import torch
import os
import numpy as np
import time

from utils.initialize import initialize
from utils.Data import get_data_spatial_lid_cavity
from utils.network import Network
from utils.encoder import Encoder
from utils.model import Model, Model_multi_net_seq
from utils.losses import Loss_Functions
from utils.trainer import Trainer
from utils.plot import compare_plot, loss_plot, der_plot

#---------------------------------------------
# paramters
# seeds = [12345, 23456, 34567, 45678, 56789, 6789, 7890,\
#           89012, 9012, 2468, 4682, 6824,102345, 203456,\
#               304567, 405678,506789, 60789, 70890,\
#           809012, 90012, 20468, 40682, 60824, 112345,\
#               213456, 314567, 415678, 516789, 61789,\
#                 55213456, 55314567, 55415678, 55516789, 5561789,\
#                     55213456, 55314567, 55415678, 55516789, 5561789,\
#                         6689012, 669012, 662468, 664682, 666824, 66102345, 66203456]

seeds = np.random.randint(100,1000000000,size = 1).tolist()
features = 4
n_cells = 2
res = [3]
domain_size = [-1,1,0,2]
epochs = 5
optimizer = 'lbfgs'
total_loss = []
data_func = get_data_spatial_lid_cavity

#'(10 * torch.sin(y*2) + 10 * x - 12.0) * (x * y * (x-1) * (y-1)) * 15'
#'(2/(5e-2+ x**3) + 2 *x*y/(5e-2+ (x-1)**4) + torch.sin(y*2)  - 25.0) * (x * y * (x-1) * (y-1)) * 15'

for seed in seeds:
    data_params = {'N_f': 50000, 'N_bc':10000, 't': 1, 
                'domain_size':domain_size, 'ntest': 100, 'seed': seed, 'Re':500,\
                    'sol':'(10 * torch.sin(y*2) + 10 * x - 12.0) * (x * y * (x-1) * (y-1)) * 15'}

    #'(10 * torch.sin(y*2) + 10 * x - 12.0) * (x * y * (x-1) * (y-1)) * 15'

    #---------------------------------------------__-------------------------__------------------------__----------------------------

    # initialize
    dtype, device = initialize(seed = data_params['seed'])


    models = []

    for r in res:
        # make network
        if r != 0:
            net = Network(input_dim= len(res) * features, layers=[16,16,16], output_dim=5)
            # make encoder
            encoder = Encoder(n_features= features, res = [r], n_cells= n_cells, domain_size= domain_size)
        else:
            net = Network(input_dim= 2, layers=[32, 32, 32, 32], output_dim=5)
            # make encoder
            encoder = None
        # make model

        models.append(Model(network=net, encoder= encoder))


        model = Model_multi_net_seq(models, device = device, res = res)

        # make loss_functions
        loss_functions = Loss_Functions(model=model, name = 'NS', params=data_params, loss_type='logcosh')
        # make trainer, 
        trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss_derivitives_input, 
                        bc_loss_function = loss_functions.bc_loss_one_p, 
                        epochs=epochs, title='-', data_params = data_params, 
                        data_func = data_func, update_data = True, use_data=True, 
                        optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=False, RAR=False)
        # start fitting:
        trainer()

        total_loss.append(trainer.val_losses['total'][-1]/trainer.val_losses['total'][0])


print(total_loss)
index = total_loss.index(min(total_loss))
seed = seeds[index]
print(f'seed is {seed}')

data_params['seed'] = seed
################################################# Picked the best ################################

title = f"Cavity_MR_{epochs}_{optimizer}_{data_params['N_f']}_{res}_features_{features}_high_grad_Re_{data_params['Re']}_[16,16,16]_Xavier_seed{data_params['seed']}_der_bc_10"
#---------------------------------------------__-------------------------__------------------------__----------------------------

optimizer = 'lbfgs'


# initialize
dtype, device = initialize(seed = data_params['seed'])


models = []
epochs = 300
for r in res:
    # make network
    if r != 0:
        net = Network(input_dim= len(res) * features, layers=[16,16,16], output_dim=5)
        # make encoder
        encoder = Encoder(n_features= features, res = [r], n_cells= n_cells, domain_size= domain_size)
    else:
        net = Network(input_dim= 2, layers=[32, 32, 32, 32], output_dim=5)
        # make encoder
        encoder = None
    # make model

    models.append(Model(network=net, encoder= encoder))


    model = Model_multi_net_seq(models, device = device, res = res)

    # make loss_functions
    loss_functions = Loss_Functions(model=model, name = 'NS', params=data_params, loss_type='logcosh')
    # make trainer, 
    trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss_derivitives_input, 
                    bc_loss_function = loss_functions.bc_loss_one_p, 
                    epochs=epochs, title=title, data_params = data_params, 
                    data_func = data_func, update_data = True, use_data=False, 
                    optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=False, RAR=False)
    # start fitting:
    trainer()


# testing
# Generate data
x, y, u_test = \
    data_func(params=data_params, train=False)

u_predict = model(x, y, test = True)


#plotting
compare_plot(x, y, u_test, u_predict, title=title)
time.sleep(3.0)
loss_plot(trainer.train_losses, trainer.val_losses, title=title)

#der_plot(x, y , model, title=title)