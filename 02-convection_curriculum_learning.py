import torch
import os
import numpy as np
import time

from utils.initialize import initialize
from utils.Data import get_convection
from utils.network import Network , AE
from utils.encoder0 import Encoder
from utils.model import Model , Model_ae
from utils.losses import Loss_Functions
from utils.trainer3 import Trainer
from utils.plot import compare_plot_h, loss_plot, der_plot, grad_hist_plot , f_plot , w_plot

#import inspect
#import utils.Data

from datetime import date

today = date.today()



# Textual month, day and year	
d2 = today.strftime("%B%d")


#---------------------------------------------
# paramters
seed = 456
features = 4
n_cells = 3#[11, 3]
res = [7 , 7 , 7]#[2, 3, 4, 5, 7, 9, 11 , 13 , 15 , 17, 19 , 21]
pde_name = 'convection' # or 
network_name = 'MR' # or MR
domain_size = [0.,2*np.pi,0.,1.]

#betas = [0.,5.,10.,15.,20.,25.,30.]
betas = [0. , 2.5 , 5. , 7.5 , 10. , 12.5 , 15. , 17.5 , 20. , 22.5 , 25. , 27.5 , 30.]
data_params = []
for beta in betas:
    d_ = {'N_f': 10000, 'N_bc':10000, 
               'domain_size':domain_size,'ntest': 100, 'seed': seed, 
               'Neuman_bc': False,  'beta':beta , 'nu':0.}
    data_params.append(d_)
data_params1 = {'N_f': 10000, 'N_bc':10000, 
               'domain_size':domain_size,'ntest': 100, 'seed': seed, 
               'Neuman_bc': False,  'beta':10.0 , 'nu':0.}
data_params2 = {'N_f': 10000, 'N_bc':10000, 
               'domain_size':domain_size,'ntest': 100, 'seed': seed, 
               'Neuman_bc': False,  'beta':20.0 , 'nu':0.}
data_params3 = {'N_f': 10000, 'N_bc':10000, 
               'domain_size':domain_size,'ntest': 100, 'seed': seed, 
               'Neuman_bc': False,  'beta':30.0 , 'nu':0.}

epoch_test = 200
epochs = 200

final_res = [7]
optimizer_test = 'adam'
optimizer = 'lbfgs'
dynamic_weight_flag = False
hbc_flag = False
evo = False

ae = False
#------------------------------------------------------------------------------
if network_name == 'PINN':
    title = "1"#f"{d2}_{pde_name}_{network_name}_{epochs}_{optimizer}_{data_params3['N_f']}_dw_{dynamic_weight_flag}"
else:
    title = f"{d2}_{pde_name}_{network_name}_{epochs}_{optimizer}_{data_params[-1]['N_f']}_{res}_features_{features}_cells_{n_cells}_dw_{dynamic_weight_flag}_ae{ae}_evo{evo}_beta{data_params[-1]['beta']}_curriculum"
data_func = get_convection
#------------------------------------------------------------------------------

# initialize
dtype, device = initialize(seed = data_params3['seed'])




if ae:

    print("error")

else:

    encoder = Encoder(n_features= features, res = final_res, n_cells= n_cells, domain_size= domain_size , mode = 'cosine')
    network = Network(input_dim= len(final_res)*features , layers=[16, 16 , 16], output_dim=1)

    for idx , beta in enumerate(betas):
        model = Model(encoder=encoder , network=network , data_params=data_params[idx] , hbc = False)
        loss_functions = Loss_Functions(model=model, name = pde_name, params = data_params[idx], loss_type='mse' , ae=ae)
        trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                bc_loss_function = loss_functions.ic_bc, 
                epochs=epochs, title=title, data_params = data_params[idx], 
                data_func = data_func, update_data = False, use_data=False, 
                optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False , evo = evo , active_res= final_res)
        trainer()




"""
    model = Model(encoder=encoder , network=network , data_params=data_params1 , hbc = False)
    # make loss_functions
    loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params1, loss_type='mse' , ae=ae)
    trainer1 = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                bc_loss_function = loss_functions.ic_bc, 
                epochs=epochs, title=title, data_params = data_params1, 
                data_func = data_func, update_data = False, use_data=False, 
                optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False , evo = evo , active_res= final_res)
    
    trainer1()
    model = Model(encoder=encoder , network=network , data_params=data_params2 , hbc = False)
    # make loss_functions
    loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params2, loss_type='mse' , ae=ae)
    trainer2 = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                bc_loss_function = loss_functions.ic_bc, 
                epochs=epochs, title=title, data_params = data_params2, 
                data_func = data_func, update_data = False, use_data=False, 
                optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False , evo = evo , active_res= final_res)
    
    trainer2()
    model = Model(encoder=encoder , network=network , data_params=data_params3 , hbc = False)
    # make loss_functions
    loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params3, loss_type='mse' , ae=ae)
    trainer3 = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                bc_loss_function = loss_functions.ic_bc, 
                epochs=epochs, title=title, data_params = data_params3, 
                data_func = data_func, update_data = False, use_data=False, 
                optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False , evo = evo , active_res= final_res)
    
    trainer3()
"""
print(f"title after train:  {title}")


# testing
# Generate data
x, y, u_test = \
    data_func(params=data_params[-1], train=False)
print(x.shape , y.shape , u_test.shape)

if ae:
    if hbc_flag:
     _ ,  _ , u_predict  = model(x, y)
    else:  u_predict ,  _ , _  = model(x, y)
else:
    if hbc_flag:
        _ , u_predict = model(x, y)
    else:
        u_predict , _ = model(x, y)





#plotting
compare_plot_h(x, y, u_test, u_predict, title=title)
time.sleep(3.0)
loss_plot(trainer.train_losses, trainer.val_losses, title=title+"1")
#loss_plot(trainer2.train_losses, trainer2.val_losses, title=title+"2")
#loss_plot(trainer3.train_losses, trainer3.val_losses, title=title+"3")



w_plot(trainer.w,title=title)
time.sleep(3.0)
#grad_hist_plot(trainer.gradient_loss_total, title = title)

f_plot(encoder.F_active , title = title)



#der_plot(x, y , model, title=title)