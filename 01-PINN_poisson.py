import torch
import os
import numpy as np
import time

from utils.initialize import initialize
from utils.Data import get_Poisson_analytical
from utils.network import Network
from utils.encoder import Encoder
from utils.model import Model
from utils.losses import Loss_Functions
from utils.trainer import Trainer
from utils.plot import compare_plot_u, loss_plot, der_plot

#---------------------------------------------
# paramters
seed = 456
features = 4
n_cells = [11, 3]
res = [3, 11]
pde_name = 'PoissonA' # or 
network = 'PINN' # or MR
domain_size = [0,1,0,1]
data_params = {'N_f': 50000, 'N_bc':10000, 'nu':0.002, 
               'domain_size':domain_size,'ntest': 100, 'seed': seed, 
               'Neuman_bc': True, 'a1':1.0, 'a2': 4.0, 'k':1.0}
epochs = 100
optimizer = 'lbfgs'
if network == 'PINN':
    title = f"{pde_name}_{network}_{epochs}_{optimizer}_{data_params['N_f']}"
else:
    title = f"{pde_name}_{network}_{epochs}_{optimizer}_{data_params['N_f']}_{res}_features_{features}_cells_{n_cells}_data"
data_func = get_Poisson_analytical
#---------------------------------------------

# initialize
dtype, device = initialize(seed = data_params['seed'])

# make network
net = Network(input_dim= 2, layers=[50, 50, 50 , 50], output_dim=1)
# make encoder
encoder = None
# make model
model = Model(network=net, encoder= encoder)
# make loss_functions
loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse')
# make trainer, 
trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                  bc_loss_function = loss_functions.bc_loss, 
                  epochs=epochs, title=title, data_params = data_params, 
                  data_func = data_func, update_data = True, use_data=False, 
                  optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=False, RAR=False)
# start fitting:
trainer()

# testing
# Generate data
x, y, u_test = \
    data_func(params=data_params, train=False)

u_predict = model(x, y)


#plotting
compare_plot_u(x, y, u_test, u_predict, title=title)
time.sleep(3.0)
loss_plot(trainer.train_losses, trainer.val_losses, title=title)

#der_plot(x, y , model, title=title)