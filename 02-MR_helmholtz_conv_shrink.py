import torch
import os
import numpy as np
import time

import sys
sys.path.append('.')

from utils.initialize import initialize
from utils.Data import get_Helmholtz
from utils.network import Network
from utils.encoder import Encoder, Encoder_conv, Encoder_conv_shirink_kernel
from utils.model import Model , Model_multi_net_seq
from utils.losses import Loss_Functions
from utils.trainer import Trainer
from utils.plot import compare_plot_u, loss_plot, der_plot, grad_hist_plot , grad_hist_plot_log

import inspect
import utils.Data

from datetime import date

today = date.today()



# Textual month, day and year	
d2 = today.strftime("%B%d")

print(d2)

#---------------------------------------------
# paramters
seed = 456
features = 32
n_cells = [2,2,2]
res = [128]
pde_name = 'Helmholtz' # or 
network = 'MR' # or MR
domain_size = [-1,1,-1,1]
data_params = {'N_f': 10000, 'N_bc':10000, 'nu':0.002, 
               'domain_size':domain_size,'ntest': 100, 'seed': seed, 
               'Neuman_bc': False, 'a1':1.0, 'a2': 8.0, 'k':1.0}
epochs = 10000
optimizer = 'adam'
dynamic_weight_flag = True
mode = 'cosine'
q = 3
#------------------------------------------------------------------------------
if network == 'PINN':
    title = f"{d2}_{pde_name}_{network}_{epochs}_{optimizer}_{data_params['N_f']}_a2_{data_params['a2']}_dw_{dynamic_weight_flag}"
else:
    title = f"{d2}_conv_shrinking_[6,4,2]_{pde_name}_{network}_{epochs}_{optimizer}_{data_params['N_f']}_{res}_features_{features}_cells_{n_cells}_a2_{data_params['a2']}_dw_{dynamic_weight_flag}_check"
data_func = get_Helmholtz
#------------------------------------------------------------------------------

dtype, device = initialize(seed = data_params['seed'])

cascade = False
freeze = False

if cascade:
    models = []
    #f_map = 0
    for  idx , r in enumerate(res):
        # make network
        net = Network(input_dim= 1 * features, layers=[16, 16], output_dim=1)
        # make encoder
        encoder = Encoder(n_features= features, res = [r], n_cells= n_cells, domain_size= domain_size , mode = mode)
        # make model

        models.append(Model(network=net, encoder= encoder))


        model = Model_multi_net_seq(models, device = device)

        # make loss_functions
        loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse')
        # make trainer, 
        trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                        bc_loss_function = loss_functions.bc_loss, 
                        epochs=epochs, title=title, data_params = data_params, 
                        data_func = data_func, update_data = True, use_data=False, 
                        optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=False, RAR=False)
        # start fitting:
        trainer()




else:
# make network

    # make encoder
    encoder = Encoder_conv_shirink_kernel(n_features= features, res = res, n_cells= n_cells, domain_size= domain_size , mode = mode)

    #make network and model

    if 0 in res:
        net = Network(input_dim= 2, output_dim=1, layers=[32,32,32,32,32])
        model = Model(network=net, encoder= None , data_params= data_params)
    else:
        net = Network(input_dim= len(res)*features, layers=[32, 32], output_dim=1) 
        model = Model(network=net, encoder= encoder)

                    

        print(model)

    #print(model)
    # make loss_functions
    loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse')
    # make trainer, 
    trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                    bc_loss_function = loss_functions.bc_loss, 
                    epochs=epochs, title=title, data_params = data_params, 
                    data_func = data_func, update_data = False, use_data=False, 
                    optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False)
    # start fitting:
    trainer()

"""

# initialize
dtype, device = initialize(seed = data_params['seed'])

# make network
net = Network(input_dim= len(res) * features, layers=[16, 16], output_dim=1)
# make encoder
encoder = Encoder(n_features= features, res = res, n_cells= n_cells, domain_size= domain_size)
# make model
model = Model(network=net, encoder= encoder)
# make loss_functions
loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse')
# make trainer, 
trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                  bc_loss_function = loss_functions.bc_loss, 
                  epochs=epochs, title=title, data_params = data_params, 
                  data_func = data_func, update_data = True, use_data=False, 
                  optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False)
# start fitting:
trainer()

"""

# testing
# Generate data
x, y, u_test = \
    data_func(params=data_params, train=False)

u_predict = model(x, y)


#plotting

compare_plot_u(x, y, u_test, u_predict, title=title)
time.sleep(3.0)
loss_plot(trainer.train_losses, trainer.val_losses, title=title)
time.sleep(3.0)
"""
#grad_hist_plot(trainer.gradient_loss_total, title = title + '_total')
if encoder is not None:
    grad_hist_plot(trainer.gradient_loss_network, title = title + '_network')
    grad_hist_plot(trainer.gradient_loss_encoder, title = title + '_encoder')
    grad_hist_plot_log(trainer.gradient_loss_encoder, title = title + '_encoder')
    grad_hist_plot_log(trainer.gradient_loss_network, title = title + '_network')


"""

#der_plot(x, y , model, title=title)