import torch
import torch.nn as nn
#from HBC import apply_hbc
from utils.HBC import apply_hbc, apply_neural_bc

class Model(nn.Module):
    
    def __init__(self, network, encoder = None ) -> None:
        super().__init__()
        self.pinns = True
        self.network = network
        self.encoder = encoder
        
        # self.drop = nn.Dropout(p = 0.1)
        if encoder is not None:
            self.encoder = encoder
            self.pinns = False

        
    def forward(self, x, y):
        if self.pinns:
            return self.network(torch.cat([x, y], dim=-1))
        else:

            out = self.encoder(x, y)
            #out = nn.functional.tanh(out)
            #out = (out - out.min())/(out.max() - out.min())
            #out = self.drop(out)
            #out = torch.cat([x,y, out], dim = -1)
            if self.network is not None:
                out = self.network(out)
            return out

class Model_combo(nn.Module):
    
    def __init__(self, model1, model2):
        super().__init__()
        self.model1 = model1
        self.model2 = model2

    def forward(self, x, y):
        return self.model1(x, y) + self.model2(x, y)



class Model_multi_net(nn.Module):
    
    def __init__(self, model_list, device):
        super().__init__()
        self.model_list = nn.ModuleList(model_list)
        self.device = device
        self.linear = nn.Linear(len(self.model_list), 1, bias=False, device=device)

    def forward(self, x, y, test = False):
        out = []
        for m in self.model_list:
            out.append(m(x,y))
        out = torch.cat(out, -1).to(self.device)
        if test:
            print(out)
        out = torch.tensor([[1.0/3.0, 1.0/7.0]]).to(self.device) * out #self.linear(out)
        return torch.sum(out, dim =-1).reshape(-1,1)



class Model_multi_net_seq(nn.Module):
    
    def __init__(self, model_list, device, res):
        super().__init__()
        self.model_list = nn.ModuleList(model_list)
        self.device = device
        self.linear = nn.Linear(len(self.model_list), 1, bias=False, device=device)
        self.res = res
        ######-------------------------######
        for model in self.model_list[:-1]:
            for param in model.parameters():
                param.requires_grad = False

    def forward(self, x, y, test = False):
        out = self.model_list[0](x,y)/(self.res[0]+1)
        for i in range(1,len(self.model_list)):
            out += self.model_list[i](x,y)/(self.res[i]+1)
        if test:
            print(out)

        return out




class Model_pass_xy(nn.Module):
    
    def __init__(self, network, encoder = None) -> None:
        super().__init__()
        self.pinns = True
        self.network = network
        if encoder is not None:
            self.encoder = encoder
            self.pinns = False
        
    def forward(self, x, y):
        if self.pinns:
            return self.network(torch.cat([x, y], dim=-1))
        else:
            out = self.encoder(x, y)
            out = torch.cat([x,y, out], dim = -1)
            out = self.network(out)
            return out




class Model2(nn.Module):
    
    def __init__(self, network, encoder = None) -> None:
        super().__init__()
        self.pinns = True
        self.network = network
        if encoder is not None:
            self.encoder = encoder
            self.pinns = False
        
    def forward(self, x, y):
        if self.pinns:
            return self.network(torch.cat([x, y], dim=-1))
        else:
            out = self.encoder(x, y)
            out1 = (self.network[0])(out)
            out2 = (self.network[1])(out)
            out = torch.cat([out1, out2], dim = -1)
            return out

class Model3(nn.Module):
    
    def __init__(self, network, encoder = None) -> None:
        super().__init__()
        self.pinns = True
        self.network = network
        if encoder is not None:
            self.encoder = encoder
            self.pinns = False
        
    def forward(self, x, y):
        if self.pinns:
            return self.network(torch.cat([x, y], dim=-1))
        else:
            out1 = (self.encoder[0])(x, y)
            out1 = (self.network[0])(out1)
            out2 = (self.encoder[1])(x, y)
            out2 = (self.network[1])(out2)
            out = torch.cat([out1, out2], dim = -1)
            return out





class Model_hbc(nn.Module):
    
    def __init__(self, network, encoder = None, enable_HBC = False , data_params=None) -> None:
        super().__init__()
        self.pinns = True
        self.network = network
        self.enable_HBC = enable_HBC
        self.data_params = data_params
        if encoder is not None:
            self.encoder = encoder
            self.pinns = False
        
    def forward(self, x, y):
        #print(f'x shape {x.shape}')
        if self.pinns:
            return self.network(torch.cat([x, y], dim=-1))
        else:
            f = self.encoder(x, y)
            u = self.network(f)

            if self.enable_HBC:
                out = apply_hbc(x,y,u,self.data_params)
            else:
                out = u

            return out
        

class Model_neural_hbc(nn.Module):
    
    def __init__(self, network, encoder = None, model_bc = None, model_g = None, enable_HBC = False , 
                 data_params={'N_f': 50000, 'N_bc':50000, 't':1.0, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 100, 'seed': 1234}) -> None:
        super().__init__()
        self.pinns = True
        self.network = network
        self.enable_HBC = enable_HBC
        self.data_params = data_params
        self.model_bc = model_bc
        self.model_g = model_g
        if encoder is not None:
            self.encoder = encoder
            self.pinns = False
        
    def forward(self, x, y):
        #print(f'x shape {x.shape}')
        if self.pinns:
            upde = self.network(torch.cat([x, y], dim=-1))
            return apply_neural_bc(x, y, model_bc = self.model_bc, model_g = self.model_g, u = upde, data_params=self.data_params)
            
        
        else:
            f = self.encoder(x, y)
            upde = self.network(f)


            return apply_neural_bc(x, y, model_bc = self.model_bc, model_g = self.model_g, u = upde, data_params=self.data_params)
 