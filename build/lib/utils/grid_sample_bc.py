
import torch
''' see the Section "Mesh-agnostic representations through interpolation" in the paper.
            below codes are about cosine/linear interpolation in 2d and 3d case. '''
def grid_sample_2d(input, grid, step='cosine', offset=True , shift_mode ='/' , shift = 1/4):
    '''
    Args:
        input : A torch.Tensor of dimension (N, C, IH, IW).     (M , f , res ,res)
        grid: A torch.Tensor of dimension (N, H, W, 2).         (M , f , N , 2)
    Return:
        torch.Tensor: The bilinearly interpolated values (N, H, W, 2).
    '''

    N, C, IH, IW = input.shape
    _, H, W, _ = grid.shape
    """

    x_min , x_max = 0. , 1.
    y_min , y_max = 0. , 1.
    top = torch.linspace(x_min , x_max, IH , device =  'cuda' ,  requires_grad= False)

    top_f = (15*(top-1)*top*(4*y_max*torch.sin(2*torch.tensor(y_max))+(8*top*y_max**2)/(top**4-4*top**3+6*top**2-4*top+21/20)+(8*y_max)/(top**3+1/20)-100*y_max))/4
    top_f = top_f.view(1,1,1,IW).repeat(N , C , 1 , 1) #(3,4,1,11)
    top_f.requires_grad = False
    #print(top_f.shape)

    #print(f"top_f: {top_f}")
    
    #beta = input.clone()
    #beta[:,0,0 , :] = 0.
    #beta[:,0,:,0] = 0.
    rest = input[:,:,:-1,:] #(3,4, 10 ,11)
    #print(f"rest shape: {rest.shape}")
    #beta[:,0,-1 , :] = top_f



    #beta[:,0 , : , -1] = 0.

    input = torch.cat([top_f ,  rest ] , 2)


    
    #input[:,0,-1 , :] = input[:,0,-1 , :].detach()


    #input = torch.cat()

    """



    if step=='bilinear':
        step_f = lambda x: x
    elif step=='cosine':
        step_f = lambda x: 0.5*(1-torch.cos(torch.pi*x))
    else:
        raise NotImplementedError

    ''' (iy,ix) will be the indices of the input
            1. normalize coordinates 0 to 1 (from -1 to 1)
            2. scaling to input size
            3. adding offset to make non-zero derivative interpolation '''
    ix = grid[..., 0]
    iy = grid[..., 1]
    if offset:

        if shift_mode == '/':

          N=2

          oy = ((iy[0:1,...]+1)/2)*(IH-1)
          ox = ((ix[0:1,...]+1)/2)*(IW-1) #(1 , f, N)

          #offset = torch.linspace(0,1-(1/N),N).reshape(N,1,1).to('cuda')
          offset = torch.Tensor([ shift , -shift]).reshape(N,1,1).to('cuda')
          iy = ((iy[0:2,...]+1)/2)*(IH-2) + offset
          ix = ((ix[0:2,...]+1)/2)*(IW-2) + offset

          ix = torch.cat([ox , ix] , 0)
          iy = torch.cat([oy , iy] , 0)

          
          N = N+1


          


        if shift_mode == 'h':

          offset_x = torch.Tensor([0., shift , -shift]).reshape(N,1,1).to('cuda')
          offset_y = torch.zeros_like(offset_x)


          iy = ((iy+1)/2)*(IH-1) + offset_y #from 0 to 1, and then at which cell i lands
          ix = ((ix+1)/2)*(IW-1) + offset_x

        if shift_mode == 'v':

          offset_y = torch.Tensor([0., shift , -shift]).reshape(N,1,1).to('cuda')
          offset_x = torch.zeros_like(offset_y)


          iy = ((iy+1)/2)*(IH-1) + offset_y #from 0 to 1, and then at which cell i lands
          ix = ((ix+1)/2)*(IW-1) + offset_x

        if shift_mode == '+':

          offset_x = torch.Tensor([0., shift , -shift, 0., 0.]).reshape(N,1,1).to('cuda')
          offset_y = torch.Tensor([0., 0., 0., shift , -shift]).reshape(N,1,1).to('cuda')


          iy = ((iy+1)/2)*(IH-1) + offset_y #from 0 to 1, and then at which cell i lands
          ix = ((ix+1)/2)*(IW-1) + offset_x

        if shift_mode == 'x':

          offset_x = torch.Tensor([0., shift , -shift, -shift , shift]).reshape(N,1,1).to('cuda')
          offset_y = torch.Tensor([0., shift , -shift, shift , -shift]).reshape(N,1,1).to('cuda')


          iy = ((iy+1)/2)*(IH-1) + offset_y #from 0 to 1, and then at which cell i lands
          ix = ((ix+1)/2)*(IW-1) + offset_x

        if shift_mode == '@':

          offset_x = torch.Tensor([0., 0.011048934589638494 , -0.1344782430931685, -0.09620156150300525 , 0.22886100463605757 , 0.2497604087132912 , -0.2496044884401818]).reshape(N,1,1).to('cuda')
          offset_y = torch.Tensor([0., 0.06982411990025791 , 0.043652592548684824, -0.18900433763035399 , -0.1660795814167464 , 0.2501137843555655 , 0.3429389445153999]).reshape(N,1,1).to('cuda')


          iy = ((iy+1)/2)*(IH-1) + offset_y #from 0 to 1, and then at which cell i lands
          ix = ((ix+1)/2)*(IW-1) + offset_x



    else:
        iy = ((iy+1)/2)*(IH-1) #from 0 to 1, and then at which cell i lands
        ix = ((ix+1)/2)*(IW-1)
    
    # compute corner indices
    with torch.no_grad():
        ix_left = torch.floor(ix)
        ix_right = ix_left + 1
        iy_top = torch.floor(iy)
        iy_bottom = iy_top + 1

    # compute weights
    dx_right = step_f(ix_right-ix)  #(n_grid , 1 , batch_size)
    dx_left = 1 - dx_right
    dy_bottom = step_f(iy_bottom-iy)
    dy_top = 1 - dy_bottom


    nw = dx_right*dy_bottom
    ne = dx_left*dy_bottom
    sw = dx_right*dy_top
    se = dx_left*dy_top


    SW_top = (iy) * (dx_right)
    SE_top = (iy) * (dx_left)
    NW_top = nw
    NE_top = ne


    SW_bottom = sw
    SE_bottom = se
    NW_bottom = ((IH-1) - iy) * (dx_right)
    NE_bottom = ((IH-1) - iy) * (dx_left)

    SW_right = ((IW-1) - ix) *dy_top
    SE_right = se
    NW_right = ((IW-1) - ix) *dy_bottom
    NE_right = ne

    SW_left = sw
    SE_left = (ix) * dy_top
    NW_left = nw
    NE_left = (ix) * dy_bottom




    # sanity checking
    with torch.no_grad():
        torch.clamp(ix_left, 0, IW-1, out=ix_left)  #(n_grid , 1 , batch_size)
        torch.clamp(ix_right, 0, IW-1, out=ix_right)
        torch.clamp(iy_top, 0, IH-1, out=iy_top)
        torch.clamp(iy_bottom, 0, IH-1, out=iy_bottom)
        
        
    """

    dx_right_b  =  (IW-1) - ix_right
    dx_left_b = ix_left
    dy_top_b = (IH-1) - iy_top
    dy_bottom_b = iy_bottom

    """

    #print(dx_right_b[0,0 ,0] , dx_left_b[0,0 ,0] ,dy_top_b[0,0 ,0] , dy_bottom_b[0,0 ,0] )


    #right_close = (dx_right_b <= dx_left_b) | (dx_right_b <= dy_top_b) | (dx_right_b <= dy_bottom_b)


    
    

    # look up values
    input = input.view(N, C, IH*IW)
    nw_val = torch.gather(input, 2, (iy_top * IW + ix_left).long().view(N, 1, H*W).repeat(1, C, 1))  #(n_grid , n_grid , batch_size)
    ne_val = torch.gather(input, 2, (iy_top * IW + ix_right).long().view(N, 1, H*W).repeat(1, C, 1))
    sw_val = torch.gather(input, 2, (iy_bottom * IW + ix_left).long().view(N, 1, H*W).repeat(1, C, 1))
    se_val = torch.gather(input, 2, (iy_bottom * IW + ix_right).long().view(N, 1, H*W).repeat(1, C, 1))


    #extension to boundary features
    nw_bc = torch.gather(input, 2, ((IH-1) * IW + ix_left).long().view(N, 1, H*W).repeat(1, C, 1))
    ne_bc = torch.gather(input, 2, ((IH-1) * IW + ix_right).long().view(N, 1, H*W).repeat(1, C, 1))
    sw_bc = torch.gather(input, 2, ((0.) * IW + ix_left).long().view(N, 1, H*W).repeat(1, C, 1))
    se_bc = torch.gather(input, 2, ((0.) * IW + ix_right).long().view(N, 1, H*W).repeat(1, C, 1))



    out_val_top = (nw_bc.view(N, C, H, W) * NW_top.view(N, 1, H, W) + 
                   ne_bc.view(N, C, H, W) * NE_top.view(N, 1, H, W) + 
                   sw_val.view(N, C, H, W) * SW_top.view(N, 1, H, W) +
                   se_val.view(N, C, H, W) * SE_top.view(N, 1, H, W))
    
    
    out_val_bottom = (nw_val.view(N, C, H, W) * NW_bottom.view(N, 1, H, W) + 
                     ne_val.view(N, C, H, W) * NE_bottom.view(N, 1, H, W) + 
                     sw_bc.view(N, C, H, W) * SW_bottom.view(N, 1, H, W) +
                     se_bc.view(N, C, H, W) * SE_bottom.view(N, 1, H, W))
    
    out_val_right = (nw_val.view(N, C, H, W) * NW_right.view(N, 1, H, W) + 
                    ne_bc.view(N, C, H, W) * NE_right.view(N, 1, H, W) + 
                    sw_val.view(N, C, H, W) * SW_right.view(N, 1, H, W) +
                    se_bc.view(N, C, H, W) * SE_right.view(N, 1, H, W))
    
    out_val_left = (nw_bc.view(N, C, H, W) * NW_left.view(N, 1, H, W) + 
                    ne_val.view(N, C, H, W) * NE_left.view(N, 1, H, W) + 
                    sw_bc.view(N, C, H, W) * SW_left.view(N, 1, H, W) +
                    se_val.view(N, C, H, W) * SE_left.view(N, 1, H, W))

    e = 1e-4
    area_top =  1.# 1/(iy_bottom.unsqueeze(1) + e) #(M , 1 , N)
    area_right = 1.#ix_left.unsqueeze(1) + e
    area_left =  1.#ix_right.unsqueeze(1) + e
    area_bottom = 1.#iy_top.unsqueeze(1)  + e 
    

    #print(area_top)

   # w_top , w_right , w_left , w_bottom =  torch.zeros(iy_bottom.unsqueeze(1)) ,   torch.zeros(iy_bottom.unsqueeze(1)) , torch.zeros(iy_bottom.unsqueeze(1)) , torch.zeros(iy_bottom.unsqueeze(1))




    # 2d_cosine/bilinear interpolation
    out_val_main = (nw_val.view(N, C, H, W) * ((nw.view(N, 1, H, W))) + 
               ne_val.view(N, C, H, W) * ((ne.view(N, 1, H, W))) +
               sw_val.view(N, C, H, W) * ((sw.view(N, 1, H, W))) +
               se_val.view(N, C, H, W) * ((se.view(N, 1, H, W))))
    
    #(n_grid , n_f , 1 , batch_size)
    #print(out_val_main.shape , out_val_top.shape , area_top.shape)
    gamma =  0.


    out_val = out_val_main +  0.005 *(area_top* out_val_top  \
        + (area_top) * out_val_top + (1/area_bottom) * out_val_bottom +  (1/area_left) * out_val_left + (1/area_right) * out_val_right) * gamma

    return out_val # dx_right , dy_bottom , sw_val , se_val, ne_val, nw_val  #out_val #