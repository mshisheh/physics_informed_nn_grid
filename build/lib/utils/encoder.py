import torch
import torch.nn as nn
#from cosine_sampler_2d import CosineSampler2d
from utils.grid_sample_bc import grid_sample_2d


dtype = torch.float32
device = torch.device("cuda")

class EncoderShirin (nn.Module):

    def __init__(self, n_features = 4, res = [8, 12, 20, 18, 32], n_cells = 2, domain_size = [0, 1, 0, 1] , mode = 'bilinear') -> None:
        super().__init__()
        self.mode = mode
        self.n_features = n_features 
        self.res = res
        self.n_cells = n_cells



        a = [torch.rand(size=(self.n_cells, self.n_features, self.res[i], self.res[i]),
             dtype=dtype).to(device).data.uniform_(-1e-5,1e-5) for i in range(len(self.res))]
        

        #b = [torch.empty(size = (self.n_cells[0], self.n_features, self.res[0], self.res[0]), dtype=dtype, device=device) ]
             
        self.F_active = nn.ParameterList(a)
        self.Ws = nn.ParameterList([torch.tensor([10.]).to(device) , torch.tensor([10.]).to(device)])
        self.domain_size = domain_size

    def forward(self, x, y):
        features  = []
        x_min, x_max, y_min, y_max = self.domain_size
        x = (x - x_min)/(x_max - x_min)
        y = (y - y_min)/(y_max - y_min)
        x = x*2-1
        y = y*2-1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)
        x = x.repeat([self.n_cells,1,1,1])

        for idx , alpha in enumerate(self.F_active):
            #print(alpha.shape)
            #alpha: [3 , 4, 3 , 3]
            """
            beta = alpha.clone()
            beta[:,0,0 , :] = 0
            beta[:,0,:,0] = 0
            beta[:,0,-1 , :] = 1.
            beta[:,0 , : , -1] = 0.

            beta[:,0,0 , :].detach()
            beta[:,0,:,0].detach()
            beta[:,0,-1 , :].detach()
            beta[:,0 , : , -1].detach()



            beta[:,1,0 , :] = 0
            beta[:,1,:,0] = 0
            beta[:,1,-1 , :] = 0
            beta[:,1 , : , -1] = 0

            beta[:,1,0 , :].detach()
            beta[:,1,:,0].detach()
            beta[:,1,-1 , :].detach()
            beta[:,1 , : , -1].detach()
            """

            
            #print(beta)

            beta = alpha


            #print(beta)

            F = grid_sample_2d(beta, x, step=self.mode, offset=True)
            #F =  grid_sample_2d(beta, x, step=self.mode, offset=True , shift_mode='/' , shift= 1/3)
            #F = grid_sample_2d(alpha, x, step=self.mode, offset=True , shift_mode='+' , shift= 1/3)
            #F = CosineSampler2d.apply(alpha, x, 'zeros', True, 'cosine', True)

            features.append(F.sum(0).view(self.n_features,-1).t())

            #when shifting turned off:
            #features.append(F[0,...].view(self.n_features,-1).t())
        
        F = torch.cat(tuple(features) , 1)
        return F

import torch
import torch.nn as nn
# from cosine_sampler_2d import CosineSampler2d
from utils.grid_sample import grid_sample_2d

dtype = torch.float32
device = torch.device("cuda:0")


def Gaussian_average0(alpha):
    seg = alpha[0,0,0,:].shape[-1]
    meshes = torch.meshgrid(torch.linspace(0,1, seg), torch.linspace(0,1, seg))
    weight = torch.zeros((seg,seg)).to(device)
    alpha_new = torch.zeros_like(alpha)
    sigma = 1.0/(2*seg)
    for k in range(seg):
        for l in range(seg):
            for i in range(seg):
                for j in range(seg):
                    weight[i,j] = torch.exp(-0.5 * (torch.sqrt((meshes[0][i,j] - meshes[0][k,l])**2.0 + 
                                                                (meshes[1][i,j] - meshes[1][k,l])**2.0)/sigma)**2.0)/(sigma * (2 * torch.pi)**0.5)
            weight/=torch.sum(weight)
            weight_expand = weight.repeat(2,4,1,1)
            alpha_new[...,k,l] = torch.sum(weight_expand * alpha, dim = (2,3))

    return alpha_new


def Gaussian_average(alpha):
    seg = alpha.shape[-1]
    meshes = torch.meshgrid(torch.linspace(0,1, seg), torch.linspace(0,1, seg))
    weight = torch.zeros((seg,seg)).to(device)
    alpha_new = torch.zeros_like(alpha).to(device)
    sigma = 0.1

    for k in range(seg):
        for l in range(seg):
            dist = torch.sqrt((meshes[0] - meshes[0][k,l])**2.0 + (meshes[1] - meshes[1][k,l])**2.0)
            weight = torch.exp(-0.5 * (dist / sigma)**2.0) / (sigma * (2 * torch.pi)**0.5)
            weight /= torch.sum(weight)
            weight_expand = weight.repeat(alpha.shape[0], alpha.shape[1], 1, 1).to(device)
            alpha_new[...,k,l] = torch.sum(weight_expand * alpha, dim=(2,3))

    return alpha_new





class Encoder (nn.Module):

    def __init__(self, n_features = 4, res = [8, 12, 20, 18, 32], n_cells = 2, domain_size = [0, 1, 0, 1],  mode = 'bilinear') -> None:
        super().__init__()
        self.n_features = n_features 
        self.mode = mode
        self.res = res
        self.n_cells = n_cells

        
        self.F_active = nn.ParameterList([torch.rand(size=(self.n_cells[i], self.n_features, self.res[i], self.res[i]),
             dtype=dtype , device = device).data.uniform_(-1e-5,1e-5) for i in range(len(self.res))])

        # self.F_active = nn.ParameterList([torch.rand(size=(self.n_cells, self.n_features, self.res[i], self.res[i]),
        #      dtype=dtype , device = device) for i in range(len(self.res))])

        # for f in self.F_active:
        #     nn.init.xavier_normal_(f)

        self.domain_size = domain_size
        self.active = nn.SiLU()


    def forward(self, x, y):
        features  = []
        x_min, x_max, y_min, y_max = self.domain_size
        x = (x - x_min)/(x_max - x_min)
        y = (y - y_min)/(y_max - y_min)
        x = x*2-1
        y = y*2-1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)


        for idx , alpha in enumerate(self.F_active):
            x3 = x.repeat([self.n_cells[idx],1,1,1])
            #alpha = Gaussian_average(alpha)
            F = grid_sample_2d(alpha, x3, step= self.mode, offset=True)
            #F = CosineSampler2d.apply(alpha, x, 'zeros', True, 'cosine', True)
            features.append(F.sum(0).view(self.n_features,-1).t())
        
        # sum_level = sum(self.res[i]**2 for i in range(len(self.res)))
        # F = sum(self.res[len(self.res)-1-i]**2.0/sum_level * features[i] for i in range(len(self.res))) #*  sum(tuple(features))
        F = torch.cat(tuple(features) , 1)
        return F



if __name__ == '__main__':
    encoder = Encoder()
    features = encoder(torch.rand(1,1).to(device), torch.rand(1,1).to(device))
    print(features)
    print(features.shape)