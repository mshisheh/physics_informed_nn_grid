import torch
import torch.nn as nn
from torch.nn.functional import mse_loss, smooth_l1_loss

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')



def get_f(x = 'x', y = 'y', expr = 'x * y', nu = 1):
    from sympy import lambdify, Derivative, Symbol, simplify
    
    x = Symbol('x')
    y = Symbol('y')
    u = expr.replace('torch.','')
    u = simplify(u)
    ux = Derivative(u, x, evaluate = True)
    uxx = Derivative(ux, x, evaluate = True)
    uy = Derivative(u, y, evaluate = True)
    uyy = Derivative(uy, y, evaluate = True)
    f4 = u * ux + nu * (uxx + uyy)
    f=lambdify([x,y],f4, "numpy")
    return f


# NSu = (u * u_x + v * u_y - diffu - self.fu(x.data.cpu(),y.data.cpu()).to(device) )
# NSv = (u * v_x + v * v_y - diffv - self.fv(x.data.cpu(),y.data.cpu()).to(device) )

def get_f2(x = 'x', y = 'y', sol = {'u':'x * y', 'v':'x + y'}, nu = 1):
    from sympy import lambdify, Derivative, Symbol, simplify, integrate
    expr_u = sol['u']
    x = Symbol('x')
    y = Symbol('y')
    u = expr_u.replace('torch.','')
    u = simplify(u)
    ux = Derivative(u, x, evaluate = True)
    uxx = Derivative(ux, x, evaluate = True)
    uy = Derivative(u, y, evaluate = True)
    uyy = Derivative(uy, y, evaluate = True)
    expr_v = Derivative(integrate(u, y),x, evaluate = True)
    v = -1 * expr_v #expr_v.replace('torch.','')
    v = simplify(v)
    vx = Derivative(v, x, evaluate = True)
    vxx = Derivative(vx, x, evaluate = True)
    vy = Derivative(v, y, evaluate = True)
    vyy = Derivative(vy, y, evaluate = True)
    
    f4 = u * ux + v * uy - nu * (uxx + uyy)
    f5 = u * vx + v * vy - nu * (vxx + vyy)

    return lambdify([x,y],f4, "numpy"), lambdify([x,y],f5, "numpy")


class Loss_Functions():
    def __init__(self, model, name = 'NS', params = {}, loss_type = 'mse') -> None:
        self.name = name
        self.model = model
        self.params = params
        domain_size = params['domain_size']
        self.xmin, self.xmax, self.ymin, self.ymax = domain_size
        self.xmin, self.xmax, self.ymin, self.ymax = torch.tensor([float(self.xmin)]).to(device), torch.tensor([float(self.xmax)]).to(device), \
        torch.tensor([float(self.ymin)]).to(device), torch.tensor([float(self.ymax)]).to(device)
        if loss_type == 'mse':
            self.loss_type = mse_loss
        elif loss_type == 'logcosh':
            self.loss_type = smooth_l1_loss

        if self.name == 'NS':
            if 'Re' not in params.keys():
                raise ValueError('Re must be specified for NS')
            self.Re = self.params['Re']
            self.original_Re = self.params['Re']

        if self.name == 'uux':
            self.Re = self.params['Re']
            self.sol = params['sol']
            self.f = get_f('x', 'y', expr=self.sol, nu = 1.0/self.Re)

        if self.name == 'uux-uuy-uv' or self.name == 'uux-uuy-si' :
            self.Re = self.params['Re']
            self.sol = params['sol']
            self.fu, self.fv = get_f2('x', 'y', sol=self.sol, nu = 1.0/self.Re)
    
    def pde_loss_batch(self, dataLoader_c):
        if self.name == 'NS':
            mse_pde = 0
            for x, y in dataLoader_c:
                S = self.model(x,y)
                psi = S[:,0].reshape(-1,1)
                p = S[:,1].reshape(-1,1)

                u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
                v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]

                u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
                u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
                u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]
                u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u_y), True, True)[0]

                v_x = torch.autograd.grad(v, x, torch.ones_like(v), True, True)[0]
                v_y = -u_x #torch.autograd.grad(v, y, torch.ones_like(v), True, True)[0]
                v_xx = torch.autograd.grad(v_x, x, torch.ones_like(v_x), True, True)[0]
                v_yy = torch.autograd.grad(v_y, y, torch.ones_like(v_y), True, True)[0]

                p_x = torch.autograd.grad(p, x, torch.ones_like(p), True, True)[0]
                p_y = torch.autograd.grad(p, y, torch.ones_like(p), True, True)[0]


                NSu = (u * u_x + v * u_y + p_x - 1/self.Re * (u_xx + u_yy))
                NSv = (u * v_x + v * v_y + p_y - 1/self.Re * (v_xx + v_yy))

                mse_pde += torch.mean(NSu ** 2.0) + torch.mean(NSv ** 2.0) 


        return mse_pde

    def pde_loss(self, x, y, return_pde_terms = False):
        if self.name == 'NS':

            S = self.model(x,y)
            psi = S[:,0].reshape(-1,1)
            p = S[:,1].reshape(-1,1)

            u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
            v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]

            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]
            u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u_y), True, True)[0]

            v_x = torch.autograd.grad(v, x, torch.ones_like(v), True, True)[0]
            v_y = -u_x #torch.autograd.grad(v, y, torch.ones_like(v), True, True)[0]
            v_xx = torch.autograd.grad(v_x, x, torch.ones_like(v_x), True, True)[0]
            v_yy = torch.autograd.grad(v_y, y, torch.ones_like(v_y), True, True)[0]

            p_x = torch.autograd.grad(p, x, torch.ones_like(p), True, True)[0]
            p_y = torch.autograd.grad(p, y, torch.ones_like(p), True, True)[0]

            diffusion_u = 1/self.Re  * (u_xx + u_yy)
            diffusion_v = 1/self.Re  * (v_xx + v_yy)

            NSu = (u * u_x + v * u_y + p_x - diffusion_u)
            NSv = (u * v_x + v * v_y + p_y - diffusion_v)
            #mse_pde = torch.mean(NSu ** 2.0) + torch.mean(NSv ** 2.0) 
            pde_loss = self.loss_type(NSu, torch.zeros_like(NSu)) +  self.loss_type(NSv, torch.zeros_like(NSv))

            if return_pde_terms:
                return pde_loss, NSu, NSv, torch.abs(u_x)+torch.abs(u_y), torch.abs(v_x)+torch.abs(v_y)
            
        elif self.name == 'PO':
            u = self.model(x,y)
            
            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u), True, True)[0]
            po = u_xx + u_yy - x **2.0 - x * y + 100 * u * u_x 
            pde_loss = self.loss_type(po, torch.zeros_like(po))

            if return_pde_terms:
                return pde_loss, po

            return pde_loss

        elif self.name == 'PoissonA':
            u = self.model(x,y)
            
            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u), True, True)[0]
            po = u_xx + u_yy - 2.0 * (x**4.0 * (3.0*y - 2.0) + x**3.0 * (4.0 - 6.0 * y) + 
                                      x**2.0 * (6 * y**3.0 - 12 * y ** 2.0 + 9.0 * y - 2.0) - 6.0 * x * (y - 1.0)**2.0 * y + (y - 1.0)**2.0 * y)
            pde_loss = self.loss_type(po, torch.zeros_like(po))

            if return_pde_terms:
                return pde_loss, po, po, po, po

            return pde_loss
        
        elif self.name == 'Helmholtz':
            u = self.model(x,y)
            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u), True, True)[0]
            temp1 = torch.sin(self.params['a1'] * torch.pi * x) * torch.sin(self.params['a2'] * torch.pi * y)
            q = self.params['k'] ** 2.0 * temp1 - (self.params['a1'] * torch.pi)**2.0 * temp1 - (self.params['a2'] * torch.pi)**2.0 * temp1 
            po = u_xx + u_yy + self.params['k'] * u - q
            pde_loss = self.loss_type(po, torch.zeros_like(po))

            if return_pde_terms:
                return pde_loss, po, po, po, po
            
        elif self.name == 'Sinusoid':
            u = self.model(x,y)
            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            f = torch.cos(self.params['omega']*x) + torch.cos(self.params['omega']*y)
            #temp1 = torch.sin(self.params['a1'] * torch.pi * x) * torch.sin(self.params['a2'] * torch.pi * y)
            #q = self.params['k'] ** 2.0 * temp1 - (self.params['a1'] * torch.pi)**2.0 * temp1 - (self.params['a2'] * torch.pi)**2.0 * temp1 
            po = u_x + u_y - f
            pde_loss = self.loss_type(po, torch.zeros_like(po))

            if return_pde_terms:
                return pde_loss, po, po, po, po

            return pde_loss


        elif self.name == 'uux':
            u = self.model(x,y)
            
            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u), True, True)[0]

            diff = 1/self.Re * (u_xx + u_yy) 
            po = u * u_x  + diff - self.f(x.data.cpu(),y.data.cpu()).to(device)
            pde_loss = self.loss_type(po, torch.zeros_like(po))

            if return_pde_terms:
                return pde_loss, po, diff, torch.abs(u_x), torch.abs(u_y)

            return pde_loss #, torch.max(torch.abs(diff))


        elif self.name == 'uux-uuy-uv':
            out = self.model(x,y)
            u = out[:,0]
            v = out[:,1]
            
            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u_y), True, True)[0]

            v_x = torch.autograd.grad(v, x, torch.ones_like(v), True, True)[0]
            v_y = -u_x #torch.autograd.grad(v, y, torch.ones_like(v), True, True)[0]
            v_xx = torch.autograd.grad(v_x, x, torch.ones_like(v_x), True, True)[0]
            v_yy = torch.autograd.grad(v_y, y, torch.ones_like(v_y), True, True)[0]

            diffu = 1/self.Re * (u_xx + u_yy) 
            diffv = 1/self.Re * (v_xx + v_yy) 

            NSu = (u * u_x + v * u_y - diffu - self.fu(x.data.cpu(),y.data.cpu()).to(device) )
            NSv = (u * v_x + v * v_y - diffv - self.fv(x.data.cpu(),y.data.cpu()).to(device) )

            #cont = u_x + v_y

            pde_loss = self.loss_type(NSu, torch.zeros_like(NSu)) +  self.loss_type(NSv, torch.zeros_like(NSv))# + mse_loss(cont, torch.zeros_like(cont))

            if return_pde_terms:
                return pde_loss, NSu+NSv, diffu+diffv, torch.abs(u_x), torch.abs(u_y)

            return pde_loss #, torch.max(torch.abs(diff))


        elif self.name == 'uux-uuy-si':
            out = self.model(x,y)

            u = torch.autograd.grad(out, y, torch.ones_like(out), True, True)[0]
            v = -1*torch.autograd.grad(out, x, torch.ones_like(out), True, True)[0]
            
            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u_y), True, True)[0]

            v_x = torch.autograd.grad(v, x, torch.ones_like(v), True, True)[0]
            v_y = -u_x #torch.autograd.grad(v, y, torch.ones_like(v), True, True)[0]
            v_xx = torch.autograd.grad(v_x, x, torch.ones_like(v_x), True, True)[0]
            v_yy = torch.autograd.grad(v_y, y, torch.ones_like(v_y), True, True)[0]

            diffu = 1/self.Re * (u_xx + u_yy) 
            diffv = 1/self.Re * (v_xx + v_yy) 

            NSu = (u * u_x + v * u_y - diffu - self.fu(x.data.cpu(),y.data.cpu()).to(device) )
            NSv = (u * v_x + v * v_y - diffv - self.fv(x.data.cpu(),y.data.cpu()).to(device) )

            pde_loss = self.loss_type(NSu, torch.zeros_like(NSu)) +  self.loss_type(NSv, torch.zeros_like(NSv))

            if return_pde_terms:
                return pde_loss, NSu+NSv, diffu+diffv, torch.abs(u_x), torch.abs(u_y)

            return pde_loss #, torch.max(torch.abs(diff))


        return pde_loss

    def pde_loss_derivitives_input(self, x, y, return_pde_terms = False):
        if self.name == 'NS':
            S = self.model(x,y)
            psi = S[...,0].reshape(-1,1)
            p = S[...,1].reshape(-1,1)
            dudx = S[...,2].reshape(-1,1)
            dudy = S[...,3].reshape(-1,1)
            dvdx = S[...,4].reshape(-1,1)
            dvdy = -dudx

            u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
            v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]

            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(dudx, x, torch.ones_like(dudx), True, True)[0]
            u_yy = torch.autograd.grad(dudy, y, torch.ones_like(dudy), True, True)[0]

            v_x = torch.autograd.grad(v, x, torch.ones_like(v), True, True)[0]
            v_y = -u_x #torch.autograd.grad(v, y, torch.ones_like(v), True, True)[0]
            v_xx = torch.autograd.grad(dvdx, x, torch.ones_like(dvdx), True, True)[0]
            v_yy = torch.autograd.grad(dvdy, y, torch.ones_like(dvdy), True, True)[0]

            p_x = torch.autograd.grad(p, x, torch.ones_like(p), True, True)[0]
            p_y = torch.autograd.grad(p, y, torch.ones_like(p), True, True)[0]


            NSu = (u * u_x + v * u_y + p_x - 1/self.Re  * (u_xx + u_yy))
            NSv = (u * v_x + v * v_y + p_y - 1/self.Re  * (v_xx + v_yy))

            # mse_pde = torch.mean(NSu ** 2.0) + torch.mean(NSv ** 2.0) + \
            #     torch.mean((dudx - u_x)**2.0) + torch.mean((dudy - u_y)**2.0)  + \
            #         torch.mean((dvdx - v_x)**2.0)

            mse_pde = self.loss_type(NSu, torch.zeros_like(NSu).to(device)) + self.loss_type(NSv, torch.zeros_like(NSv).to(device)) +\
                self.loss_type(dudx, u_x) + self.loss_type(dudy, u_y) + self.loss_type(dvdx, v_x)



            if return_pde_terms:
                return mse_pde, NSu, NSv,NSu, NSv

            return mse_pde

        elif self.name == 'PO':
            S = self.model(x,y)
            u = S[...,0].reshape(-1,1)
            dudx = S[...,1].reshape(-1,1)
            dudy = S[...,2].reshape(-1,1)
            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(u_x, y, torch.ones_like(u), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u), True, True)[0]
            po = u_xx + u_yy - x **2.0 - x * y
            pde_loss = self.loss_type(po, torch.zeros_like(po)) + self.loss_type(dudx, u_x) + self.loss_type(dudy, u_y) + self.loss_type(dvdx, v_x)

            if return_pde_terms:
                return pde_loss, po

            return pde_loss

        elif self.name == 'uux':
            S = self.model(x,y)
            u = S[...,0].reshape(-1,1)
            dudx = S[...,1].reshape(-1,1)
            dudy = S[...,2].reshape(-1,1)

            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(dudx, x, torch.ones_like(dudx), True, True)[0]
            u_yy = torch.autograd.grad(dudy, y, torch.ones_like(dudy), True, True)[0]

            diff = 1/self.Re * (u_xx + u_yy) 
            po = u * u_x  + diff - self.f(x.data.cpu(),y.data.cpu()).to(device)
            pde_loss = self.loss_type(po, torch.zeros_like(po)) + self.loss_type(dudx, u_x) + self.loss_type(dudy, u_y)

            if return_pde_terms:
                return pde_loss, po, diff, torch.abs(u_x), torch.abs(u_y)

            return pde_loss #, torch.max(torch.abs(diff))

######################################################################
#--------------------------------------------------------------------#
######################################################################

    def pde_loss_derivitives_input_vorticity(self, x, y, return_pde_terms = False):
        if self.name == 'NS':
            S = self.model(x,y)
            si = S[...,0].reshape(-1,1)
            # dsi_dx = S[...,1].reshape(-1,1)
            # dsi_dy = S[...,2].reshape(-1,1)
            omega = S[...,1].reshape(-1,1)
            omega_dx = S[...,2].reshape(-1,1)
            omega_dy = S[...,3].reshape(-1,1)

            dsi_dx_p = torch.autograd.grad(si, x, torch.ones_like(si), True, True)[0]
            dsi_dy_p = torch.autograd.grad(si, y, torch.ones_like(si), True, True)[0]
            
            omega_dx_p = torch.autograd.grad(omega, x, torch.ones_like(omega), True, True)[0]
            omega_dy_p = torch.autograd.grad(omega, y, torch.ones_like(omega), True, True)[0]
            omega_xx = torch.autograd.grad(omega_dx, x, torch.ones_like(omega_dx), True, True)[0]
            omega_yy = torch.autograd.grad(omega_dy, y, torch.ones_like(omega_dy), True, True)[0]
            
            dsi_dxx = torch.autograd.grad(dsi_dx_p, x, torch.ones_like(dsi_dx_p), True, True)[0]
            dsi_dyy = torch.autograd.grad(dsi_dy_p, y, torch.ones_like(dsi_dy_p), True, True)[0]
            # du_dx = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            # dv_dy = torch.autograd.grad(v, y, torch.ones_like(u), True, True)[0]

            NSu = (dsi_dy_p * omega_dx - dsi_dx_p * omega_dy - 1/self.Re  * (omega_xx + omega_yy))
            NSv = (dsi_dxx + dsi_dyy + omega)

            mse_pde = self.loss_type(NSu, torch.zeros_like(NSu).to(device)) + self.loss_type(NSv, torch.zeros_like(NSv).to(device)) +\
                self.loss_type(omega_dx_p, omega_dx) + self.loss_type(omega_dy_p, omega_dy)
                # self.loss_type(dsi_dx_p, dsi_dx) + self.loss_type(dsi_dy_p, dsi_dy)

            if return_pde_terms:
                return mse_pde, NSu, NSv

        return mse_pde
###################################################################################

    def pde_loss_derivitives_input_vorticity2(self, x, y, return_pde_terms = False):
        if self.name == 'NS':
            S = self.model(x,y)
            dsi_dx = S[...,0].reshape(-1,1)
            dsi_dy = S[...,1].reshape(-1,1)
            omega = S[...,2].reshape(-1,1)
            omega_dx = S[...,3].reshape(-1,1)
            omega_dy = S[...,4].reshape(-1,1)

            u = dsi_dx
            v = -1 * dsi_dy
            
            omega_dx_p = torch.autograd.grad(omega, x, torch.ones_like(omega), True, True)[0]
            omega_dy_p = torch.autograd.grad(omega, y, torch.ones_like(omega), True, True)[0]
            omega_xx = torch.autograd.grad(omega_dx, x, torch.ones_like(omega_dx), True, True)[0]
            omega_yy = torch.autograd.grad(omega_dy, y, torch.ones_like(omega_dy), True, True)[0]
            
            dsi_dxx = torch.autograd.grad(dsi_dx, x, torch.ones_like(dsi_dx), True, True)[0]
            dsi_dyy = torch.autograd.grad(dsi_dy, y, torch.ones_like(dsi_dy), True, True)[0]
            du_dx = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            dv_dy = torch.autograd.grad(v, y, torch.ones_like(v), True, True)[0]

            NSu = (dsi_dy * omega_dx - dsi_dx * omega_dy - 1/self.Re  * (omega_xx + omega_yy))
            NSv = (dsi_dxx + dsi_dyy + omega)

            cont = du_dx + dv_dy
            mse_pde = self.loss_type(NSu, torch.zeros_like(NSu).to(device)) + self.loss_type(NSv, torch.zeros_like(NSv).to(device)) +\
                self.loss_type(omega_dx_p, omega_dx) + self.loss_type(omega_dy_p, omega_dy) + self.loss_type(cont,  torch.zeros_like(cont).to(device))

            if return_pde_terms:
                return mse_pde, NSu, NSv

        return mse_pde
##################################---------------###################################

    def pde_loss_derivitives_input_fisher(self, x, y, return_pde_terms = False, fisher = None, solution = None):
        if self.name == 'NS':
            S = self.model(x,y)
            psi = S[...,0].reshape(-1,1)
            p = S[...,1].reshape(-1,1)
            dudx = S[...,2].reshape(-1,1)
            dudy = S[...,3].reshape(-1,1)
            dvdx = S[...,4].reshape(-1,1)
            dvdy = -dudx

            u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
            v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]

            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(dudx, x, torch.ones_like(dudx), True, True)[0]
            u_yy = torch.autograd.grad(dudy, y, torch.ones_like(dudy), True, True)[0]

            v_x = torch.autograd.grad(v, x, torch.ones_like(v), True, True)[0]
            v_y = -u_x #torch.autograd.grad(v, y, torch.ones_like(v), True, True)[0]
            v_xx = torch.autograd.grad(dvdx, x, torch.ones_like(dvdx), True, True)[0]
            v_yy = torch.autograd.grad(dvdy, y, torch.ones_like(dvdy), True, True)[0]

            p_x = torch.autograd.grad(p, x, torch.ones_like(p), True, True)[0]
            p_y = torch.autograd.grad(p, y, torch.ones_like(p), True, True)[0]


            NSu = (u * u_x + v * u_y + p_x - 1/self.Re  * (u_xx + u_yy))
            NSv = (u * v_x + v * v_y + p_y - 1/self.Re  * (v_xx + v_yy))

            mse_pde = torch.mean(NSu ** 2.0) + torch.mean(NSv ** 2.0) + \
                torch.mean((dudx - u_x)**2.0) + torch.mean((dudy - u_y)**2.0)  + \
                    torch.mean((dvdx - v_x)**2.0)
            
            if fisher is not None:
                temp = [p for p in self.model.parameters()]
                param = torch.cat([p.view(-1) for p in temp])
                mse_pde += torch.sum((param - solution)**2) 

            if return_pde_terms:
                return mse_pde, NSu, NSv

        return mse_pde
    

    def fisher_loss(self, fisher, solution):
        temp = [p for p in self.model.parameters()]
        param = torch.cat([p.view(-1) for p in temp])
        return torch.sum((param - solution)**2)



    def bc_loss(self, x, y, u_bc, normalize = False):
        if self.name == 'NS':
            S = self.model(x,y)
            psi = S[...,0].reshape(-1,1)
            p = S[...,1].reshape(-1,1)
            u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
            v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
            uv = torch.cat([u,v], axis = -1)

            if normalize:
                diff = torch.square(uv - u_bc)
                diff2 = (diff - diff.min(axis = 0)[0])/(diff.max(axis = 0)[0] - diff.min(axis = 0)[0]) 
                mse_bc = torch.mean(diff2) 
            mse_bc = self.loss_type(uv, u_bc) 
        elif self.name == 'PO' or self.name == 'uux'  or self.name == 'PoissonA' or self.name == 'Helmholtz' or self.name =='Sinusoid':
            S = self.model(x,y)
            uv = S[:,0].reshape(-1,1)
            mse_bc = mse_loss(uv, u_bc) 
        elif self.name == 'uux-uuy-uv':
            S = self.model(x,y)
            u = S[:,0].reshape(-1,1)
            v = S[:,1].reshape(-1,1)
            uv = torch.cat([u,v], axis = -1)
            mse_bc = mse_loss(uv, u_bc) 
        elif self.name == 'uux-uuy-si':
            S = self.model(x,y)
            u = torch.autograd.grad(S, y, torch.ones_like(S), True, True)[0].reshape(-1,1)
            v = -1*torch.autograd.grad(S, x, torch.ones_like(S), True, True)[0].reshape(-1,1)
            uv = torch.cat([u,v], axis = -1)
            mse_bc = mse_loss(uv, u_bc) 
        return mse_bc
    

    def bc_neuman_loss(self, x, y, u_bc):
        index = torch.isclose(x, self.xmin)
        x1, y1 = x[index].reshape(-1,1), y[index].reshape(-1,1)
        S1 = self.model(x1,y1)
        un1 = torch.autograd.grad(S1, x1, torch.ones_like(S1), True, True)[0]
        
        index = torch.isclose(x, self.xmax)
        x2, y2 = x[index].reshape(-1,1), y[index].reshape(-1,1)
        S2 = self.model(x2,y2)
        un2 = torch.autograd.grad(S2, x2, torch.ones_like(S2), True, True)[0]

        index = torch.isclose(y, self.ymax)
        x3, y3 = x[index].reshape(-1,1), y[index].reshape(-1,1)
        S3 = self.model(x3,y3)
        un3 = torch.autograd.grad(S3, y3, torch.ones_like(S3), True, True)[0]                
        

        un = torch.concat([un1, un2, un3], axis = 0)

        mse_bc = mse_loss(un, torch.zeros_like(un)) 
        return mse_bc


    def bc_loss_pde(self, x, y, u_bc, normalize = False):
        if self.name == 'NS':
            S = self.model(x,y)
            psi = S[...,0].reshape(-1,1)
            p = S[...,1].reshape(-1,1)
            u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
            v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
            uv = torch.cat([u,v,p], axis = -1)

            if normalize:
                diff = torch.square(uv - u_bc)
                diff2 = (diff - diff.min(axis = 0)[0])/(diff.max(axis = 0)[0] - diff.min(axis = 0)[0]) 
                mse_bc = torch.mean(diff2) 
            mse_bc = self.loss_type(uv, u_bc) 
        elif self.name == 'PO':
            bc_pde_loss = self.pde_loss(x, y)
            S = self.model(x,y)
            uv = S.reshape(-1,1)
            mse_bc = mse_loss(uv, u_bc) 
        return mse_bc + bc_pde_loss



######################################################################
#--------------------------------------------------------------------#
######################################################################
    def bc_loss_no_p_vorticity(self, x, y, u_bc, normalize = False):

        S = self.model(x,y)
        dsi_dx = S[...,1].reshape(-1,1)
        dsi_dy = S[...,2].reshape(-1,1)

        u = dsi_dy
        v = -1 * dsi_dx

        uv = torch.cat([u,v], axis = -1)
        if normalize:
            diff = torch.square(uv - u_bc)
            diff2 = (diff - diff.min(axis = 0)[0])/(diff.max(axis = 0)[0] - diff.min(axis = 0)[0]) 
            mse_bc = torch.mean(diff2) 

        mse_bc = self.loss_type(uv, u_bc) 
        return mse_bc
######################################################################
#--------------------------------------------------------------------#
######################################################################
    def bc_loss_nop(self, x, y, u_bc, normalize = False):

        S = self.model(x,y)
        psi = S[...,0].reshape(-1,1)
        p = S[...,1].reshape(-1,1)
        u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
        v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
        uv = torch.cat([u,v], axis = -1)

        if normalize:
            diff = torch.square(uv - u_bc)
            diff2 = (diff - diff.min(axis = 0)[0])/(diff.max(axis = 0)[0] - diff.min(axis = 0)[0]) 
            mse_bc = torch.mean(diff2) 
            return mse_bc

        #mse_bc = torch.mean(torch.square(uv - u_bc)) 
        mse_bc = self.loss_type(uv, u_bc)
        return mse_bc

    def bc_loss_one_p(self, x, y, u_bc, normalize = False):

        S = self.model(x,y)
        psi = S[:,0].reshape(-1,1)
        p = S[:,1].reshape(-1,1)
        u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
        v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
        uv = torch.cat([u,v], axis = -1)

        x, y = torch.tensor(-1.0).to(device).reshape(-1,1), torch.tensor(0.0).to(device).reshape(-1,1)
        Sp = self.model(x, y)

        #mse_p = torch.mean(torch.square(S[:,1].reshape(-1,1) - (torch.tensor(-0.0).to(device))))
        mse_p = self.loss_type(Sp[:,1].reshape(-1,1), torch.tensor(-0.0).to(device))
        if normalize:
            diff = torch.square(uv - u_bc[:,:2])
            diff2 = (diff - diff.min(axis = 0)[0])/(diff.max(axis = 0)[0] - diff.min(axis = 0)[0]) 
            mse_bc = torch.mean(diff2) 
            return mse_bc

        mse_bc = self.loss_type(uv, u_bc[:,:2]) #torch.mean(torch.square(uv - u_bc[:,:2]))

        return mse_bc + mse_p
    
    def bc_loss_one_p_pde(self, x, y, u_bc, normalize = False):

        bc_pde_loss = self.pde_loss_derivitives_input(x, y)

        S = self.model(x,y)
        psi = S[...,0].reshape(-1,1)
        p = S[...,1].reshape(-1,1)
        u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
        v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
        uv = torch.cat([u,v], axis = -1)

        x, y = torch.tensor(-1.0).to(device).reshape(-1,1), torch.tensor(0.0).to(device).reshape(-1,1)
        S = self.model(x, y)

        #mse_p = torch.mean(torch.square(S[:,1].reshape(-1,1) - (torch.tensor(-0.0).to(device))))
        mse_p = self.loss_type(S[:,1].reshape(-1,1), torch.tensor(-0.0).to(device))

        if normalize:
            diff = torch.square(uv - u_bc[:,:2])
            diff2 = (diff - diff.min(axis = 0)[0])/(diff.max(axis = 0)[0] - diff.min(axis = 0)[0]) 
            mse_bc = torch.mean(diff2) 
            return mse_bc

        #mse_bc = torch.mean(torch.square(uv - u_bc[:,:2]))
        mse_bc = self.loss_type(uv, u_bc[...,:2])

        return mse_bc + mse_p + bc_pde_loss

    def bc_loss_all_p_pde(self, x, y, u_bc, normalize = False):

        bc_pde_loss = self.pde_loss_derivitives_input(x, y)

        S = self.model(x,y)
        psi = S[...,0].reshape(-1,1)
        p = S[...,1].reshape(-1,1)
        u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
        v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
        uv = torch.cat([u,v, p], axis = -1)

        if normalize:
            diff = torch.square(uv - u_bc)
            diff2 = (diff - diff.min(axis = 0)[0])/(diff.max(axis = 0)[0] - diff.min(axis = 0)[0]) 
            mse_bc = torch.mean(diff2) 
            return mse_bc

        #mse_bc = torch.mean(torch.square(uv - u_bc[:,:2]))
        mse_bc = self.loss_type(uv, u_bc)

        return mse_bc + bc_pde_loss


    def bc_loss_no_p_pde_vorticity(self, x, y, u_bc, normalize = False):

        bc_pde_loss = self.pde_loss_derivitives_input_vorticity(x, y)

        S = self.model(x,y)
        psi = S[...,0].reshape(-1,1)
        p = S[...,1].reshape(-1,1)
        u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
        v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
        uv = torch.cat([u,v], axis = -1)

        if normalize:
            diff = torch.square(uv - u_bc)
            diff2 = (diff - diff.min(axis = 0)[0])/(diff.max(axis = 0)[0] - diff.min(axis = 0)[0]) 
            mse_bc = torch.mean(diff2) 
            return mse_bc

        #mse_bc = torch.mean(torch.square(uv - u_bc[:,:2]))
        mse_bc = self.loss_type(uv, u_bc)

        return mse_bc + bc_pde_loss



    def bc_loss_omega_pde_vorticity(self, x, y, u_bc, normalize = False):

        bc_pde_loss = self.pde_loss_derivitives_input_vorticity(x, y)

        S = self.model(x,y)
        psi = S[...,0].reshape(-1,1)
        p = S[...,1].reshape(-1,1)
        u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
        v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
        uv = torch.cat([u,v, p], axis = -1)

        if normalize:
            diff = torch.square(uv - u_bc)
            diff2 = (diff - diff.min(axis = 0)[0])/(diff.max(axis = 0)[0] - diff.min(axis = 0)[0]) 
            mse_bc = torch.mean(diff2) 
            return mse_bc

        #mse_bc = torch.mean(torch.square(uv - u_bc[:,:2]))
        mse_bc = self.loss_type(uv, u_bc)

        return mse_bc + bc_pde_loss



    def bc_loss_dp(self, x, y, u_bc):

        #bc_pde_loss = self.pde_loss_derivitives_input(x, y)

        S = self.model(x,y)
        p = S[:,1].reshape(-1,1)
        dpdx = torch.autograd.grad(p, x, torch.ones_like(p), create_graph = True, retain_graph = True)[0]
        dpdy = torch.autograd.grad(p, y, torch.ones_like(p), create_graph = True, retain_graph = True)[0]
        uv = torch.cat([dpdx,dpdy], axis = -1)


        mse_dp = self.loss_type(uv, u_bc)

        return mse_dp


    def hbc_loss(self, x, y, u_bc):

        S = self.decoder(self.encoder(x,y))
        psi = S[:,0].reshape(-1,1)
        p = S[:,1].reshape(-1,1)
        u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
        v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
        uvp = torch.cat([u,v,p], axis = -1) #(N,3)

        weight = torch.ones_like(uvp)
        weight[:,2] = 1.15

        mse_bc = torch.mean(weight * torch.square(uvp - u_bc)) 

        return mse_bc

    def data_loss(self, x, y, u_data):
        if self.name == 'NS':
            S = self.model(x,y)
            psi = S[:,0].reshape(-1,1)
            p = S[:,1].reshape(-1,1)
            u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
            v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
            uv = torch.cat([u,v], axis = -1)
            mse_data = torch.mean(torch.square(uv - u_data)) 

        elif self.name == 'PO' or self.name == 'uux' or self.name == 'PoissonA' or self.name == 'Helmholtz':
            S = self.model(x,y)
            uv = S.reshape(-1,1)
            mse_data = torch.mean(torch.square(uv - u_data)) 

        return mse_data

    def MSE_loss(self, x, y, u_bc):

        S = self.model(x,y).reshape(-1,1)
        mse_data = torch.mean(torch.square(S - u_bc)) 

        return mse_data

    def bc_loss_batch(self, dataLoader_bc):

        mse_bc = 0
        for x, y, u_bc in dataLoader_bc:
            S = self.model(x,y)
            psi = S[:,0].reshape(-1,1)
            p = S[:,1].reshape(-1,1)
            u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
            v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]
            uv = torch.cat([u,v], axis = -1)

            mse_bc += torch.mean(torch.square(uv - u_bc)) 

        return mse_bc

    
    # def hybrid_bc(self ,x,y,u_bc):

        
    #     #This would work for this particular example (Taylor-Green vortex), normally 4 different boundary functions should be defined: uL , uB , uT , uR
    #     uL = lambda x , y: customized_2d_exact_u(x_min , y)
    #     uB =  lambda x , y: customized_2d_exact_u(x , y_min)
    #     uT = lambda x , y: customized_2d_exact_u(x , y_max)
    #     uR = lambda x , y: customized_2d_exact_u(x_max , y)


    #     #threshold for identifying the boundaries
    #     eps = 1e-8

    #     ONE = lambda k: torch.where(torch.abs(1-k)<eps , 1. , k)
    #     ZERO = lambda k: torch.where(torch.abs(k)<eps , 0. , k)

        
    #     #rescaling the domain to [0,1]x[0,1] for easier formulation:
    #     x_n = (x-x_min)/(x_max-x_min)
    #     y_n = (y-y_min)/(y_max-y_min)

    #     #"VERY" close to the boundaries (i.e., closer than the threshold) = on the boundaries
    #     x_n , y_n = ONE(x_n) , ONE(y_n)
    #     x_n , y_n = ZERO(x_n) , ZERO(y_n)

    #     X = torch.cat([x_n , y_n] , -1) #(N,2)
        
    #     phi_= torch.min(torch.sign(X*(1-X)) , dim = 1)[:][0] #returns 0 on boundaries, 1 inside the domain #(N,)
    #     phi = torch.unsqueeze(phi_, dim = 1) #(N,1)

    
    #     g = (1-torch.sign(x_n))*uL(x,y)+\
    #         (1-torch.sign(y_n))*uB(x,y)+\
    #             (1-torch.sign(1-y_n))*uT(x,y)+\
    #             (1-torch.sign(1-x_n))*uR(x,y)-\
    #                 (1-torch.sign(1-x_n*y_n))*uR(x,y) #to fix the value at (1,1) because it is considered twice (rescaled coordinates)
    

    #     u_ = u*phi + (1-phi)*g

    #     return u_