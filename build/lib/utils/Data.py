import torch
import torch.nn as nn
import numpy as np
from scipy.stats.qmc import Sobol
from utils.initialize import initialize
from sympy import lambdify, Derivative, Symbol, simplify, integrate

def get_data_spatial_Taylor_green(params = 
                                  {'N_f': 50000, 'N_bc':50000, 't':1.0, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 100, 'seed': 1234}, 
                                  train = True, use_data = False):
    
    dtype, device = initialize(seed = params['seed']) 
    nu = params['nu']
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    t = params['t']
    seed = params['seed']
    ntest = params['ntest']

    t = torch.tensor(t)
    
    # The exact solution
    def extcat_solution(x, y, t):
        f = torch.exp(-2 * nu * t)
        u = torch.cos(x) * torch.sin(y) * f
        v = -torch.sin(x) * torch.cos(y) * f
        p = -1.0/4.0 * (torch.cos(2 * x) + torch.cos(2 * y)) * f ** 2
        return u, v, p

    # training
    xmin, xmax, ymin, ymax = domain_size
    Sobol1d = Sobol(d = 1, scramble=True, seed = seed)
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    if train == True:
        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax - xmin) + xmin
        y = data[:,1].reshape(-1,1) * (ymax - ymin) + ymin
        
        u, v, p = extcat_solution(x, y, t)
        sol = torch.hstack([u,v,p]).to(device).to(dtype)
        x.requires_grad = True
        y.requires_grad = True

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([torch.ones_like(temp_x) * xmin, temp_x, torch.ones_like(temp_x) * xmax, temp_x])
        y_bc = torch.vstack([temp_y, torch.ones_like(temp_y) * ymin, temp_y, torch.ones_like(temp_y) * ymax])

        idx = torch.randperm(x_bc.shape[0])
        x_bc = x_bc[idx].view(x_bc.size())
        y_bc = y_bc[idx].view(y_bc.size())

        u_bc, v_bc, p_bc = extcat_solution(x_bc, y_bc, t)
        sol_bc = torch.hstack([u_bc, v_bc, p_bc]).to(device).to(dtype)

        x.requires_grad = True
        y.requires_grad = True
        x_bc.requires_grad = True
        y_bc.requires_grad = True


        if use_data:
            d_x = torch.distributions.uniform.Uniform(xmin, xmax)
            d_y = torch.distributions.uniform.Uniform(ymin, ymax)
            x_t, y_t  = d_x.sample((100,1)).to(device), d_y.sample((100,1)).to(device)

            u, v, p = extcat_solution(x_t, y_t, t)
            sol_t = torch.hstack([u,v,p]).to(device).to(dtype)

            x_t.requires_grad = True
            y_t.requires_grad = True

            return x, y, sol, x_bc, y_bc, sol_bc, x_t, y_t, sol_t


        return x, y, sol, x_bc, y_bc, sol_bc

    # testing
    if train is False:
        x = torch.linspace(xmin, xmax, ntest)
        y = torch.linspace(ymin, ymax, ntest)
        x, y  = torch.meshgrid(x, y)
        x = x.reshape(-1,1).to(device)
        y = y.reshape(-1,1).to(device)

        u, v, p = extcat_solution(x, y, t)
        sol_tes = torch.hstack([u,v,p]).to(device).to(dtype)

        x.requires_grad = True
        y.requires_grad = True

        return x, y, sol_tes

def get_data_spatial_lid_cavity(
        params = {'N_f': 50000, 'N_bc':50000, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 100, 'seed': 1234}, train = True, use_data = False):

    dtype, device = initialize(seed = params['seed']) 
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    seed = params['seed']
    ntest = params['ntest']
    Re = params['Re']
    mask = params['mask']

    


    # The exact solution
    def extcat_solution(resturn_outs = False ):
        DATA_np = np.loadtxt(r"./Data/LDC_sine_Re"+str(Re)+".txt") 
        DATA_ = DATA_np[np.random.permutation(DATA_np.shape[0]), :]    #It has solutions for 90K points
        DATA = torch.from_numpy(DATA_[:,:6]).to(device).type(dtype) #x , y , u , v , p , U (the rest are derivatives computed by comsol)

        xc , yc = DATA[:,0:1] , DATA[:,1:2]
        sol = DATA[:,2:6] #u,v

        yc.requires_grad = True
        xc.requires_grad = True

        DATA_bnp = np.loadtxt(r"./Data/cavity_boundary_data.txt")
        DATA_b = torch.from_numpy(DATA_bnp).to(device).type(dtype)

        xb , yb = DATA_b[:,0:1] , DATA_b[:,1:2]
        sol_bc = DATA_b[:,2:6] #u,v

        if resturn_outs:
            u = DATA[:,2]
            v = DATA[:,3]
            p = DATA[:,4]
            U = DATA[:,5]
            return xc, yc, sol, xb, yb, sol_bc, u, v, p, U


        return xc, yc, sol, xb, yb, sol_bc

    # training
    xmin, xmax, ymin, ymax = domain_size
    Sobol1d = Sobol(d = 1, scramble=True, seed = seed)
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    if train == True:
        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax - xmin) + xmin
        y = data[:,1].reshape(-1,1) * (ymax - ymin) + ymin
        
        x.requires_grad = True
        y.requires_grad = True

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([torch.ones_like(temp_y) * xmin, temp_x, torch.ones_like(temp_y) * xmax, temp_x])
        y_bc = torch.vstack([temp_y, torch.ones_like(temp_x) * ymin, temp_y, torch.ones_like(temp_x) * ymax])
        u_bc = torch.vstack([torch.zeros_like(temp_y), torch.zeros_like(temp_x), torch.zeros_like(temp_y), torch.ones_like(temp_x)])
        v_bc = torch.vstack([torch.zeros_like(temp_y), torch.zeros_like(temp_x), torch.zeros_like(temp_y),torch.zeros_like(temp_x)])

        idx = torch.randperm(x_bc.shape[0])
        x_bc = x_bc[idx].view(x_bc.size())
        y_bc = y_bc[idx].view(y_bc.size())
        u_bc = u_bc[idx].view(u_bc.size())
        v_bc = v_bc[idx].view(v_bc.size())

        sol_bc = torch.hstack([u_bc, v_bc]).to(device).to(dtype)
        sol = torch.zeros_like(sol_bc)[:N_f]

        x.requires_grad = True
        y.requires_grad = True
        x_bc.requires_grad = True
        y_bc.requires_grad = True


        if use_data:

            xc, yc, sol, _, _, _ = extcat_solution(resturn_outs=False)
            num_temp = torch.randint(0, len(xc), (1,))
            x_t = xc[num_temp].detach().data
            y_t = yc[num_temp].detach().data
            sol_t = sol[num_temp,:2].detach().data
            #print(sol_t)


            x_t.requires_grad = True
            y_t.requires_grad = True

            return x, y, sol, x_bc, y_bc, sol_bc, x_t, y_t, sol_t
        
        print(x.shape)
        
        x_masked = []
        y_masked = []
        sol_masked = []
        """
        if mask > 0.:
            t = mask

            for i in range(x.shape[0]):
                if (x[i , :] < xmin + t ) or (x[i , :] > xmax - t) or (y[i, :] < ymin + t) or (y[i, :] > ymax - t):
                    x_masked.append(x[i , :].reshape(-1 , 1))
                    y_masked.append(y[i , :].reshape(-1 , 1))
                    #sol_masked.append(sol[i ,:])

            x = torch.cat(x_masked , 0)
            y = torch.cat(y_masked , 0)
            #sol_masked = torch.cat(sol_masked , 0)
            print(x.shape)
        """




        return x, y, sol, x_bc, y_bc, sol_bc



    # testing
    if train is False:

        xc, yc, sol, xb, yb, sol_bc = extcat_solution(resturn_outs=False)

        xc.requires_grad = True
        yc.requires_grad = True

        return xc, yc, sol




def get_data_spatial_lid_cavity_vorticity(
        params = {'N_f': 50000, 'N_bc':50000, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 100, 'seed': 1234}, train = True, use_data = False):

    dtype, device = initialize(seed = params['seed']) 
    nu = params['nu']
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    seed = params['seed']
    ntest = params['ntest']


    # The exact solution
    def extcat_solution(resturn_outs = False):
        DATA_np = np.loadtxt(r"./Data/cavity_data.txt") 
        DATA_ = DATA_np[np.random.permutation(DATA_np.shape[0]), :]    #It has solutions for 90K points
        DATA = torch.from_numpy(DATA_[:,:6]).to(device).type(dtype) #x , y , u , v , p , U (the rest are derivatives computed by comsol)

        xc , yc = DATA[:,0:1] , DATA[:,1:2]
        sol = DATA[:,2:6] #u,v

        yc.requires_grad = True
        xc.requires_grad = True

        DATA_bnp = np.loadtxt(r"./Data/cavity_boundary_data.txt")
        DATA_b = torch.from_numpy(DATA_bnp).to(device).type(dtype)

        xb , yb = DATA_b[:,0:1] , DATA_b[:,1:2]
        sol_bc = DATA_b[:,2:6] #u,v

        if resturn_outs:
            u = DATA[:,2]
            v = DATA[:,3]
            p = DATA[:,4]
            U = DATA[:,5]
            return xc, yc, sol, xb, yb, sol_bc, u, v, p, U


        return xc, yc, sol, xb, yb, sol_bc

    # training
    xmin, xmax, ymin, ymax = domain_size
    Sobol1d = Sobol(d = 1, scramble=True, seed = seed)
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    if train == True:
        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax - xmin) + xmin
        y = data[:,1].reshape(-1,1) * (ymax - ymin) + ymin
        
        x.requires_grad = True
        y.requires_grad = True

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([torch.ones_like(temp_y) * xmin, temp_x, torch.ones_like(temp_y) * xmax, temp_x])
        y_bc = torch.vstack([temp_y, torch.ones_like(temp_x) * ymin, temp_y, torch.ones_like(temp_x) * ymax])
        u_bc = torch.vstack([torch.zeros_like(temp_y), torch.zeros_like(temp_x), torch.zeros_like(temp_y),torch.ones_like(temp_x)])
        v_bc = torch.vstack([torch.zeros_like(temp_y), torch.zeros_like(temp_x), torch.zeros_like(temp_y),torch.zeros_like(temp_x)])
        vort_bc = torch.vstack([torch.zeros_like(temp_y), torch.zeros_like(temp_x), torch.zeros_like(temp_y),torch.zeros_like(temp_x)])

        idx = torch.randperm(x_bc.shape[0])
        x_bc = x_bc[idx].view(x_bc.size())
        y_bc = y_bc[idx].view(y_bc.size())
        u_bc = u_bc[idx].view(u_bc.size())
        v_bc = v_bc[idx].view(v_bc.size())
        vort_bc = vort_bc[idx].view(v_bc.size())

        sol_bc = torch.hstack([u_bc, v_bc, vort_bc]).to(device).to(dtype)
        sol = torch.zeros_like(sol_bc)[:N_f]

        x.requires_grad = True
        y.requires_grad = True
        x_bc.requires_grad = True
        y_bc.requires_grad = True


        if use_data:

            xc, yc, sol, _, _, _ = extcat_solution(resturn_outs=False)
            num_temp = torch.randint(0, len(xc), (100,))
            x_t = xc[num_temp].detach().data
            y_t = yc[num_temp].detach().data
            sol_t = sol[num_temp,:2].detach().data
            #print(sol_t)


            x_t.requires_grad = True
            y_t.requires_grad = True

            return x, y, sol, x_bc, y_bc, sol_bc, x_t, y_t, sol_t


        return x, y, sol, x_bc, y_bc, sol_bc



    # testing
    if train is False:

        xc, yc, sol, xb, yb, sol_bc = extcat_solution(resturn_outs=False)

        xc.requires_grad = True
        yc.requires_grad = True

        return xc, yc, sol


#--------------- Boundary smooth function-------------------

def get_smooth_boundary_data(params = 
                                  {'N_f': 50000, 'N_bc':50000, 't':1.0, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 100, 'seed': 1234}, 
                                  train = True, use_data = False):
    
    dtype, device = initialize(seed = params['seed']) 
    nu = params['nu']
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    seed = params['seed']
    ntest = params['ntest']
    
    xmin, xmax, ymin, ymax = domain_size
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    
    if train == True:
        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax -2e-10 - xmin) + xmin + 1e-10
        y = data[:,1].reshape(-1,1) * (ymax -2e-10 - ymin) + ymin + 1e-10
        

        #u = torch.ones_like(x).to(device)
        u = (x - xmin) * (x - xmax) * (y - ymin) * (y - ymax)/((x - xmin) * (y - ymin) * (x - xmax) * (y - ymax) + 1e-11)

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([torch.ones_like(temp_y) * xmin, temp_x, torch.ones_like(temp_y) * xmax, temp_x])
        y_bc = torch.vstack([temp_y, torch.ones_like(temp_x) * ymin, temp_y, torch.ones_like(temp_x) * ymax])
        u_bc = torch.vstack([torch.zeros_like(temp_y), torch.zeros_like(temp_x), torch.zeros_like(temp_y),torch.zeros_like(temp_x)])

        x_bc = torch.vstack([x_bc, x]).to(device)
        y_bc = torch.vstack([y_bc, y]).to(device)
        u_bc = torch.vstack([u_bc, u]).to(device)

        x_bc.requires_grad = True
        y_bc.requires_grad = True
        u_bc.requires_grad = True

        x.requires_grad = True
        y.requires_grad = True

    return x, y, u, x_bc, y_bc, u_bc




def get_dp_from_model(model, x, y):
    
    S = model(x,y)
    p = S[:,1].reshape(-1,1)

    p_x = torch.autograd.grad(p, x, torch.ones_like(p), True, True)[0].detach()
    p_y = torch.autograd.grad(p, y, torch.ones_like(p), True, True)[0].detach()


    return torch.cat([p_x, p_y], axis = -1)


def get_dp_from_model2(model, x, y, Re):
        S = model(x,y)
        psi = S[:,0].reshape(-1,1)
        dudx = S[:,2].reshape(-1,1)
        dudy = S[:,3].reshape(-1,1)
        dvdx = S[:,4].reshape(-1,1)
        dvdy = -dudx

        u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
        v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]

        u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
        u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
        u_xx = torch.autograd.grad(dudx, x, torch.ones_like(dudx), True, True)[0]
        u_yy = torch.autograd.grad(dudy, y, torch.ones_like(dudy), True, True)[0]

        v_x = torch.autograd.grad(v, x, torch.ones_like(v), True, True)[0]
        v_y = -u_x #torch.autograd.grad(v, y, torch.ones_like(v), True, True)[0]
        v_xx = torch.autograd.grad(dvdx, x, torch.ones_like(dvdx), True, True)[0]
        v_yy = torch.autograd.grad(dvdy, y, torch.ones_like(dvdy), True, True)[0]

        p_x = -1*(u * u_x + v * u_y - 1/Re  * (u_xx + u_yy)).detach()
        p_y = -1*(u * v_x + v * v_y - 1/Re  * (v_xx + v_yy)).detach()

        return torch.cat([p_x, p_y], axis = -1)


def get_data_poisson(
        params = {'N_f': 50000, 'N_bc':50000, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 100, 'seed': 1234}, train = True, use_data = False):

    dtype, device = initialize(seed = params['seed']) 
    nu = params['nu']
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    seed = params['seed']
    ntest = params['ntest']


    # The exact solution
    def extcat_solution(resturn_outs = False):
        DATA_np = np.loadtxt(r"./Data/poisson_f=x2+xy.txt") 
        DATA_ = DATA_np[np.random.permutation(DATA_np.shape[0]), :]    #It has solutions for 90K points
        DATA = torch.from_numpy(DATA_[:,...]).to(device).type(dtype) #x , y , u , v , p , U (the rest are derivatives computed by comsol)

        xc , yc = DATA[:,0:1] , DATA[:,1:2]
        sol = DATA[:,2:] #u,v

        yc.requires_grad = True
        xc.requires_grad = True


        return xc, yc, sol

    # training
    xmin, xmax, ymin, ymax = domain_size
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    if train == True:
        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax - xmin) + xmin
        y = data[:,1].reshape(-1,1) * (ymax - ymin) + ymin
        
        x.requires_grad = True
        y.requires_grad = True

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([torch.ones_like(temp_y) * xmin, temp_x, torch.ones_like(temp_y) * xmax, temp_x])
        y_bc = torch.vstack([temp_y, torch.ones_like(temp_x) * ymin, temp_y, torch.ones_like(temp_x) * ymax])
        u_bc = torch.vstack([torch.zeros_like(temp_y), torch.ones_like(temp_x) * torch.sin(torch.pi * temp_x), 
                             torch.zeros_like(temp_y),torch.ones_like(temp_x) * torch.sin(torch.pi * temp_x)])

        idx = torch.randperm(x_bc.shape[0])
        x_bc = x_bc[idx].view(x_bc.size())
        y_bc = y_bc[idx].view(y_bc.size())
        u_bc = u_bc[idx].view(u_bc.size())

        sol_bc = u_bc.to(device).to(dtype)
        sol = torch.zeros((N_f, sol_bc.shape[-1])).to(device).to(dtype)

        x.requires_grad = True
        y.requires_grad = True
        x_bc.requires_grad = True
        y_bc.requires_grad = True

        if use_data:

            xc, yc, sol, _, _, _ = extcat_solution(resturn_outs=False)
            num_temp = torch.randint(0, len(xc), (100,))
            x_t = xc[num_temp].detach().data
            y_t = yc[num_temp].detach().data
            sol_t = sol[num_temp,...].detach().data
            #print(sol_t)

            x_t.requires_grad = True
            y_t.requires_grad = True

            return x, y, sol, x_bc, y_bc, sol_bc, x_t, y_t, sol_t


        return x, y, sol, x_bc, y_bc, sol_bc

    # testing
    if train is False:

        xc, yc, sol = extcat_solution(resturn_outs=False)

        xc.requires_grad = True
        yc.requires_grad = True

        return xc, yc, sol



def get_PoissonA(
        params = {'N_f': 50000, 'N_bc':50000, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 100, 'seed': 1234}, train = True, use_data = False):

    dtype, device = initialize(seed = params['seed']) 
    nu = params['nu']
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    seed = params['seed']
    ntest = params['ntest']
    if 'Neuman_bc' in params.keys():
        neuman_bc =   params['Neuman_bc']
    else:
        neuman_bc  = False


    # The exact solution
    def extcat_solution(x, y):
        return x**2.0 * (x - 1.0)**2.0 * y * (y - 1.0)**2.0

    # training
    xmin, xmax, ymin, ymax = domain_size
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    if train == True:

        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax - xmin) + xmin
        y = data[:,1].reshape(-1,1) * (ymax - ymin) + ymin
        x.requires_grad = True
        y.requires_grad = True

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([temp_x])
        y_bc = torch.vstack([torch.ones_like(temp_x) * ymin])
        u_bc = torch.vstack([torch.zeros_like(temp_x)])
        
        x_bc_n = torch.vstack([torch.ones_like(temp_y) * xmin, torch.ones_like(temp_y) * xmax, temp_x])
        y_bc_n = torch.vstack([temp_y, temp_y, torch.ones_like(temp_x) * ymax])

        idx = torch.randperm(x_bc.shape[0])
        x_bc = x_bc[idx].view(x_bc.size())
        y_bc = y_bc[idx].view(y_bc.size())
        u_bc = u_bc[idx].view(u_bc.size())

        sol_bc = u_bc.to(device).to(dtype)
        sol = torch.zeros((N_f, sol_bc.shape[-1])).to(device).to(dtype)
        sol_bc_n = torch.zeros((len(x_bc_n), sol_bc.shape[-1])).to(device).to(dtype)

        idx_n = torch.randperm(x_bc_n.shape[0])
        x_bc_n = x_bc_n[idx_n].view(x_bc_n.size())
        y_bc_n = y_bc_n[idx_n].view(y_bc_n.size())
        sol_bc_n = sol_bc_n[idx_n].view(sol_bc_n.size())

        x_bc.requires_grad = True
        y_bc.requires_grad = True

        x_bc_n.requires_grad = True
        y_bc_n.requires_grad = True

        if use_data:
            sol= extcat_solution(x, y)
            num_temp = torch.randint(0, len(x), (100,))
            x_t = x[num_temp].detach().data
            y_t = y[num_temp].detach().data
            sol_t = sol[num_temp,...].detach().data
            #print(sol_t)

            x_t.requires_grad = True
            y_t.requires_grad = True

            if neuman_bc:
                return x, y, sol, x_bc, y_bc, sol_bc,  x_bc_n, y_bc_n, sol_bc_n, x_t, y_t, sol_t
            else:
                return x, y, sol, x_bc, y_bc, sol_bc, x_t, y_t, sol_t

        if neuman_bc:
            return x, y, sol, x_bc, y_bc, sol_bc, x_bc_n, y_bc_n, sol_bc_n
        else:
            return x, y, sol, x_bc, y_bc, sol_bc

    # testing
    if train is False:
        x = torch.linspace(xmin, xmax, ntest)
        y = torch.linspace(ymin, ymax, ntest)
        x, y  = torch.meshgrid(x, y)
        x = x.reshape(-1,1).to(device)
        y = y.reshape(-1,1).to(device)

        sol = extcat_solution(x, y)
        sol_tes = sol.to(device).to(dtype)

        x.requires_grad = True
        y.requires_grad = True

        return x, y, sol_tes




def get_Helmholtz(
        params = {'N_f': 50000, 'N_bc':50000, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 100, 'seed': 1234, 'Neuman_bc': False, 'a1':1.0, 'a2': 4.0, 'k':1.0}, 
        train = True, use_data = False):

    dtype, device = initialize(seed = params['seed']) 
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    seed = params['seed']
    ntest = params['ntest']
    if 'Neuman_bc' in params.keys():
        neuman_bc =   params['Neuman_bc']
    else:
        neuman_bc  = False


    # The exact solution
    def extcat_solution(x, y):
        return torch.sin(params['a1'] * torch.pi * x) * torch.sin(params['a2'] * torch.pi * y)

    # training
    xmin, xmax, ymin, ymax = domain_size
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    if train == True:

        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax - xmin) + xmin
        y = data[:,1].reshape(-1,1) * (ymax - ymin) + ymin
        x.requires_grad = True
        y.requires_grad = True

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([torch.ones_like(temp_y) * xmin, temp_x, torch.ones_like(temp_y) * xmax, temp_x])
        y_bc = torch.vstack([temp_y, torch.ones_like(temp_x) * ymin, temp_y, torch.ones_like(temp_x) * ymax])
        u_bc = extcat_solution(x_bc, y_bc).to(device).to(dtype)
        
        x_bc_n = torch.vstack([torch.ones_like(temp_y) * xmin, torch.ones_like(temp_y) * xmax, temp_x])
        y_bc_n = torch.vstack([temp_y, temp_y, torch.ones_like(temp_x) * ymax])

        idx = torch.randperm(x_bc.shape[0])
        x_bc = x_bc[idx].view(x_bc.size())
        y_bc = y_bc[idx].view(y_bc.size())
        u_bc = u_bc[idx].view(u_bc.size())

        sol_bc = u_bc.to(device).to(dtype)
        sol = torch.zeros((N_f, sol_bc.shape[-1])).to(device).to(dtype)
        sol_bc_n = torch.zeros((len(x_bc_n), sol_bc.shape[-1])).to(device).to(dtype)

        idx_n = torch.randperm(x_bc_n.shape[0])
        x_bc_n = x_bc_n[idx_n].view(x_bc_n.size())
        y_bc_n = y_bc_n[idx_n].view(y_bc_n.size())
        sol_bc_n = sol_bc_n[idx_n].view(sol_bc_n.size())

        x_bc.requires_grad = True
        y_bc.requires_grad = True

        x_bc_n.requires_grad = True
        y_bc_n.requires_grad = True

        if use_data:
            sol= extcat_solution(x, y)
            num_temp = torch.randint(0, len(x), (100,))
            x_t = x[num_temp].detach().data
            y_t = y[num_temp].detach().data
            sol_t = sol[num_temp,...].detach().data
            #print(sol_t)

            x_t.requires_grad = True
            y_t.requires_grad = True

            if neuman_bc:
                return x, y, sol, x_bc, y_bc, sol_bc,  x_bc_n, y_bc_n, sol_bc_n, x_t, y_t, sol_t
            else:
                return x, y, sol, x_bc, y_bc, sol_bc, x_t, y_t, sol_t

        if neuman_bc:
            return x, y, sol, x_bc, y_bc, sol_bc, x_bc_n, y_bc_n, sol_bc_n
        else:
            return x, y, sol, x_bc, y_bc, sol_bc

    # testing
    if train is False:
        x = torch.linspace(xmin, xmax, ntest)
        y = torch.linspace(ymin, ymax, ntest)
        x, y  = torch.meshgrid(x, y)
        x = x.reshape(-1,1).to(device)
        y = y.reshape(-1,1).to(device)

        sol = extcat_solution(x, y)
        sol_tes = sol.to(device).to(dtype)

        x.requires_grad = True
        y.requires_grad = True

        return x, y, sol_tes


def get_Sinusoid(
        params = {'N_f': 50000, 'N_bc':50000, 'nu':0.01, 'domain_size':[-3,3,-3,3], 'ntest': 100, 'seed': 1234, 'omega'  : 15.}, 
        train = True, use_data = False):

    dtype, device = initialize(seed = params['seed']) 
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    seed = params['seed']
    ntest = params['ntest']



    # The exact solution
    def extcat_solution(x, y):
        return (torch.sin(x*params['omega']) + torch.sin(y*params['omega']))/params['omega']

    # training
    xmin, xmax, ymin, ymax = domain_size
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    if train == True:

        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax - xmin) + xmin
        y = data[:,1].reshape(-1,1) * (ymax - ymin) + ymin
        x.requires_grad = True
        y.requires_grad = True

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([torch.ones_like(temp_y) * xmin, temp_x, torch.ones_like(temp_y) * xmax, temp_x])
        y_bc = torch.vstack([temp_y, torch.ones_like(temp_x) * ymin, temp_y, torch.ones_like(temp_x) * ymax])
        u_bc = extcat_solution(x_bc, y_bc).to(device).to(dtype)
        
        x_bc_n = torch.vstack([torch.ones_like(temp_y) * xmin, torch.ones_like(temp_y) * xmax, temp_x])
        y_bc_n = torch.vstack([temp_y, temp_y, torch.ones_like(temp_x) * ymax])

        idx = torch.randperm(x_bc.shape[0])
        x_bc = x_bc[idx].view(x_bc.size())
        y_bc = y_bc[idx].view(y_bc.size())
        u_bc = u_bc[idx].view(u_bc.size())

        sol_bc = u_bc.to(device).to(dtype)
        sol = torch.zeros((N_f, sol_bc.shape[-1])).to(device).to(dtype)
        sol_bc_n = torch.zeros((len(x_bc_n), sol_bc.shape[-1])).to(device).to(dtype)

        idx_n = torch.randperm(x_bc_n.shape[0])
        x_bc_n = x_bc_n[idx_n].view(x_bc_n.size())
        y_bc_n = y_bc_n[idx_n].view(y_bc_n.size())
        sol_bc_n = sol_bc_n[idx_n].view(sol_bc_n.size())

        x_bc.requires_grad = True
        y_bc.requires_grad = True

        x_bc_n.requires_grad = True
        y_bc_n.requires_grad = True

        if use_data:
            sol= extcat_solution(x, y)
            num_temp = torch.randint(0, len(x), (100,))
            x_t = x[num_temp].detach().data
            y_t = y[num_temp].detach().data
            sol_t = sol[num_temp,...].detach().data
            #print(sol_t)

            x_t.requires_grad = True
            y_t.requires_grad = True


            return x, y, sol, x_bc, y_bc, sol_bc, x_t, y_t, sol_t


        return x, y, sol, x_bc, y_bc, sol_bc

    # testing
    if train is False:
        x = torch.linspace(xmin, xmax, ntest)
        y = torch.linspace(ymin, ymax, ntest)
        x, y  = torch.meshgrid(x, y)
        x = x.reshape(-1,1).to(device)
        y = y.reshape(-1,1).to(device)

        sol = extcat_solution(x, y)
        sol_tes = sol.to(device).to(dtype)

        x.requires_grad = True
        y.requires_grad = True

        return x, y, sol_tes








def get_data_u_ux(params = 
                                  {'N_f': 50000, 'N_bc':50000, 't':1.0, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 100, 'seed': 1234}, 
                                  train = True, use_data = False):
    
    dtype, device = initialize(seed = params['seed']) 
    nu = params['nu']
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    t = params['t']
    seed = params['seed']
    ntest = params['ntest']

    t = torch.tensor(t)
    
    # The exact solution
    def extcat_solution(x, y, t):
        u = x + y**2 + x*y + x**3 + 10*x*y**2 - 5*torch.log(1+x*y) + 12/(1+ x**2) + 1e-1* torch.exp(x+y) + torch.sin(y*8) - 15
        #u = x**2 + y * torch.sin(x) + x * y
        return u

    # training
    xmin, xmax, ymin, ymax = domain_size
    Sobol1d = Sobol(d = 1, scramble=True, seed = seed)
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    if train == True:
        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax - xmin) + xmin
        y = data[:,1].reshape(-1,1) * (ymax - ymin) + ymin
        
        u = extcat_solution(x, y, t)
        sol = u.to(device).to(dtype)
        x.requires_grad = True
        y.requires_grad = True

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([torch.ones_like(temp_x) * xmin, temp_x, torch.ones_like(temp_x) * xmax, temp_x])
        y_bc = torch.vstack([temp_y, torch.ones_like(temp_y) * ymin, temp_y, torch.ones_like(temp_y) * ymax])

        idx = torch.randperm(x_bc.shape[0])
        x_bc = x_bc[idx].view(x_bc.size())
        y_bc = y_bc[idx].view(y_bc.size())

        u_bc = extcat_solution(x_bc, y_bc, t).to(device).to(dtype)
        sol_bc = u_bc

        x.requires_grad = True
        y.requires_grad = True
        x_bc.requires_grad = True
        y_bc.requires_grad = True


        if use_data:
            d_x = torch.distributions.uniform.Uniform(xmin, xmax)
            d_y = torch.distributions.uniform.Uniform(ymin, ymax)
            x_t, y_t  = d_x.sample((100,1)).to(device), d_y.sample((10,1)).to(device)

            u, v, p = extcat_solution(x_t, y_t, t)
            sol_t = torch.hstack([u,v,p]).to(device).to(dtype)

            x_t.requires_grad = True
            y_t.requires_grad = True

            return x, y, sol, x_bc, y_bc, sol_bc, x_t, y_t, sol_t


        return x, y, sol, x_bc, y_bc, sol_bc

    # testing
    if train is False:
        x = torch.linspace(xmin, xmax, ntest)
        y = torch.linspace(ymin, ymax, ntest)
        x, y  = torch.meshgrid(x, y)
        x = x.reshape(-1,1).to(device)
        y = y.reshape(-1,1).to(device)

        u = extcat_solution(x, y, t)
        sol_tes = u.to(device).to(dtype)

        x.requires_grad = True
        y.requires_grad = True

        return x, y, sol_tes


def get_data_u_ux_zero(params = 
                                  {'N_f': 50000, 'N_bc':50000, \
                                   't':1.0, 'nu':0.01, 'domain_size':[0,1,0,1], \
                                    'ntest': 100, 'seed': 1234, 'sol':'x+y'}, 
                                  train = True, use_data = False):
    
    dtype, device = initialize(seed = params['seed']) 
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    t = params['t']
    seed = params['seed']
    ntest = params['ntest']
    sol_eq = params['sol']

    t = torch.tensor(t)
    
    # The exact solution
    def extcat_solution(x, y, sol):
        x = x
        y = y
        u = eval(sol)
        #u = x**2 + y * torch.sin(x) + x * y
        return u

    # training
    xmin, xmax, ymin, ymax = domain_size
    Sobol1d = Sobol(d = 1, scramble=True, seed = seed)
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    if train == True:
        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax - xmin) + xmin
        y = data[:,1].reshape(-1,1) * (ymax - ymin) + ymin
        
        u = extcat_solution(x, y, sol_eq)
        sol = u.to(device).to(dtype)
        x.requires_grad = True
        y.requires_grad = True

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([torch.ones_like(temp_x) * xmin, temp_x, torch.ones_like(temp_x) * xmax, temp_x])
        y_bc = torch.vstack([temp_y, torch.ones_like(temp_y) * ymin, temp_y, torch.ones_like(temp_y) * ymax])

        idx = torch.randperm(x_bc.shape[0])
        x_bc = x_bc[idx].view(x_bc.size())
        y_bc = y_bc[idx].view(y_bc.size())

        u_bc = extcat_solution(x_bc, y_bc, sol_eq).to(device).to(dtype)
        sol_bc = u_bc

        x.requires_grad = True
        y.requires_grad = True
        x_bc.requires_grad = True
        y_bc.requires_grad = True


        if use_data:
            d_x = torch.distributions.uniform.Uniform(xmin, xmax)
            d_y = torch.distributions.uniform.Uniform(ymin, ymax)
            x_t, y_t  = d_x.sample((9000,1)).to(device), d_y.sample((9000,1)).to(device)

            u  = extcat_solution(x_t, y_t, sol_eq)
            sol_t = torch.hstack([u]).to(device).to(dtype)

            x_t.requires_grad = True
            y_t.requires_grad = True

            return x, y, sol, x_bc, y_bc, sol_bc, x_t, y_t, sol_t


        return x, y, sol, x_bc, y_bc, sol_bc

    # testing
    if train is False:
        x = torch.linspace(xmin, xmax, ntest)
        y = torch.linspace(ymin, ymax, ntest)
        x, y  = torch.meshgrid(x, y)
        x = x.reshape(-1,1).to(device)
        y = y.reshape(-1,1).to(device)

        u = extcat_solution(x, y, sol_eq)
        sol_tes = u.to(device).to(dtype)

        x.requires_grad = True
        y.requires_grad = True

        return x, y, sol_tes




def get_data_u_ux_2d(params = 
                                  {'N_f': 50000, 'N_bc':50000, \
                                   't':1.0, 'nu':0.01, 'domain_size':[0,1,0,1], \
                                    'ntest': 100, 'seed': 1234, 'sol':{'u':'x+y', 'v':'2x+2y'}}, 
                                  train = True, use_data = False):
    
    dtype, device = initialize(seed = params['seed']) 
    domain_size = params['domain_size']
    N_f = params['N_f']
    N_bc = params['N_bc']
    t = params['t']
    seed = params['seed']
    ntest = params['ntest']
    sol_eq = params['sol']

    t = torch.tensor(t)
    
    # The exact solution
    def extcat_solution(x, y, sol):
        def calculate_v(sol):
            x = Symbol('x')
            y = Symbol('y')
            u_exp = sol['u'].replace('torch.','')
            u_exp = simplify(u_exp)
            expr_v = Derivative(integrate(u_exp, y), x, evaluate = True)
            v = -1 * expr_v #expr_v.replace('torch.','')
            v = simplify(v)
            return v

        x = x
        y = y
        u = eval(sol['u'])
        
        #v = eval(sol['v'])
        v = eval(str(calculate_v(sol)))
        sol = torch.cat([u,v], axis = -1)
        #u = x**2 + y * torch.sin(x) + x * y
        return sol

    # training
    xmin, xmax, ymin, ymax = domain_size
    Sobol1d = Sobol(d = 1, scramble=True, seed = seed)
    Sobol2d = Sobol(d = 2, scramble=True, seed = seed)
    if train == True:
        data = Sobol2d.random(N_f)
        data = torch.tensor(data, dtype = dtype, device=device)
        x = data[:,0].reshape(-1,1) * (xmax - xmin) + xmin
        y = data[:,1].reshape(-1,1) * (ymax - ymin) + ymin
        
        sol = extcat_solution(x, y, sol_eq)
        sol = sol.to(device).to(dtype)
        x.requires_grad = True
        y.requires_grad = True

        temp_x = torch.linspace(xmin, xmax, N_bc).to(device).to(dtype).reshape(-1,1)
        temp_y = torch.linspace(ymin, ymax, N_bc).to(device).to(dtype).reshape(-1,1)

        x_bc = torch.vstack([torch.ones_like(temp_x) * xmin, temp_x, torch.ones_like(temp_x) * xmax, temp_x])
        y_bc = torch.vstack([temp_y, torch.ones_like(temp_y) * ymin, temp_y, torch.ones_like(temp_y) * ymax])

        idx = torch.randperm(x_bc.shape[0])
        x_bc = x_bc[idx].view(x_bc.size())
        y_bc = y_bc[idx].view(y_bc.size())

        u_bc = extcat_solution(x_bc, y_bc, sol_eq).to(device).to(dtype)
        sol_bc = u_bc

        x.requires_grad = True
        y.requires_grad = True
        x_bc.requires_grad = True
        y_bc.requires_grad = True


        if use_data:
            d_x = torch.distributions.uniform.Uniform(xmin, xmax)
            d_y = torch.distributions.uniform.Uniform(ymin, ymax)
            x_t, y_t  = d_x.sample((9000,1)).to(device), d_y.sample((9000,1)).to(device)

            u  = extcat_solution(x_t, y_t, sol_eq)
            sol_t = torch.hstack([u]).to(device).to(dtype)

            x_t.requires_grad = True
            y_t.requires_grad = True

            return x, y, sol, x_bc, y_bc, sol_bc, x_t, y_t, sol_t


        return x, y, sol, x_bc, y_bc, sol_bc

    # testing
    if train is False:
        x = torch.linspace(xmin, xmax, ntest)
        y = torch.linspace(ymin, ymax, ntest)
        x, y  = torch.meshgrid(x, y)
        x = x.reshape(-1,1).to(device)
        y = y.reshape(-1,1).to(device)

        u = extcat_solution(x, y, sol_eq)
        sol_tes = u.to(device).to(dtype)

        x.requires_grad = True
        y.requires_grad = True

        return x, y, sol_tes







if __name__ == '__main__':
    from plot import plot_u
    domain_size = [0,1,0,1]
    data_params = {'N_f': 50000, 'N_bc':10000, 't': 1, 'nu':0.002, 
                'domain_size':domain_size, 'ntest': 100, 'seed': 12345, \
                    'sol':'(x*y  + x**3 + 2/(1+ x**2) + torch.sin(y*3)  - 3.0) * (x * y * (x-1)) * 15'}

    # x_c, y_c, sol_c, x_bc, y_bc, sol_bc = get_data_poisson(train=True)
    # x, y, sol_test = get_data_poisson(train=False)
    

    # plot_u(x.reshape(-1,).detach().cpu().numpy(), y.reshape(-1,).detach().cpu().numpy(), 
    # sol_test.reshape(-1,).detach().cpu().numpy())

    # plot_u(x_c.reshape(-1,).detach().cpu().numpy(), y_c.reshape(-1,).detach().cpu().numpy(), 
    # sol_c.reshape(-1,).detach().cpu().numpy())

    # x, y, sol_test = get_data_u_ux(train=False)
    # plot_u(x.reshape(-1,).detach().cpu().numpy(), y.reshape(-1,).detach().cpu().numpy(), 
    # sol_test.reshape(-1,).detach().cpu().numpy())

    x, y, sol_test = get_data_u_ux_zero(train=False, params = data_params)
    plot_u(x.reshape(-1,).detach().cpu().numpy(), y.reshape(-1,).detach().cpu().numpy(), 
    sol_test.reshape(-1,).detach().cpu().numpy())



    print(x)