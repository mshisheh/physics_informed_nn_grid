import torch
import os
import numpy as np
import time

from utils.initialize import initialize
from utils.Data import  get_data_u_ux_zero , get_data_spatial_lid_cavity 
from utils.network import Network
from utils.encoder import Encoder, Encoder_conv
from utils.model import Model , Model_multi_net_seq
from utils.losses import Loss_Functions
from utils.trainer import Trainer
from utils.plot import compare_plot, loss_plot, der_plot, grad_hist_plot

import inspect
import utils.Data

from datetime import date

today = date.today()



# Textual month, day and year	
d2 = today.strftime("%B%d")

print(d2)

#---------------------------------------------
# paramters
seed = 456
features = 32
n_cells = [2, 2, 2]
res = [3, 9]
pde_name = 'NS' # or 
network = 'MR' # or MR
domain_size = [0,1,0,1]
mask = 0.

data_params = {'N_f': 1000, 'N_bc':200, 't': 1, 'nu':0.01, 
               'domain_size':domain_size, 'ntest': 100, 'seed': seed, 'Re':100,\
                'sol':'(15*(x-1)*x*(4*y*torch.sin(2*y)+(8*x*y**2)/(x**4-4*x**3+6*x**2-4*x+21/20)+(8*y)/(x**3+1/20)-100*y))/4', 'Neuman_bc' : False , 'mask':mask}
epochs = 5000
optimizer = 'adam'
dynamic_weight_flag = True
mode = 'cosine'

#------------------------------------------------------------------------------
if network == 'PINN':
    title = f"{d2}_{pde_name}_{network}_{epochs}_{optimizer}_{data_params['N_f']}_Re{data_params['Re']}_dw_{dynamic_weight_flag}"
else:
    title = f"{d2}_cavity_conv_2_32_correct_{pde_name}_{network}_{epochs}_{optimizer}_{data_params['N_f']}_{res}_features_{features}_cells_{n_cells}_Re{data_params['Re']}_dw_{dynamic_weight_flag}_whole_mask{data_params['mask']}_all0.005"
data_func = get_data_spatial_lid_cavity
#------------------------------------------------------------------------------

dtype, device = initialize(seed = data_params['seed'])


#Ws = torch.nn.parameter.Parameter(data=torch.tensor([1. , 1.]), requires_grad=True).to(device)

cascade = False
freeze = False

if cascade:
    models = []
    #f_map = 0
    for  idx , r in enumerate(res):
        # make network
        net = Network(input_dim= 1 * features, layers=[32, 32], output_dim=2)
        # make encoder
        encoder = Encoder(n_features= features, res = [r], n_cells= n_cells, domain_size= domain_size , mode = mode)
        # make model

        models.append(Model(network=net, encoder= encoder))


        model = Model_multi_net_seq(models, device = device)

        # make loss_functions
        loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse')
        # make trainer, 
        trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                            bc_loss_function = loss_functions.bc_loss_one_p, 
                            epochs=epochs, title=title, data_params = data_params, 
                            data_func = data_func, use_data=False, 
                            optimizer=optimizer, loss_functions = loss_functions, 
                            dynamic_weights=dynamic_weight_flag, RAR=False, L2 = False)
            # start fitting:
        trainer()




else:
# make network

    # make encoder
    encoder = Encoder_conv(n_features= features, res = res, n_cells= n_cells, domain_size= domain_size , mode = mode)

    #make network and model

    if 0 in res:
        net = Network(input_dim= 2, output_dim=2, layers=[32,32,32,32,32])
        model = Model(network=net, encoder= None )#, data_params= data_params)
    else:
        net = Network(input_dim= len(res)*features, layers=[32, 32], output_dim=2) 
        model = Model(network=net, encoder= encoder)

                    

        print(model)

    # make loss_functions
    loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse')
    # make trainer, 
    trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                    bc_loss_function = loss_functions.bc_loss_one_p, 
                    epochs=epochs, title=title, data_params = data_params, 
                    data_func = data_func, use_data=False, 
                    optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False, L2 = False)
    # start fitting:
    trainer()


# testing
# Generate data
x, y, u_test = \
    data_func(params=data_params, train=False)

u_predict = model(x, y)

#plotting
compare_plot(x, y, u_test, u_predict, title=title)
time.sleep(3.0)
loss_plot(trainer.train_losses, trainer.val_losses, title=title)
time.sleep(3.0)