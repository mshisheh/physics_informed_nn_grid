#!/bin/bash

# Array of parameter values
#pde_names=("Helmholtz" "Example2" "Example3")

optim_methods=("lbfgs" "adam" "mix")
nf_values=(0 10 100 10000)
nd_values=(0 10 100 10000)

rm submit*
rm error*
rm output*

conda init bash
conda activate myenv

# Array of parameter values
optim_methods=("lbfgs" "adam")
models=("PINNS" "MR" "SR")
res_values=(3 7 11 17 21)
a2=(1 2 4 6 8)

# Loop over parameter values
for a2 in "${a2[@]}"; do
  for optim in "${optim_methods[@]}"; do
    for model in "${models[@]}"; do
      for nf_value in "${nf_values[@]}"; do
        for nd_values in "${nd_values[@]}"; do
          # Create a submit file for each case
          submit_file="submit_${optim}_${model}_nf${nf_value}_nd${nd_values}.sh"

          echo "#!/bin/bash" > "$submit_file"
          echo "#SBATCH --job-name=Taylor_${optim}_${model}_${a2}" >> "$submit_file"
          echo "#SBATCH -A raminb_lab_gpu" >> "$submit_file"
          echo "#SBATCH -p free-gpu" >> "$submit_file"
          echo "#SBATCH -N 1" >> "$submit_file"
          echo "#SBATCH --gres=gpu:V100:1" >> "$submit_file"
          echo "#SBATCH -t 05:00:00" >> "$submit_file"
          echo "#SBATCH --output=output_${optim}_${model}_nf${nf_value}_nd${nd_values}.log" >> "$submit_file"
          echo "#SBATCH --error=error_${optim}_${model}_nf${nf_value}_nd${nd_values}.log" >> "$submit_file"

          # Handle --res value based on the model name
          if [[ $model == "PINNS" ]]; then
            echo "python main.py --pde-name 'Helmholtz' --optim '$optim' --model '$model' --a2 '$a2' --N_f $nf_value --N_d $nd_values" >> "$submit_file"
          elif [[ $model == "SR" ]]; then
            for res_value in "${res_values[@]}"; do
              echo "python main.py --pde-name 'Helmholtz' --optim '$optim' --model '$model' --res $res_value --a2 '$a2' --N_f $nf_value --N_d $nd_values" >> "$submit_file"
            done
          elif [[ $model == "MR" ]]; then
            for ((i = 0; i < ${#res_values[@]}; i++)); do
              res_range=("${res_values[@]:0:$((i + 2))}")
              echo "python main.py --pde-name 'Helmholtz' --optim '$optim' --model '$model' --res ${res_range[*]} --a2 '$a2' --N_f $nf_value --N_d $nd_values" >> "$submit_file"
            done
          fi

          # Create a separate submit file for the case without --dynamic_weight
          submit_file_no_dynamic="submit_${optim}_${model}_nf${nf_value}_nd${nd_values}_no_dynamic.sh"

          cp "$submit_file" "$submit_file_no_dynamic"
          sed -i '/--dynamic_weight/d' "$submit_file_no_dynamic"

          # Submit the job using sbatch for both cases
          sbatch "$submit_file"
          #sbatch "$submit_file_no_dynamic"

          # Wait for a few seconds before submitting the next job (optional)
          sleep 1
        done
      done
    done
  done
done
