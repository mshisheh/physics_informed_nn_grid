#!/bin/bash

# Array of parameter values
#pde_names=("Helmholtz" "Example2" "Example3")

optim_methods=("lbfgs" "adam")

rm submit*
rm error*
rm output*

conda init bash
conda activate myenv

# Array of parameter values
optim_methods=("lbfgs" "adam")
models=("PINNS" "MR" "SR")
res_values=(2 3 5 7 9 11 13 15 17 19 21)

Re_values=(100)

for Re in "${Re_values[@]}"; do
  for optim in "${optim_methods[@]}"; do
    for model in "${models[@]}"; do
      # Create a submit file for each case
      submit_file="submit_${optim}_${model}.sh"

      echo "#!/bin/bash" > "$submit_file"
      echo "#SBATCH --job-name=T_${optim}_${model}_${a2}" >> "$submit_file"
      echo "#SBATCH -A raminb_lab_gpu" >> "$submit_file"
      echo "#SBATCH -p free-gpu" >> "$submit_file"
      echo "#SBATCH -N 1" >> "$submit_file"
      echo "#SBATCH --gres=gpu:V100:1" >> "$submit_file"
      echo "#SBATCH -t 05:00:00" >> "$submit_file"
      echo "#SBATCH --output=output_${optim}_${model}.log" >> "$submit_file"
      echo "#SBATCH --error=error_${optim}_${model}.log" >> "$submit_file"
      echo "" >> "$submit_file"
      echo "conda activate your-environment" >> "$submit_file"

      # Handle --res value based on the model name
      if [[ $model == "PINNS" ]]; then
        echo "python main2.py --pde-name 'NS' --optim '$optim' --model '$model' --Re '$Re' --domain_size 0 6.28 0 6.28  --dynamic_weight" >> "$submit_file"
	sbatch "$submit_file"
      elif [[ $model == "SR" ]]; then
        for res_value in "${res_values[@]}"; do
          echo "python main2.py --pde-name 'NS' --optim '$optim' --model '$model' --res $res_value --Re '$Re' --domain_size 0 6.28 0 6.28  --dynamic_weight" >> "$submit_file"
	  sbatch "$submit_file"
        done
      elif [[ $model == "MR" ]]; then
        for ((i = 0; i < ${#res_values[@]}; i++)); do
          res_range=("${res_values[@]:0:$((i + 2))}")
          echo "python main2.py --pde-name 'NS' --optim '$optim' --model '$model' --res ${res_range[*]} --Re '$Re' --domain_size 0 6.28 0 6.28  --dynamic_weight" >> "$submit_file"
	  sbatch "$submit_file"
        done
      fi

      # Wait for a few seconds before submitting the next job (optional)
      sleep 2
    done
   done
done
