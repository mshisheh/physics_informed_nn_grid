import torch
import os
import numpy as np
import time

from utils.initialize import initialize
from utils.Data import get_burgers
from utils.network import Network 
from utils.encoder import Encoder , Encoder_conv_skip, Encoder_conv_ave
from utils.model import Model 
from utils.losses import Loss_Functions
from utils.trainer4 import Trainer
from utils.plot import compare_plot_h, compare_plot_u, loss_plot, der_plot, grad_hist_plot , f_plot

#import inspect
#import utils.Data

from datetime import date

today = date.today()



# Textual month, day and year	
d2 = today.strftime("%B%d")


#---------------------------------------------
# paramters
seed = 456
features = 32
n_cells = [2, 2, 2, 2]
res = [32]
q= 3

pde_name = 'Burgers' # or 
network_name = 'MR' # or MR
domain_size = [-1.,1. ,0.,1.]
data_params = {'N_f': 10000, 'N_bc':10000, 
               'domain_size':domain_size,'ntest': 100, 'seed': seed, 'features' : features, 
               'Neuman_bc': False, 'nu':0.01/ np.pi}
epochs = 50000
optimizer_test = 'adam'
optimizer = 'adam'
dynamic_weight_flag = True
hbc_flag = False
evo = False
data_flag = False

ae = False

data_func = get_burgers
#------------------------------------------------------------------------------
if network_name == 'PINN':
    title = f"{d2}_{pde_name}_{network_name}_{epochs}_{optimizer}_{data_params['N_f']}_dw_{dynamic_weight_flag}"
else:
    title = f"{d2}_{pde_name}_generational_4_32_1e-2_{network_name}_{epochs}_{optimizer}_{data_params['N_f']}_{res}_features_{features}_cells_{n_cells}_dw_{dynamic_weight_flag}_nu_{data_params['nu']}"
#------------------------------------------------------------------------------

# initialize
dtype, device = initialize(seed = data_params['seed'])

enc_f =  Encoder_conv_skip(n_features= features, res = res, n_cells= n_cells, domain_size= domain_size , mode = 'cosine', q = q )
network = Network(input_dim= len(res)*features , layers=[32,32, 32, 32], output_dim=1)

model2 = Model(encoder=enc_f , network=network)
loss_functions = Loss_Functions(model=model2, name = pde_name, params=data_params, loss_type='mse')
trainer3 = Trainer(model=model2, pde_loss_function=loss_functions.pde_loss, 
            bc_loss_function = loss_functions.ic_bc, lr = 1e-3,
            epochs= epochs, title=title, data_params = data_params, 
            data_func = data_func, update_data = False, use_data=False, 
            optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False , evo = evo , active_res= res)

trainer3()

print(model2)

loss_plot(trainer3.train_losses, trainer3.val_losses, title=title)

# testing
# Generate data
x, y, u_test = \
    data_func(params=data_params, train=False)
print(x.shape , y.shape , u_test.shape)

u_predict = model2(x, y)

#plotting
compare_plot_u(x, y, u_test, u_predict, title=title)