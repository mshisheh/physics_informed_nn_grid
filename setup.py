from setuptools import setup, find_packages

setup(
    name='MR-PINNS',
    version='1.0',
    packages=find_packages()
)