import torch
import os
import numpy as np
import time

from utils.initialize import initialize
from utils.Data import get_data_spatial_lid_cavity
from utils.network import Network
from utils.encoder import Encoder
from utils.model import Model
from utils.losses import Loss_Functions
from utils.trainer import Trainer
from utils.plot import compare_plot, loss_plot

#---------------------------------------------
# paramters
seed = 2456
features = 4
res = [8, 12, 18, 32]
domain_size = [-1,1,0,2]
data_params = {'N_f': 50000, 'N_bc':50000, 'nu':0.002, 'domain_size':domain_size, 'ntest': 100, 'seed': seed}
epochs = 2000
title = f'Cavity_PINN_{epochs}_derivitives_0_2pi_mix_RAR_50_10x_pde_loss'
data_func = get_data_spatial_lid_cavity
#---------------------------------------------

# initialize
dtype, device = initialize(seed = data_params['seed'])

# make network
net = Network(input_dim= 2, output_dim=5, layers=[32,32,32,32])
# make model
model = Model(network=net, encoder= None)
# make loss_functions
loss_functions = Loss_Functions(model=model, name = 'NS', params={'Re':1/data_params['nu']}, loss_type='logcosh')
# make trainer, 
trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss_derivitives_input, 
                  bc_loss_function = loss_functions.bc_loss_one_p, 
                  epochs=epochs, title=title, data_params = data_params, 
                  data_func = data_func, update_data = True, use_data=False, 
                  optimizer='mix', loss_functions = loss_functions, dynamic_weights=False)
# start fitting:
trainer()

# testing
# Generate data
x, y, u_test = \
    get_data_spatial_lid_cavity(params=data_params, train=False)

u_predict = model(x, y)


#plotting
compare_plot(x, y, u_test, u_predict, title=title)
time.sleep(5.0)
loss_plot(trainer.train_losses, trainer.val_losses, title=title)