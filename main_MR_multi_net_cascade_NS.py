import torch
import os
import numpy as np
import time

from utils.initialize import initialize
from utils.Data import get_data_spatial_lid_cavity
from utils.network import Network
from utils.encoder import Encoder
from utils.model import Model, Model_multi_net_seq
from utils.losses import Loss_Functions
from utils.trainer import Trainer
from utils.plot import compare_plot, loss_plot, der_plot

#---------------------------------------------
# paramters
seed = 456
features = 4
n_cells = 2
res = [0]
domain_size =  [-1,1,0,2]
data_params = {'N_f': 50000, 'N_bc':10000, 'domain_size':domain_size, 'ntest': 100, 'seed': seed, 'Re':500}
epochs = 200
optimizer = 'lbfgs'
title = f"cavity_MR_No_derivitive_{epochs}_{optimizer}_{data_params['N_f']}_{res}_domain_{domain_size}_features_{features}_Re_{data_params['Re']}_data_bc_pde_1000"
data_func = get_data_spatial_lid_cavity
#---------------------------------------------__-------------------------__------------------------__----------------------------

# initialize
dtype, device = initialize(seed = data_params['seed'])


models = []

for r in res:
    # make network
    if r != 0:
        net = Network(input_dim= 1 * features, layers=[32, 32], output_dim=2)
        # make encoder
        encoder = Encoder(n_features= features, res = [r], n_cells= n_cells, domain_size= domain_size)
    else:
        net = Network(input_dim= 1 * 2, layers=[64, 64, 64, 64], output_dim=2)
        encoder = None
    # make model

    models.append(Model(network=net, encoder= encoder))


    model = Model_multi_net_seq(models, device = device, res = res)

    # make loss_functions
    loss_functions = Loss_Functions(model=model, name = 'NS', params=data_params, loss_type='logcosh')
    # make trainer, 
    trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                    bc_loss_function = loss_functions.bc_loss_one_p, 
                    epochs=epochs, title=title, data_params = data_params, 
                    data_func = data_func, update_data = True, use_data=True, 
                    optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=False, RAR=False, high_res_data=False)
    # start fitting:
    trainer()


#-------------------------------------------------------------------------------------------------------------#

# for r in res:

#     trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
#                     bc_loss_function = loss_functions.bc_loss_one_p, 
#                     epochs=epochs, title=title, data_params = data_params, 
#                     data_func = data_func, update_data = True, use_data=False, 
#                     optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=False, RAR=False, high_res_data=False)
#     # start fitting:
#     trainer()





# testing
# Generate data
x, y, u_test = \
    data_func(params=data_params, train=False)

u_predict = model(x, y)


#plotting
compare_plot(x, y, u_test, u_predict, title=title)
time.sleep(3.0)
loss_plot(trainer.train_losses, trainer.val_losses, title=title)

#der_plot(x, y , model, title=title)