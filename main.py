import argparse
import os
import datetime as date
from utils.initialize import initialize
from utils.Data import get_Helmholtz
from utils.network import Network
from utils.encoder import Encoder
from utils.model import Model , Model_multi_net_seq
from utils.losses import Loss_Functions
from utils.trainer import Trainer
from utils.plot import *
import time


today = date.datetime.now()
# Textual month, day and year	
d2 = today.strftime("%B%d_%Y_%H")



# Configure
parser = argparse.ArgumentParser(description="This model runs python code for running MR, one resolution and PINNS for different examples")

# PDE
parser.add_argument('--pde-name', type = str, default = 'None', 
                    help = 'choose the examples you want to solve,(1) Helmholtz for 2D helmholz (2)  (3) (4) (5)', required = True)
parser.add_argument('--a1', type=float, default=1.0, help = 'sin(a1*pi*x)' )
parser.add_argument('--a2', type=float, default=1.0, help = 'sin(a2*pi*y)')
parser.add_argument('--a3', type=float, default=1.0, help = 'sin(a3*pi*z)')

# Training arguments 
parser.add_argument('--seed', type = int, default=123456, help = 'Random seed' )
parser.add_argument('--lr', type = float, default= 1e-2, help = 'learning rate')

parser.add_argument('--model', type = str, default = 'None', help = 'Choose between MR for multi-resolution, SR for single resolution or PINNS', required = True)
parser.add_argument('--epochs', type = int, default = 1235543, help = 'The default value for LBFGS is 100 and for Adam is 5000. The 1235543 is fake') # 1235543 this is just a number to 
parser.add_argument('--optim', type = str, default = 'Adam', help = 'Optimizer type, use Adam, LBFGS or Mix')
parser.add_argument('--interpolation', type=str, default='cosine', help = 'Type of interpolation')
parser.add_argument('--dynamic_weight', action= 'store_true', 
                    help = "if the user includes --dynamic-flag in the command-line arguments, the dynamic weight is activated. If the user doesn't include it, there is no dynamic weight")



# MR parameters
parser.add_argument('--res', type=int, default=[3,7,11], nargs='+', help ='Number of resolutions for MR, default is [3,7,11]')
parser.add_argument('--features', type=int, default=4, help ='Number of features')
parser.add_argument('--ncell', type=int, default=2, help ='Number of repeatition of encoders')



# Physics informed 
parser.add_argument('--N_f', type = int, default = 20000, help='Number of collocation points')
parser.add_argument('--N_bc', type = int, default = 10000, help='Number of bc points' )
parser.add_argument('--domain_size', type = int, nargs = 4, default = [-1, 1, -1, 1], metavar= 'value' ,
                    help= "min max for domain. call like --domain-size -1 1 -1 1 ('x_min', 'x_max', 'y_min', 'y_max')" )
parser.add_argument('--N_d', type = int, default = 0, help='Number of data points')


def _create_save_directory(output_name):
    # log for logging purposes
    if not os.path.isdir("./log/"):
        os.mkdir("./log")

    # plots for plotting purposes
    if not os.path.exists(f"./plots/"):
        os.mkdir("./plots/")

    if not os.path.exists(f"./plots/predictions/"):
        os.mkdir("./plots/predictions/")

    if not os.path.exists(f"./plots/predictions/jpgs/"):
        os.mkdir("./plots/predictions/jpgs/")

    if not os.path.exists(f"./plots/predictions/csvs/"):
        os.mkdir("./plots/predictions/csvs/")

    if not os.path.exists(f"./plots/loss/"):
        os.mkdir("./plots/loss/")

    if not os.path.exists(f"./plots/loss/jpgs/"):
        os.mkdir("./plots/loss/jpgs/")

    if not os.path.exists(f"./plots/loss/csvs/"):
        os.mkdir("./plots/loss/csvs/")

    if not os.path.exists(f"./saved_models/"):
        os.mkdir(f"./saved_models/" )



def _trainer(args, data_func, device, title):

    print(args.optim)
    domain_size = [args.domain_size[0], args.domain_size[1], args.domain_size[2], args.domain_size[3]]
    print(domain_size)
    optimizer = args.optim
    dynamic_weight_flag = args.dynamic_weight
    mode = args.interpolation
    pde_name = args.pde_name

    if args.N_d == 0:
        use_data_flag = False
    else:
        use_data_flag = True

    print(f'use data flag is {use_data_flag}')
    data_params = {'N_f': args.N_f, 'N_bc':args.N_bc, 'N_d': args.N_d, 
               'domain_size':domain_size,'ntest': 100, 'seed': args.seed, 
               'Neuman_bc': False, 'a1':args.a1, 'a2': args.a2, 'k':1.0}
    
    model_type = args.model
    epochs = args.epochs
    features = args.features
    n_cells = [args.ncell] * len(args.res)
    res = [int(args.res[i]) for i in range(len(args.res))]



    seed = 456
    features = 4
    pde_name = 'Helmholtz' # or 


    cascade = False
    freeze = False

    if cascade and model_type in ['MR', 'SR']:
        models = []
        #f_map = 0
        for  idx , r in enumerate(res):
            # make network
            net = Network(input_dim= 1 * features, layers=[16, 16], output_dim=1)
            # make encoder
            encoder = Encoder(n_features= features, res = [r], n_cells= n_cells, domain_size= domain_size , mode = mode)
            # make model

            models.append(Model(network=net, encoder= encoder))
            model = Model_multi_net_seq(models, device = device)

            with open( f'./log/history_{title}.txt', 'w') as f:
                f.write('\n' + f"The {model_type} model used is: \n" + f'{model}' + '\n')

            # make loss_functions
            loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse')
            # make trainer, 
            trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                            bc_loss_function = loss_functions.bc_loss, 
                            epochs=epochs, title=title, data_params = data_params, 
                            data_func = data_func, update_data = True, use_data=use_data_flag, 
                            optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False)
            # start fitting:
            trainer()




    elif model_type in ['MR', 'SR']:
    # make network

        # make encoder
        encoder = Encoder(n_features= features, res = res, n_cells= n_cells, domain_size= domain_size , mode = mode)

        #make network and model

        if 0 in res:
            net = Network(input_dim= 2, output_dim=1, layers=[32,32,32,32,32])
            model = Model(network=net, encoder= None , data_params= data_params)
        else:
            net = Network(input_dim= len(res)*features, layers=[16, 16], output_dim=1) 
            model = Model(network=net, encoder= encoder)

        # print(model)
        with open( f'./log/history_{title}.txt', 'w') as f:
            f.write('\n' + f"The {model_type} model used is: \n" + f'{model}' + '\n')

        # make loss_functions
        loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse')
        # make trainer, 
        trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                        bc_loss_function = loss_functions.bc_loss, 
                        epochs=epochs, title=title, data_params = data_params, 
                        data_func = data_func, update_data = False, use_data=use_data_flag, 
                        optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False)
        # start fitting:
        trainer()

    elif model_type == 'PINNS':
        net = Network(input_dim= 2, output_dim=1, layers=[32,32,32,32,32])
        model = Model(network=net, encoder= None)


        with open( f'./log/history_{title}.txt', 'w') as f:
            f.write('\n' + f"The {model_type} model used is: \n" + f'{model}' + '\n')

        loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse')
        # make trainer, 
        trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                        bc_loss_function = loss_functions.bc_loss, 
                        epochs=epochs, title=title, data_params = data_params, 
                        data_func = data_func, update_data = True, use_data=use_data_flag, 
                        optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False)
        # start fitting:
        trainer()


    else:
        raise ValueError('--model is wrong')

    return trainer, model, data_params




def _testing(trainer, model, data_func, data_params, title):
    x, y, u_test = \
    data_func(params=data_params, train=False)

    u_predict = model(x, y)


    #plotting

    compare_plot_u(x, y, u_test, u_predict, title=title)
    time.sleep(3.0)
    loss_plot(trainer.train_losses, trainer.val_losses, title=title)
    time.sleep(3.0)

    with open(f'./log/history_{title}.txt', 'a') as f:
        f.write('\n'+f"Total time is {trainer.time_loss[-1]}")

def main(args, different_keys):
    if args.epochs == 1235543:
        if args.optim.lower() == 'lbfgs':
            args.epochs = 151
        elif args.optim.lower() == 'adam':
            args.epochs = 5001
    else:
        args.epochs += 1

    #----------------------------------------------------------------
    # Now let's setup the case here
    #----------------------------------------------------------------


    # output name will be used to save all the names for plots and etc
    output_name = d2 + "_" + "_".join([f"{str(key)}_{str(key_value[key])}" for key_value in different_keys for key in key_value])


    # Initialize
    dtype, device = initialize(seed = args.seed)

    # Making the directories
    _create_save_directory(output_name)

    # training
    trainer, model, data_params = _trainer(args, data_func = get_Helmholtz, device = device, title  = output_name)

    _testing(trainer=trainer, model = model, data_func = get_Helmholtz, data_params=data_params, title  = output_name)


if __name__ == '__main__':

    args = parser.parse_args()
    default_dict = {parser._actions[i].dest:parser._actions[i].default for i in range(len(parser._actions))}
    values_dict = vars(args)
    different_keys = [{key: values_dict[key]} for key in values_dict.keys() if values_dict[key] != default_dict[key] ]
    # print(different_keys)
    
    main(args, different_keys)