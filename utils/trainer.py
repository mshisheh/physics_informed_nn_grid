import torch
import torch.nn as nn
import time
from torch.utils.data import TensorDataset, DataLoader
from utils.Data import get_dp_from_model, get_dp_from_model2
import numpy as np
import os


class Trainer(nn.Module):
    def __init__(
        self, 
        model, 
        pde_loss_function = None,
        bc_loss_function = None, 
        train_size = 0.8,
        epochs = 50, 
        optimizer = 'lbfgs',
        lr = 1.0,
        title = 'default',
        data_params = {'N_f': 50000, 'N_bc':50000, 'N_d':1000, 't':1.0, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 100, 'seed': 1234, 'Neuman_bc': True}, 
        data_func = None, 
        update_data = False,
        use_data = False,
        loss_functions = None,
        dynamic_weights = False, 
        RAR = False,
        device = 'cuda',
        ewc = None,
        use_dp = False,
        high_res_data = False,
        Annealing = False,
        Ws = None,
        L2 = False,
        ) -> None:
        super().__init__()
        
        #print(optimizer, type(optimizer))
        self.optim_type = optimizer
        self.model = model
        self.pde_loss_function = pde_loss_function
        self.bc_loss_function = bc_loss_function
        self.loss_functions = loss_functions
        self.dynamic_weights = dynamic_weights
        self.alpha = 1.0
        self.beta = 1.0
        self.lambdaa = 0.9
        self.RAR = RAR
        self.device = device
        self.ewc = ewc
        self.use_dp = use_dp
        self.coeff_bc = 1.0
        self.coeff_pde = 1.0
        self.Ws = Ws
        self.L2 = L2
        

        if not os.path.exists(f'./saved_models/{title}/'):
            os.mkdir(f'./saved_models/{title}/')




        self.annealing = Annealing
        self.T = 0.05
        self.cooling_factor = 0.95

        if self.optim_type.lower() == 'lbfgs' or self.optim_type.lower() == 'mix':
            self.optim = torch.optim.LBFGS(
            model.parameters(), 
            lr=lr, 
            # max_iter=10000,
            # max_eval=10000,
            # history_size=50,
            # tolerance_grad=1e-12,
            # tolerance_change=1.0 * np.finfo(float).eps,
            line_search_fn="strong_wolfe",
            )
        else:
            self.optim = None


        self.use_data = use_data
        self.train_size = train_size
        self.neuman_bc = False#data_params['Neuman_bc']

        if use_data:
            if self.neuman_bc:
                x_c, y_c, u_c, x_bc, y_bc, u_bc, x_bc_n, y_bc_n, u_bc_n, x_d, y_d, u_d = data_func(data_params, train = True, use_data = True)
                self.x_d, self.y_d, self.u_d = x_d, y_d, u_d
                size_bc_n = int(len(x_bc_n) * self.train_size)
            else:
                x_c, y_c, u_c, x_bc, y_bc, u_bc, x_d, y_d, u_d = data_func(data_params, train = True, use_data = True)
                self.x_d, self.y_d, self.u_d = x_d, y_d, u_d
        else:
            if self.neuman_bc:
                x_c, y_c, u_c, x_bc, y_bc, u_bc, x_bc_n, y_bc_n, u_bc_n = data_func(data_params, train = True)
                size_bc_n = int(len(x_bc_n) * self.train_size)
            else:
                x_c, y_c, u_c, x_bc, y_bc, u_bc = data_func(params = data_params, train = True)
        
        
        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)
        self.epochs = epochs
        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:]
        self.data_params = data_params
        self.data_func = data_func

        self.train_losses = {"bc": [], "pde": [], "data": [], "total":[]}
        self.val_losses = {"bc": [], "pde": [], "data": [], "total":[]}
        self.gradient_loss_total = {'pde':[], 'bc':[]}
        self.gradient_loss_network = {'pde':[], 'bc':[]}
        self.gradient_loss_encoder = {'pde':[], 'bc':[]}
        self.title = title
        self.time_loss = []

        self.count = 0
        self.data_exist = high_res_data

        ####################################### Neumann BC ############################
        if self.neuman_bc:
            self.xtrain_bc_n,  self.ytrain_bc_n,  self.utrain_bc_n =  x_bc_n[:size_bc_n],  y_bc_n[:size_bc_n],  u_bc_n[:size_bc_n]
            self.xval_bc_n,  self.yval_bc_n,  self.uval_bc_n =  x_bc_n[size_bc_n:],  y_bc_n[size_bc_n:],  u_bc_n[size_bc_n:]
        ##################################### add data from prior grids ###############
        if self.data_exist:
            domain_size = self.data_params['domain_size']
            xmin, xmax, ymin, ymax = domain_size
            d_x = torch.distributions.uniform.Uniform(xmin, xmax)
            d_y = torch.distributions.uniform.Uniform(ymin, ymax)
            x_t, y_t  = d_x.sample((10000,1)).to(device), d_y.sample((10000,1)).to(device)
            self.xpdata = x_t
            self.ypdata = y_t
            self.xpdata.requires_grad = True
            self.ypdata.requires_grad = True
            self.sol_data = self.evaluate_from_prior(self.xpdata,  self.ypdata)



        # if self.use_dp:
        #     self.dp_bc = get_dp_from_model2(self.model, self.xtrain_bc, self.ytrain_bc, 1.0/self.data_params['nu'])
        #     #self.utrain_bc = self.dp_bc
        



        if self.pde_loss_function is None:
            self.col_exist = False
        else:
            self.col_exist = True
        
        if self.bc_loss_function is None:
            self.bc_exist = False
            # self.xtrain_c = torch.vstack([self.xtrain_c, self.xtrain_bc])
            # self.ytrain_c = torch.vstack([self.ytrain_c, self.ytrain_bc])
        else:
            self.bc_exist = True


        self.new_dic_col = self.data_params.copy()

        # gate_params = [param for name, param in model.named_parameters() 
        #                if name in [f'encoder.lambda_param.lambda{i}' for i in range(len(self.model.encoder.lambda_param))]]  # replace 'gate_name' with appropriate keyword

        # # Collect all other parameters
        # other_params = [param for name, param in model.named_parameters() 
        #                 if name not in [f'encoder.lambda_param.lambda{i}' for i in range(len(self.model.encoder.lambda_param))]]


        # self.adam = torch.optim.Adam( [{'params': gate_params, 'lr': 1e-1}, {'params': other_params, 'lr': 1e-2}])

        self.adam = torch.optim.Adam(self.model.parameters(), 1e-2)
        self.scheduler = torch.optim.lr_scheduler.MultiStepLR(self.adam, milestones= np.linspace(0,self.epochs,4).tolist(), gamma=0.1)
        #self.scheduler  = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer= self.adam, last_epoch=-1, eta_min = 1e-5, T_max=int(self.epochs/5), verbose=False)
        self.iter = 0



    def update_data(self):
        self.data_params['seed'] = int(torch.randint(1000, 100000, (1,)))

        if self.neuman_bc:
            x_c, y_c, u_c, x_bc, y_bc, u_bc, x_bc_n, y_bc_n, u_bc_n = self.data_func(self.data_params, train = True)
        else:
            x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)
        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:]

        if self.neuman_bc:
            size_bc_n = int(len(x_bc_n) * self.train_size)
            self.xtrain_bc_n,  self.ytrain_bc_n,  self.utrain_bc_n =  x_bc_n[:size_bc_n],  y_bc_n[:size_bc_n],  u_bc_n[:size_bc_n]
            self.xval_bc_n,  self.yval_bc_n,  self.uval_bc_n =  x_bc_n[size_bc_n:],  y_bc_n[size_bc_n:],  u_bc_n[size_bc_n:]
    




    def update_domain_data(self):
        self.count += 1
        div_num = int(self.epochs/15)+1
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        
        xmin, xmax, ymin, ymax = self.data_params['domain_size']

        xmin_new = round(xmin + (xmax - xmin)/10.0/div_num * self.count,4)
        xmax_new = round(xmax - (xmax - xmin)/10.0/div_num * self.count,4)
        ymin_new = round(ymin + (ymax - ymin)/10.0/div_num * self.count,4)
        ymax_new = round(ymax - (ymax - ymin)/10.0/div_num * self.count,4)

        self.new_dic_col['domain_size'] = [xmin_new, xmax_new, ymin_new, ymax_new]
        self.data_exist = True

        x_c_temp, y_c_temp, u_c_temp, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        
        indices = torch.where((x_c_temp < xmin_new) | (x_c_temp > xmax_new) | (y_c_temp < ymin_new) | (y_c_temp > ymax_new))
        
        print(f'new domain is {[xmin_new, xmax_new, ymin_new, ymax_new]}')
        self.new_dic_col['N_f'] = self.data_params['N_f'] - len(indices[0])
        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_col, train = True)

        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)

        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc,:]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:,:]

        x_data = x_c_temp[indices[0],:].detach()
        y_data = y_c_temp[indices[0], :].detach()

        size_bc = int(len(x_data) * self.train_size)

        x_data.requires_grad = True
        y_data.requires_grad = True

        u_predict = self.evaluate(x_data, y_data, include_p = True).detach()

        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]
        self.utrain_data = u_predict[:size_bc]

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]
        self.uval_data = u_predict[size_bc:]

    def update_domain_data_sequential(self):
        self.count += 1
        div_num = int(self.epochs/100)+1
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        
        xmin, xmax, ymin, ymax = self.data_params['domain_size']

        xmin_new_0 = round(xmin + (xmax - xmin)/30.0/div_num * (self.count-1),4)
        xmax_new_0 = round(xmax - (xmax - xmin)/30.0/div_num * (self.count-1),4)
        ymin_new_0 = round(ymin + (ymax - ymin)/30.0/div_num * (self.count-1),4)
        ymax_new_0 = round(ymax - (ymax - ymin)/30.0/div_num * (self.count-1),4)


        xmin_new = round(xmin + (xmax - xmin)/30.0/div_num * self.count,4)
        xmax_new = round(xmax - (xmax - xmin)/30.0/div_num * self.count,4)
        ymin_new = round(ymin + (ymax - ymin)/30.0/div_num * self.count,4)
        ymax_new = round(ymax - (ymax - ymin)/30.0/div_num * self.count,4)

        self.new_dic_col['domain_size'] = [xmin_new, xmax_new, ymin_new, ymax_new]
        if self.count > 1:
            self.data_exist = True
        
        self.col_exist = True

        x_c_temp, y_c_temp, u_c_temp, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)

        indices_col = torch.where(((x_c_temp < xmin_new) & (x_c_temp > xmin_new_0))
        | ((x_c_temp > xmax_new) & (x_c_temp < xmax_new_0)) 
        | ((y_c_temp < ymin_new) & (y_c_temp > ymin_new_0))
        | ((y_c_temp > ymax_new) & (y_c_temp < ymax_new_0)) )

        indices_data = torch.where((x_c_temp < xmin_new_0)
        | (x_c_temp > xmax_new_0)
        | (y_c_temp < ymin_new_0) 
        | (y_c_temp > ymax_new_0))

        print(f'new domain is {[xmin_new, xmax_new, ymin_new, ymax_new]}')
        
        x_c = x_c_temp[indices_col[0],:].detach()
        y_c = y_c_temp[indices_col[0], :].detach()
        u_c = u_c_temp[indices_col[0], :].detach()
        x_bc = x_bc[indices_col[0], :].detach()
        y_bc = y_bc[indices_col[0], :].detach()
        u_bc = u_bc[indices_col[0], :].detach()

        x_c.requires_grad = True
        y_c.requires_grad = True
        u_c.requires_grad = True
        x_bc.requires_grad = True
        y_bc.requires_grad = True
        u_bc.requires_grad = True


        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)

        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc,:]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:,:]

        x_data = x_c_temp[indices_data[0],:].detach()
        y_data = y_c_temp[indices_data[0], :].detach()

        size_bc = int(len(x_data) * self.train_size)

        x_data.requires_grad = True
        y_data.requires_grad = True

        u_predict = self.evaluate(x_data, y_data, include_p = False).detach()

        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]
        self.utrain_data = u_predict[:size_bc]

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]
        self.uval_data = u_predict[size_bc:]


    def update_domain_data_3x(self):
        self.count += 1
        div_num = int(self.epochs/20)+1
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        
        xmin, xmax, ymin, ymax = self.data_params['domain_size']
        # evey 15 * 3 = 45 updated
        if self.count % 3 == 0:
            xmin_new = xmin + (xmax - xmin)/3.0/div_num * self.count
            xmax_new = xmax - (xmax - xmin)/3.0/div_num * self.count
            ymin_new = ymin + (ymax - ymin)/3.0/div_num * self.count
            ymax_new = ymax - (ymax - ymin)/3.0/div_num * self.count
            self.new_dic_col['domain_size'] = [xmin_new, xmax_new, ymin_new, ymax_new]
            self.data_exist = True
        else:
            xmin_new, xmax_new, ymin_new, ymax_new = self.new_dic_col['domain_size'] 
        # self.new_dic_col['N_f'] = max(self.data_params['N_f'] - int(self.data_params['N_f']/div_num) * self.count, 10000)

        x_c_temp, y_c_temp, u_c_temp, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        
        indices = torch.where((x_c_temp < xmin_new) | (x_c_temp > xmax_new) | (y_c_temp < ymin_new) | (y_c_temp > ymax_new))
        # all_index = torch.arange(len(x_c_temp)).to(indices[0].device)
        # col_indices = all_index[~torch.isin(all_index, indices[0])]
        
        print(f'new domain is {[xmin_new, xmax_new, ymin_new, ymax_new]}')
        self.new_dic_col['N_f'] = self.data_params['N_f'] - len(indices[0])
        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_col, train = True)

        # x_c = x_c_temp[col_indices].detach()
        # y_c = y_c_temp[col_indices].detach()
        # u_c = u_c_temp[col_indices].detach()

        # x_c.requires_grad = True
        # y_c.requires_grad = True
        # u_c.requires_grad = True


        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)

        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc,:]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:,:]


        # xmax_new = xmin_new
        # ymax_new = ymin_new
        # self.new_dic_data['domain_size'] = [xmin, xmax_new, ymin, ymax_new]
        # self.new_dic_data['N_f'] = min(int(self.data_params['N_f']/div_num) * self.count, self.data_params['N_f'])
        # x_data, y_data, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_data, train = True)

        x_data = x_c_temp[indices[0],:].detach()
        y_data = y_c_temp[indices[0], :].detach()

        size_bc = int(len(x_data) * self.train_size)

        x_data.requires_grad = True
        y_data.requires_grad = True

        u_predict = self.evaluate(x_data, y_data, include_p=False).detach()

        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]
        self.utrain_data = u_predict[:size_bc]

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]
        self.uval_data = u_predict[size_bc:]

        # self.xtrain_bc = torch.vstack([self.xtrain_bc, x_data[:size_bc]])
        # self.ytrain_bc = torch.vstack([self.ytrain_bc, y_data[:size_bc]])
        # self.utrain_bc = torch.vstack([self.utrain_bc, u_predict[:size_bc,:2]])

        # self.xval_bc = torch.vstack([self.xval_bc, x_data[size_bc:]])
        # self.yval_bc = torch.vstack([self.yval_bc, y_data[size_bc:]])
        # self.uval_bc = torch.vstack([self.uval_bc, u_predict[size_bc:,:2]])

    def update_domain_data_selective_pde_loss(self):
        self.count += 1
        div_num = int(self.epochs/20)+1
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        
        xmin, xmax, ymin, ymax = self.data_params['domain_size']
        # evey 15 * 3 = 45 updated
        if self.count % 3 == 0:
            xmin_new = xmin + (xmax - xmin)/3.0/div_num * self.count
            xmax_new = xmax - (xmax - xmin)/3.0/div_num * self.count
            ymin_new = ymin + (ymax - ymin)/3.0/div_num * self.count
            ymax_new = ymax - (ymax - ymin)/3.0/div_num * self.count
            self.new_dic_col['domain_size'] = [xmin_new, xmax_new, ymin_new, ymax_new]
            self.data_exist = True
        else:
            xmin_new, xmax_new, ymin_new, ymax_new = self.new_dic_col['domain_size'] 

        x_c_temp, y_c_temp, u_c_temp, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        
        indices = torch.where((x_c_temp < xmin_new) | (x_c_temp > xmax_new) | (y_c_temp < ymin_new) | (y_c_temp > ymax_new))

        
        print(f'new domain is {[xmin_new, xmax_new, ymin_new, ymax_new]}')
        self.new_dic_col['N_f'] = self.data_params['N_f'] - len(indices[0])
        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_col, train = True)



        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)

        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc,:]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:,:]


        if self.data_exist:
            x_data = x_c_temp[indices[0],:].detach()
            y_data = y_c_temp[indices[0], :].detach()

            size_bc = int(len(x_data) * self.train_size)

            x_data.requires_grad = True
            y_data.requires_grad = True

            u_predict = self.evaluate(x_data, y_data, include_p=False).detach()
            _, uterm, vterm = self.pde_loss_function(x_data, y_data, return_pde_terms = True)
            sum_error = torch.abs(uterm) + torch.abs(vterm)
            q20 = torch.quantile(sum_error, q = 0.2)
            idx = torch.where(sum_error <= q20)[0]
            x_data.requires_grad = False
            y_data.requires_grad = False

            x_data = x_data[idx]
            y_data = y_data[idx]
            u_predict = u_predict[idx, :]


            x_data.requires_grad = True
            y_data.requires_grad = True

            self.xtrain_data = x_data[:size_bc]
            self.ytrain_data = y_data[:size_bc]
            self.utrain_data = u_predict[:size_bc]

            self.xval_data = x_data[size_bc:]
            self.yval_data = y_data[size_bc:]
            self.uval_data = u_predict[size_bc:]


    def update_selective_pde_loss(self):
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        self.data_exist = True

        x_c_temp, y_c_temp, _, _, _, _ = self.data_func(self.data_params, train = True)

        if self.data_exist:
            x_data = x_c_temp.detach()
            y_data = y_c_temp.detach()

            size_bc = int(len(x_data) * self.train_size)

            x_data.requires_grad = True
            y_data.requires_grad = True

            u_predict = self.evaluate(x_data, y_data, include_p=True).detach()
            _, uterm, vterm = self.pde_loss_function(x_data, y_data, return_pde_terms = True)
            sum_error = torch.abs(uterm) + torch.abs(vterm)
            q20 = torch.quantile(sum_error, q = 0.01)
            idx = torch.where(sum_error <= q20)[0]
            x_data.requires_grad = False
            y_data.requires_grad = False

            x_data = x_data[idx]
            y_data = y_data[idx]
            u_predict = u_predict[idx, :]


            x_data.requires_grad = True
            y_data.requires_grad = True

            self.xtrain_data = x_data[:size_bc]
            self.ytrain_data = y_data[:size_bc]
            self.utrain_data = u_predict[:size_bc]

            self.xval_data = x_data[size_bc:]
            self.yval_data = y_data[size_bc:]
            self.uval_data = u_predict[size_bc:]

    def add_col_pde_loss(self):
        # self.params2 = self.data_params.copy()
        # self.params2['seed'] = int(torch.randint(1000, 1000000, (1,)))
        # self.params2['N_f'] = 1000
        # self.params2['domain_size'] = [0.0, 1.0, 0.65, 1.0]

        x_c_temp, y_c_temp, u_c_temp, _, _, _ = self.data_func(self.data_params, train = True)

        x_data = x_c_temp.detach()
        y_data = y_c_temp.detach()

        x_data.requires_grad = True
        y_data.requires_grad = True

        total, N1, N2, ux, uy = self.pde_loss_function(x_data, y_data, return_pde_terms = True)
        sum_error = torch.abs(ux) + torch.abs(uy)
        q20 = torch.quantile(sum_error, q = 0.99)
        idx = torch.where(sum_error >= q20)[0] #torch.randperm(len(x_c_temp)) #
        x_data.requires_grad = False
        y_data.requires_grad = False

        x_data = x_data[idx]
        y_data = y_data[idx]

        x_data.requires_grad = True
        y_data.requires_grad = True

        size_bc = int(len(x_data) * self.train_size)

        
        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]

        self.xtrain_c = torch.concat([self.xtrain_c, self.xtrain_data], axis = 0)
        self.ytrain_c = torch.concat([self.ytrain_c, self.ytrain_data], axis = 0)

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]

        self.xval_c = torch.concat([self.xval_c, self.xval_data], axis = 0)
        self.yval_c = torch.concat([self.yval_c, self.yval_data], axis = 0)

        
        if self.data_exist:
            domain_size = self.data_params['domain_size']
            xmin, xmax, ymin, ymax = domain_size
            sol_data = self.evaluate_from_prior(x_data,  y_data)
            self.xpdata = torch.concat([self.xpdata, x_data], axis = 0)
            self.ypdata = torch.concat([self.ypdata, y_data], axis = 0)
            self.sol_data = torch.concat([self.sol_data, sol_data], axis = 0)



    def update_domain_data_shrink_once(self):
        self.count += 1
        div_num = int(self.epochs/20)+1
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        self.new_dic_col = self.data_params.copy()
        
        xmin, xmax, ymin, ymax = self.data_params['domain_size']

        xmin_new = xmin + (xmax - xmin)/100.0
        xmax_new = xmax - (xmax - xmin)/100.0
        ymin_new = ymin + (ymax - ymin)/100.0
        ymax_new = ymax - (ymax - ymin)/100.0
        self.new_dic_col['domain_size'] = [xmin_new, xmax_new, ymin_new, ymax_new]

        # self.new_dic_col['N_f'] = max(self.data_params['N_f'] - int(self.data_params['N_f']/div_num) * self.count, 10000)

        x_c_temp, y_c_temp, u_c_temp, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        
        indices = torch.where((x_c_temp < xmin_new) | (x_c_temp > xmax_new) | (y_c_temp < ymin_new) | (y_c_temp > ymax_new))
        # all_index = torch.arange(len(x_c_temp)).to(indices[0].device)
        # col_indices = all_index[~torch.isin(all_index, indices[0])]
        
        print(f'new domain is {[xmin_new, xmax_new, ymin_new, ymax_new]}')
        self.new_dic_col['N_f'] = self.data_params['N_f'] - len(indices[0])
        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_col, train = True)

        # x_c = x_c_temp[col_indices].detach()
        # y_c = y_c_temp[col_indices].detach()
        # u_c = u_c_temp[col_indices].detach()

        # x_c.requires_grad = True
        # y_c.requires_grad = True
        # u_c.requires_grad = True


        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)

        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc,:]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:,:]


        # xmax_new = xmin_new
        # ymax_new = ymin_new
        # self.new_dic_data['domain_size'] = [xmin, xmax_new, ymin, ymax_new]
        # self.new_dic_data['N_f'] = min(int(self.data_params['N_f']/div_num) * self.count, self.data_params['N_f'])
        # x_data, y_data, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_data, train = True)

        x_data = x_c_temp[indices[0],:].detach()
        y_data = y_c_temp[indices[0], :].detach()

        size_bc = int(len(x_data) * self.train_size)

        x_data.requires_grad = True
        y_data.requires_grad = True

        u_predict = self.evaluate(x_data, y_data, include_p=False).detach()

        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]
        self.utrain_data = u_predict[:size_bc]

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]
        self.uval_data = u_predict[size_bc:]

        # self.xtrain_bc = torch.vstack([self.xtrain_bc, x_data[:size_bc]])
        # self.ytrain_bc = torch.vstack([self.ytrain_bc, y_data[:size_bc]])
        # self.utrain_bc = torch.vstack([self.utrain_bc, u_predict[:size_bc,:2]])

        # self.xval_bc = torch.vstack([self.xval_bc, x_data[size_bc:]])
        # self.yval_bc = torch.vstack([self.yval_bc, y_data[size_bc:]])
        # self.uval_bc = torch.vstack([self.uval_bc, u_predict[size_bc:,:2]])
        self.data_exist = True

    def evaluate(self, x, y, include_p = True):
        sol_p = self.model(x, y)
        psi = sol_p[:,0].reshape(-1,1)
        pp = sol_p[:,1].reshape(-1,1)
        up = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
        vp = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), False, False)[0]
        if include_p:
            return torch.hstack([up, vp, pp])
        else:
            return torch.hstack([up, vp])


    def evaluate_from_prior(self, x, y, include_p = False):
        if len(self.model.model_list) > 1:
            sol_p = self.model.model_list[0](x,y)/(self.model.res[0]+1)
            for i in range(1,len(self.model.model_list)-1):
                sol_p += self.model.model_list[i](x,y)/(self.model.res[i]+1)
            psi = sol_p[:,0].reshape(-1,1)
            pp = sol_p[:,1].reshape(-1,1)
            up = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
            vp = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), False, False)[0]
            if include_p:
                return torch.hstack([up, vp, pp]).detach()
            else:
                return torch.hstack([up, vp]).detach()
        else:
            self.data_exist = False
            return None


    def compute_dynamic_weights2(self):
        delta_pde_teta = torch.cat([p.reshape(-1).cpu() for p in self.gradient_loss_total['pde'][-1]])
        delta_bc_teta = torch.cat([p.reshape(-1).cpu() for p in self.gradient_loss_total['bc'][-1]])

        # if self.use_data:
        #     delta_data_teta = torch.autograd.grad(self.beta * self.data_loss, self.model.parameters(),  retain_graph=True)
        #     values2 = [p.reshape(-1,).cpu().tolist() for p in delta_data_teta]
        #     delta_data_teta_abs = torch.abs(torch.tensor([v for val in values2 for v in val]))
        #     temp2 = torch.mean(delta_pde_teta_abs) / torch.mean(delta_data_teta_abs)
        # else:
        #     temp2 = self.beta

        temp = torch.max(torch.abs(delta_pde_teta)) / torch.mean(torch.abs(delta_bc_teta))

        
        return (1.0 - self.lambdaa) * self.alpha + self.lambdaa * temp, 1.0


    def compute_dynamic_weights(self):

        # # cascading
        # delta_pde_teta = torch.autograd.grad(self.pde_loss, 
        #                                      [param for model in self.model.model_list for param in model.parameters()
        #                                        if param.requires_grad == True], retain_graph=True)
        # values = [p.reshape(-1,).cpu().tolist() for p in delta_pde_teta if p is not None]
        # delta_pde_teta_abs = torch.abs(torch.tensor([v for val in values for v in val]))

        # delta_bc_teta = torch.autograd.grad(self.bc_loss, 
        #                                      [param for model in self.model.model_list for param in model.parameters()
        #                                        if param.requires_grad == True], retain_graph=True)
        # values = [p.reshape(-1,).cpu().tolist() for p in delta_bc_teta if p is not None]
        # delta_bc_teta_abs = torch.abs(torch.tensor([v for val in values for v in val]))

        delta_pde_teta = torch.autograd.grad(self.pde_loss, self.model.parameters(),  retain_graph=True, allow_unused=True)
        values = [p.reshape(-1,).cpu().tolist() for p in delta_pde_teta if p is not None]
        delta_pde_teta_abs = torch.abs(torch.tensor([v for val in values for v in val]))

        delta_bc_teta = torch.autograd.grad(self.bc_loss, self.model.parameters(),  retain_graph=True, allow_unused=True)
        values = [p.reshape(-1,).cpu().tolist() for p in delta_bc_teta if p is not None]
        delta_bc_teta_abs = torch.abs(torch.tensor([v for val in values for v in val]))


        if self.use_data:
            delta_data_teta = torch.autograd.grad(self.data_loss, self.model.parameters(),  retain_graph=True)
            values2 = [p.reshape(-1,).cpu().tolist() for p in delta_data_teta]
            delta_data_teta_abs = torch.abs(torch.tensor([v for val in values2 for v in val]))
            temp2 = torch.mean(delta_pde_teta_abs) / torch.mean(delta_data_teta_abs)
        else:
            temp2 = self.beta

        temp = torch.mean(delta_pde_teta_abs) / torch.mean(delta_bc_teta_abs)

        
        return (1.0 - self.lambdaa) * self.alpha + self.lambdaa * temp, (1.0 - self.lambdaa) * self.beta + self.lambdaa * temp2
    

    def closure(self):
        if self.optim_type.lower() != 'adam':
            self.optim.zero_grad()
        self.adam.zero_grad()


        if self.use_dp:
            self.dp_loss = self.loss_functions.bc_loss_dp(self.xtrain_bc, self.ytrain_bc, self.dp_bc)
        else:
            self.dp_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.neuman_bc:
            self.neuman_bc_loss = self.loss_functions.bc_neuman_loss(self.xtrain_bc_n, self.ytrain_bc_n, self.utrain_bc_n)
        else:
            self.neuman_bc_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.use_data:
            self.data_loss = self.loss_functions.data_loss(self.x_d, self.y_d, self.u_d)
        else:
            self.data_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.bc_exist:
            self.bc_loss = self.bc_loss_function(self.xtrain_bc, self.ytrain_bc, self.utrain_bc)
        else:
            self.bc_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.col_exist and len(self.xtrain_c) > 0:
            self.pde_loss,_,_,uux,diff  = self.pde_loss_function(self.xtrain_c, self.ytrain_c, return_pde_terms = True)
            #print(f'uux {uux.detach().cpu().numpy()} and diffusion {diff.detach().cpu().numpy()}')
        else:
            self.pde_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.data_exist:
                # I am adding p
            #print(f'****************I am in the data exist and len is {self.sol_data.shape}*************')
            self.data_p_loss = self.loss_functions.data_loss(self.xpdata, self.ypdata, self.sol_data)
            #self.data_p_loss = self.loss_functions.bc_loss(self.xtrain_data, self.ytrain_data, self.utrain_data)
        else:
             self.data_p_loss = torch.tensor([0.0], device = self.device, requires_grad=True)


        """

        gradients_nn = torch.autograd.grad(self.pde_loss, self.model.network.parameters(), create_graph=True , retain_graph = True)
        grad_vector_nn = torch.cat([grad.view(-1) for grad in gradients_nn]) #shape ([365])
        #print(f"grad_vector_nn shape {grad_vector_nn.shape}")


        jacobian_nn = torch.autograd.grad(grad_vector_nn, self.model.network.parameters() ,torch.ones_like(grad_vector_nn) ,create_graph=True , retain_graph = True) #[16 , 4] tuple of 5



        #jacobian_vector = jacobian_nn

        jacobian_vector = torch.cat([grad.view(-1) for grad in jacobian_nn]) #[365]


        # Step 6: Compute the Hessian matrix
        hessian_nn = torch.zeros((grad_vector_nn.shape[0], grad_vector_nn.shape[0]))
        for i in range(grad_vector_nn.size(0)):
            tmp = torch.autograd.grad(jacobian_vector[i], self.model.network.parameters() ,create_graph=True , retain_graph = True) 
            hessian_nn[i] = torch.cat([grad.view(-1) for grad in tmp]) 

        print(hessian_nn.shape)
        

        """

        

        self.gradient_loss_total['pde'].append(0.)#(torch.autograd.grad(self.pde_loss, self.model.parameters(), retain_graph = True))
        self.gradient_loss_total['bc'].append(0.)#(torch.autograd.grad(self.alpha * self.bc_loss, self.model.parameters(), retain_graph = True))
        """
        if self.model.encoder is not None:
            self.gradient_loss_network['pde'].append(torch.autograd.grad(self.pde_loss, self.model.network.parameters(), retain_graph = True))
            self.gradient_loss_network['bc'].append(torch.autograd.grad(self.alpha * self.bc_loss, self.model.network.parameters(), retain_graph = True))
            self.gradient_loss_encoder['pde'].append(torch.autograd.grad(self.pde_loss, self.model.encoder.parameters(), retain_graph = True))
            self.gradient_loss_encoder['bc'].append(torch.autograd.grad(self.alpha * self.bc_loss, self.model.encoder.parameters(), retain_graph = True))
            """


        loss = 0.0 * self.bc_loss

        if self.L2:
            # for i in range(len(self.model.encoder.F_active)):
            #     loss += 0.001 * torch.sum(self.model.encoder.F_active[i] ** 2.0).reshape(-1,)[0]
            #     #nn.functional.relu(self.model.encoder.lambda_param[f"lambda{i}"][0]) * torch.sum(self.model.encoder.F_active[i] ** 2.0).reshape(-1,)[0]
            loss += 0.001 * torch.sum(torch.abs(self.model.encoder.F))
        
        if self.dynamic_weights:
                self.alpha, self.beta = self.compute_dynamic_weights()
                loss += self.alpha * self.bc_loss + self.beta * self.data_loss[0] + self.pde_loss + self.data_p_loss[0] + self.dp_loss[0] + self.neuman_bc_loss[0]
        else:
                m = nn.ReLU()
                #loss = self.data_p_loss  + m(self.model.encoder.Ws[1])* self.bc_loss + m(self.model.encoder.Ws[0])  * self.pde_loss + 0.0* self.data_loss + self.dp_loss + self.neuman_bc_loss
                loss = self.data_p_loss  + 1.* self.bc_loss + 1.  * self.pde_loss + 10.0* self.data_loss + self.dp_loss + self.neuman_bc_loss
                #print(self.model.encoder.Ws[1])


        ################################################################
        # abs_res_values = []
        # for param in self.model.encoder.F_active.parameters():
        #     delta = torch.autograd.grad(loss, param, retain_graph=True)
        #     values = [p.reshape(-1,).cpu().tolist() for p in delta if p is not None]
        #     abs_res_values.append(torch.mean(torch.abs(torch.tensor([v for val in values for v in val]))))

        # self.model.encoder.multiplier = torch.tensor(abs_res_values) / torch.sum(torch.tensor(abs_res_values))
        

        ####################### For gate paramter calculation #####################

        # for idx, param in enumerate(self.model.encoder.F_active.parameters()):
        #     delta = torch.autograd.grad(loss, param, retain_graph=True)[0]
        #     mean_gradient = torch.mean(torch.abs(delta)).item()

        #     self.model.encoder.gradient_history[idx].append(mean_gradient)

        #     if len(self.model.encoder.gradient_history[idx]) < self.model.encoder.window_size:
        #         self.model.encoder.moving_average[idx] += mean_gradient / len(self.model.encoder.gradient_history[idx])
        #     else:
        #         self.model.encoder.moving_average[idx] += mean_gradient / self.model.encoder.window_size
        #         # Also account for subtracting the oldest value, if the history exceeds the window size
        #         if len(self.model.encoder.gradient_history[idx]) > self.model.encoder.window_size:
        #             oldest_gradient = self.model.encoder.gradient_history[idx].pop(0)
        #             self.model.encoder.moving_average[idx] -= oldest_gradient / self.model.encoder.window_size

        ##########################################################################################            

        #torch.tensor(abs_res_values)/torch.sqrt(torch.sum(torch.tensor(abs_res_values) ** 2.0)) 
        #nn.functional.softmax(torch.tensor(abs_res_values))

        # if self.L2:
        #     lmbda = 1e-3
        #     l2_reg = torch.tensor(0., requires_grad=True)
        #     for param in self.model.encoder.parameters():
        #         l2_reg = l2_reg + torch.norm(param)
        #         loss = loss + lmbda * l2_reg

        #################################################################

        if self.ewc is not None:
            p = self.ewc.penalty(self.model)
            #print(loss, self.ewc.penalty(self.model))
            loss += 1000 * p

        loss.backward(retain_graph=True)
        ############################ Evaluate and save ############################################
        if self.col_exist:
            self.pde_val_loss = self.pde_loss_function(self.xval_c, self.yval_c)
        else:
            self.pde_val_loss = torch.tensor([0.0], device = self.device, requires_grad=True)
        
        if self.bc_exist:
            #self.bc_val_loss = torch.tensor([0.0], device = self.device, requires_grad=True)
            #self.data_p_loss = self.loss_functions.data_loss(self.xpdata, self.ypdata, self.sol_data)
            self.bc_val_loss = self.bc_loss_function(self.xval_bc, self.yval_bc, self.uval_bc)
        else:
            self.bc_val_loss = torch.tensor([0.0], device = self.device, requires_grad=True)
        
        if self.data_exist:
            self.data_p_loss = torch.tensor([0.0], device = self.device, requires_grad=True)
        else:
             self.data_p_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.neuman_bc:
            self.neuman_bc_val_loss = self.loss_functions.bc_neuman_loss(self.xval_bc_n, self.yval_bc_n, self.uval_bc_n)
        else:
            self.neuman_bc_val_loss = torch.tensor([0.0], device = self.device, requires_grad=True)


        if self.dynamic_weights:
                self.loss_val = self.data_p_loss  + 1.* self.bc_val_loss + 1.  * self.pde_val_loss + self.neuman_bc_val_loss
                #self.alpha * self.bc_val_loss + self.pde_val_loss + self.data_p_loss  + self.neuman_bc_val_loss
        else:
                m = nn.ReLU()
                #loss = self.data_p_loss  + m(self.model.encoder.Ws[1])* self.bc_loss + m(self.model.encoder.Ws[0])  * self.pde_loss + 0.0* self.data_loss + self.dp_loss + self.neuman_bc_loss
                self.loss_val = self.data_p_loss  + 1.* self.bc_val_loss + 1.  * self.pde_val_loss + self.neuman_bc_val_loss
                #print(self.model.encoder.Ws[1])

            
        return loss
    
    def max_eig(self , LOSS , MODEL):
    
            gradients = torch.autograd.grad(LOSS, MODEL.parameters(), create_graph=True , retain_graph= True)
            
            grad_vector = torch.cat([grad.view(-1) for grad in gradients])
            
            jacobian = torch.autograd.grad(grad_vector, MODEL.parameters() ,torch.ones_like(grad_vector) ,create_graph=True , retain_graph = True)
            
            jacobian_vector = torch.cat([grad.view(-1) for grad in jacobian]) #[365]
            
            hessian = torch.zeros((grad_vector.shape[0], grad_vector.shape[0]))
            
            for i in range(grad_vector.size(0)):
                tmp = torch.autograd.grad(jacobian_vector[i], MODEL.parameters() ,create_graph=True , retain_graph = True) 
                hessian[i] = torch.cat([grad.view(-1) for grad in tmp]) 
                
            eig_max = torch.max(torch.real(torch.linalg.eigvals(hessian)))
            
            return eig_max

##############################################################################
############################### FORWARD ######################################
##############################################################################

    #torch.autograd.set_detect_anomaly(True)
    
    def forward(self):

        
        Eigs = []
        Lines = []


        

        
        if self.optim_type.lower() == 'lbfgs':
            self.time = time.time()
            self.model.train()
            # dataset_c = TensorDataset(self.xval_c, self.yval_c)
            # dataset_bc = TensorDataset(self.xval_bc, self.yval_bc, self.uval_bc)

            # dataLoader_c = DataLoader(dataset=dataset_c, batch_size=32, shuffle=True) 
            # dataLoader_bc = DataLoader(dataset=dataset_bc, batch_size=32, shuffle=True) 
            loss_old = torch.tensor([float('inf')]).to(self.device)
            
            for iter in range(self.epochs):
#---------------------------------------------------------------#
                if self.annealing:
                    for param in self.model.parameters():
                        noise = torch.randn_like(param)
                        param.data += noise * self.T
 
                loss = self.optim.step(self.closure)
#---------------------------------------------------------------#
                if self.annealing:
                    delta_cost = loss.data - loss_old
                    if delta_cost < 0 or torch.exp(-1.0 * delta_cost /  self.T) > torch.rand(1).to(self.device):
                        loss_old = loss
                    self.T *= self.cooling_factor   
#---------------------------------------------------------------#
                self.iter = iter

                self.train_losses['pde'].append(self.pde_loss.detach().cpu().numpy().item())
                self.train_losses['bc'].append(self.bc_loss.detach().cpu().numpy().item())
                self.train_losses['total'].append(loss.data.cpu().numpy().item())
                self.val_losses['pde'].append(self.pde_val_loss.detach().cpu().numpy().item())
                self.val_losses['bc'].append(self.bc_val_loss.detach().cpu().numpy().item())
                self.val_losses['total'].append(self.loss_val.detach().cpu().numpy().item())
                #Eigs.append(self.max_eig(self.pde_loss , self.model).item())

                # if iter > 3:
                #     val_pde_ratio = self.val_losses['pde'][-1]/self.val_losses['pde'][-2]
                #     val_bc_ratio = self.val_losses['bc'][-1]/self.val_losses['bc'][-2]
                #     self.coeff_bc *= max(val_bc_ratio/val_pde_ratio, 1.0)**2.0
                #     self.coeff_pde *= max(val_pde_ratio/val_bc_ratio, 1.0)**2.0



                temp = "{:.2e}".format((time.time() - self.time)/60)
                self.time_loss.append(temp)
                #self.update_data()


                if self.iter % 1 == 0:
                    print(f"\n\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter]:.5e} \
                        V Loss: {self.val_losses['total'][self.iter]:.5e} Etime: {temp} min Lambda: {self.model.encoder.moving_average}", end="")
                    
                    #self.Eigs.append(self.max_eig(self.pde_loss , self.model).item())

                if self.iter % 10 == 0 and self.iter > 0:
                    text_lbfgs = f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter]:.5e} \
                        V Loss: {self.val_losses['total'][self.iter]:.5e} coeff_bc: {self.alpha} \
                          coeff_data: {self.beta}  Etime: {temp} min"
                    Lines.append(text_lbfgs)
                    with open(f'./log/history_{self.title}.txt', 'a') as f:
                        f.write('\n'.join(Lines))


                    
                    



                if self.RAR:
                    if iter % 1 == 0 and iter > 1:
                        if self.val_losses['total'][-1] > 1e-6:
                            self.add_col_pde_loss()
                    if iter %10 == 0:
                        self.update_data()
                else:
                    if iter % 10 == 0:
                        self.update_data()


            torch.save(self.model.state_dict(), f'./saved_models/{self.title}/trained_'+self.title+'.pt')


        elif self.optim_type.lower() == 'adam':
            self.time = time.time()
            self.model.train()

            for iter in range(self.epochs):
                self.iter = iter
                loss = self.closure()
                self.adam.step()
                self.scheduler.step()

                self.train_losses['pde'].append(self.pde_loss.detach().cpu().numpy().item())
                self.train_losses['bc'].append(self.bc_loss.detach().cpu().numpy().item())
                self.train_losses['total'].append(loss.data.cpu().numpy().item())
                self.val_losses['pde'].append(self.pde_val_loss.detach().cpu().numpy().item())
                self.val_losses['bc'].append(self.bc_val_loss.detach().cpu().numpy().item())
                self.val_losses['total'].append(self.loss_val.detach().cpu().numpy().item())

                temp = "{:.2e}".format((time.time() - self.time)/60)
                self.time_loss.append(temp)
                # if iter > 3:
                #     val_pde_ratio = self.val_losses['pde'][-1]/self.val_losses['pde'][-2]
                #     val_bc_ratio = self.val_losses['bc'][-1]/self.val_losses['bc'][-2]
                #     self.coeff_bc *= max(val_bc_ratio/val_pde_ratio, 1.0)**2.0
                #     self.coeff_pde *= max(val_pde_ratio/val_bc_ratio, 1.0)**2.0

                #self.update_data()


                if self.iter % 10 == 0:
                    print(f"\n\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter]:.5e} \
                        V Loss: {self.val_losses['total'][self.iter]:.5e} Etime: {temp} min ", end ="")
                        #\ multiplier: {self.model.encoder.moving_average}", end ="")
                             #Lambda: {[self.model.encoder.lambda_param[f'lambda{i}'].detach().cpu() for i in range(len(self.model.encoder.lambda_param))]}", end="")

                
                if self.iter % 10 == 0 and self.iter > 0:
                    text3 = f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter]:.5e} \
                        V Loss: {self.val_losses['total'][self.iter]:.5e} coeff_bc: {self.alpha} \
                          coeff_data: {self.beta}  Etime: {temp} min"
                    Lines.append(text3)
                    with open(f'./log/history_{self.title}.txt', 'a') as f:
                        f.write('\n'.join(Lines))
                        #f.writelines(Texts)

                if self.RAR:
                    if iter % 1 == 0 and iter > 5:
                        if self.pde_loss > 5e-4:
                            self.add_col_pde_loss()
                        elif iter % 20:
                            self.update_data()
                else:
                    if iter % 10 == 0:
                        self.update_data()

            torch.save(self.model.state_dict(), f'./saved_models/{self.title}/trained_'+self.title+'.pt')
        elif self.optim_type.lower() == 'mix':

            self.time = time.time()
            self.model.train()

            for iter in range(self.epochs):
                self.iter = iter
                loss = self.closure()
                self.adam.step()
                self.scheduler.step()

                self.train_losses['pde'].append(self.pde_loss.detach().cpu().numpy().item())
                self.train_losses['bc'].append(self.bc_loss.detach().cpu().numpy().item())
                self.train_losses['total'].append(loss.data.cpu().numpy().item())
                self.val_losses['pde'].append(self.pde_val_loss.detach().cpu().numpy().item())
                self.val_losses['bc'].append(self.bc_val_loss.detach().cpu().numpy().item())
                self.val_losses['total'].append(self.loss_val.detach().cpu().numpy().item())

                if iter > 3:
                    val_pde_ratio = self.val_losses['pde'][-1]/self.val_losses['pde'][-2]
                    val_bc_ratio = self.val_losses['bc'][-1]/self.val_losses['bc'][-2]
                    self.coeff_bc *= max(val_bc_ratio/val_pde_ratio, 1.0)**2.0
                    self.coeff_pde *= max(val_pde_ratio/val_bc_ratio, 1.0)**2.0



                temp = "{:.2e}".format((time.time() - self.time)/60)
                self.time_loss.append(temp)
                #self.update_data()
                if self.iter % 10 == 0:
                    print(f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter]:.5e} \
                        V Loss: {self.val_losses['total'][self.iter]:.5e} Etime: {temp} min", end="")

                if self.iter % 10 == 0 and self.iter > 0:
                    text2 = f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter].item():.5e} \
                        V Loss: {self.val_losses['total'][self.iter].item():.5e} Etime: {temp} min"
                    with open(f'./log/history_{self.title}.txt', 'a') as f:
                        f.write(text2)


                # if iter % 5 == 0 and iter > 0:
                #     self.update_data()

                if self.RAR:
                    if iter % 1 == 0 and iter > 1:
                        if self.val_losses['total'][-1] > 1e-6:
                            self.add_col_pde_loss()
                    if iter %10 == 0:
                        self.update_data()
                else:
                    if iter % 1 == 0:
                        self.update_data()

                # if iter % 500 == 0 and iter >= 1000:
                #     for iter in range(100):
                #         loss = self.optim.step(self.closure())
                #         self.iter = iter

                #         self.train_losses['pde'].append(self.pde_loss.detach().cpu().numpy())
                #         self.train_losses['bc'].append(self.bc_loss.detach().cpu().numpy())
                #         self.train_losses['total'].append(loss.data.cpu().numpy())
                #         self.val_losses['pde'].append(self.pde_val_loss.detach().cpu().numpy())
                #         self.val_losses['bc'].append(self.bc_val_loss.detach().cpu().numpy())
                #         self.val_losses['total'].append(self.loss_val.detach().cpu().numpy())

                #         temp = "{:.2e}".format((time.time() - self.time)/60)
                #         #self.update_data()
                #         if self.iter % 1 == 0:
                #             print(f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter].item():.5e} \
                #                 V Loss: {self.val_losses['total'][self.iter].item():.5e} Etime: {temp} min", end="")

                #         if self.iter % 50 == 0 and self.iter > 0:
                #             print("")

                #     Re = self.loss_functions.original_Re
                #     self.loss_functions.Re =  min(self.loss_functions.Re * 1.5, Re)



            for iter in range(int(self.epochs/10)):
                loss = self.optim.step(self.closure)
                self.iter = iter

                self.train_losses['pde'].append(self.pde_loss.detach().cpu().numpy().item())
                self.train_losses['bc'].append(self.bc_loss.detach().cpu().numpy().item())
                self.train_losses['total'].append(loss.data.cpu().numpy().item())
                self.val_losses['pde'].append(self.pde_val_loss.detach().cpu().numpy().item())
                self.val_losses['bc'].append(self.bc_val_loss.detach().cpu().numpy().item())
                self.val_losses['total'].append(self.loss_val.detach().cpu().numpy().item())

                temp = "{:.2e}".format((time.time() - self.time)/60)

                self.time_loss.append(temp)

                #self.update_data()
                if self.iter % 10 == 0:
                    text1 = f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter]:.5e} \
                        V Loss: {self.val_losses['total'][self.iter]:.5e} Etime: {temp} min"
                    print(text1 , end="")
                    with open(f'./log/history_{self.title}.txt', 'a') as f:
                        f.write(text1)

                if self.iter % 10 == 0:
                    print(f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter]:.5e} \
                        V Loss: {self.val_losses['total'][self.iter]:.5e} Etime: {temp} min", end="")


                if self.RAR:
                    if iter % 1 == 0 and iter > 1:
                        if self.val_losses['total'][-1] > 1e-6:
                            self.add_col_pde_loss()
                    if iter %10 == 0:
                        self.update_data()
                else:
                    if iter % 1 == 0:
                        self.update_data()


            torch.save(self.model.state_dict(), f'./saved_models/{self.title}/trained_'+self.title+'.pt')
        #self.max_eig(self.pde_loss , self.model)#.item()
        else:
            raise ValueError('Wrong Optimizer, choose between lbfgs, adam and mix and the input should be string')
    
