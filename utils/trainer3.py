import torch
import torch.nn as nn
import time
from torch.utils.data import TensorDataset, DataLoader
from utils.Data import get_dp_from_model, get_dp_from_model2
import numpy as np


class Trainer(nn.Module):
    def __init__(
        self, 
        model, 
        pde_loss_function = None,
        bc_loss_function = None, 
        train_size = 0.8,
        epochs = 50, 
        optimizer = 'lbfgs',
        lr = 0.1, ##1.,
        title = 'default',
        data_params = {'N_f': 50000, 'N_bc':50000, 't':1.0, 'nu':0.01, 'domain_size':[0,1,0,1], 'ntest': 10000, 'seed': 1234}, 
        data_func = None, 
        update_data = False,
        use_data = False,
        loss_functions = None,
        dynamic_weights = False, 
        RAR = False,
        device = 'cuda',
        ewc = None,
        use_dp = False,
        evo = False,
        active_res = 3
        
        ) -> None:
        super().__init__()
        self.title = title
        self.optim_type = optimizer
        self.model = model
        self.pde_loss_function = pde_loss_function
        self.bc_loss_function = bc_loss_function
        self.loss_functions = loss_functions
        self.dynamic_weights = dynamic_weights
        self.alpha = 1.0
        self.beta = 1.0
        self.lambdaa = 0.1
        self.RAR = RAR
        self.device = device
        self.ewc = ewc
        self.use_dp = use_dp
        self.coeff_bc = 1.0
        self.coeff_pde = 1.0
        self.EVO = evo
        self.udata = update_data
        self.N_f = data_params['N_f']
        self.rcount = 99
        self.active_res = active_res
        self.pde_eraser = 1.
        self.w =  {"bc": [1.], "pde": [1.], "data": [1.], "total":[1.] , "reconstruction" : [1.] , "ic":[1.]}
        #self.reverse = reverse
        if self.optim_type == 'lbfgs' or self.optim_type == 'mix':
            self.optim = torch.optim.LBFGS(
            model.parameters(), 
            lr=lr, 
            # max_iter=10000,
            # max_eval=10000,
            # history_size=50,
            # tolerance_grad=1e-12,
            # tolerance_change=1.0 * np.finfo(float).eps,
            #line_search_fn="strong_wolfe",
            )
        else:
            self.optim = None

        self.use_data = use_data
        self.train_size = train_size


        #if self.use_data:
        #    self.isol_data = 0.
        #else:
        #    self.isol_data = 1.
        #if use_data:
         #   x_c, y_c, u_c, x_bc, y_bc, u_bc, x_d, y_d, u_d = data_func(data_params, train = True, use_data = True)
          #  self.x_d, self.y_d, self.u_d = x_d, y_d, u_d
        #else:
        if 'convection' in self.title:
                print("hereeeee")
                x_c, y_c,  x_ic, t_ic, u_ic,  x_bc1 , t_bc1, x_bc2  ,  t_bc2 = data_func(data_params, train = True , model = self.model)
                size_bc = int(len(x_bc1) * self.train_size)
                size_ic = int(len(x_ic) * self.train_size)
                u_c = torch.ones_like(x_c)
            
                if use_data:
                    x_d, y_d, u_d = data_func(data_params, train = False)
                    self.x_d, self.y_d, self.u_d = x_d, y_d, u_d

        else:
                
                if use_data:
                    x_c, y_c, u_c, x_bc, y_bc, u_bc, x_d, y_d, u_d = data_func(data_params, train = True, use_data = True)
                    self.x_d, self.y_d, self.u_d = x_d, y_d, u_d
                
                x_c, y_c, u_c, x_bc, y_bc, u_bc = data_func(data_params, train = True)
                size_bc = int(len(x_bc) * self.train_size)


        
        
        size_c = int(len(x_c) * self.train_size)
        
        self.epochs = epochs
        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        if 'convection' not in self.title:
            size_bc = 5000
            self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc]
            self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:]
        else:

            self.xtrain_ic,  self.ttrain_ic,  self.utrain_ic =  x_ic[:size_ic],  t_ic[:size_ic],  u_ic[:size_ic]
            self.xval_ic,  self.tval_ic,  self.uval_ic =  x_ic[size_ic:],  t_ic[size_ic:],  u_ic[size_ic:]

            self.xtrain_bc1,  self.ttrain_bc1 =  x_bc1[:size_bc],  t_bc1[:size_bc]
            self.xtrain_bc2,  self.ttrain_bc2 =  x_bc2[:size_bc],  t_bc2[:size_bc]
            
            self.xval_bc1,  self.tval_bc1 =  x_bc1[size_bc:],  t_bc1[size_bc:]
            self.xval_bc2,  self.tval_bc2 =  x_bc2[size_bc:],  t_bc2[size_bc:]



        self.data_params = data_params
        self.data_func = data_func

        self.train_losses = {"bc": [1.], "pde": [1.], "data": [1.], "total":[1.] , "reconstruction" : [1.] , "ic":[1.]}
        self.val_losses = {"bc": [1.], "pde": [1.], "data": [1.], "total":[1.] , "reconstruction" : [1.] , 'ic':[1.]}

        self.gradient_loss_total = {'pde':[torch.ones([1])], 'bc':[torch.ones([1])]}
        self.gradient_loss_network = {'pde':[], 'bc':[]}
        self.gradient_loss_encoder = {'pde':[], 'bc':[]}
        self.title = title

        self.count = 0
        self.data_exist = False


        if self.use_dp:
            self.dp_bc = get_dp_from_model2(self.model, self.xtrain_bc, self.ytrain_bc, 1.0/self.data_params['nu'])
            #self.utrain_bc = self.dp_bc
        



        if self.pde_loss_function is None:
            self.col_exist = False
        else:
            self.col_exist = True
        
        if self.bc_loss_function is None:
            self.bc_exist = False
            # self.xtrain_c = torch.vstack([self.xtrain_c, self.xtrain_bc])
            # self.ytrain_c = torch.vstack([self.ytrain_c, self.ytrain_bc])
        else:
            self.bc_exist = True


        self.new_dic_col = self.data_params.copy()
        self.adam = torch.optim.Adam(model.parameters(), lr = lr)
        self.scheduler = torch.optim.lr_scheduler.MultiStepLR(self.adam, milestones= np.linspace(0,self.epochs,10).tolist(), gamma=0.75)
        self.iter = 0


    def update_data(self):
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        # if self.use_data:
        #     x_c, y_c, u_c, x_bc, y_bc, u_bc, x_d, y_d, u_d = self.data_func(self.data_params, train = True, use_data = False)
        #     self.x_d, self.y_d, self.u_d = x_d, y_d, u_d
        # else:
        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)
        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:]
    
    def update_data2(self):
        t = 1/30 * self.N_f
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))

        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        x_c_asorted , idx_x_asorted = torch.sort(x_c , dim = 0) #ascending
        y_c_asorted , idx_y_asorted= torch.sort(y_c , dim = 0) #ascending
        x_c_dsorted , idx_x_dsorted = torch.sort(x_c , dim = 0 , descending = True) #descending
        y_c_dsorted , idx_y_dsorted= torch.sort(y_c , dim = 0 , descending = True) #descending

        x_c = torch.cat([x_c_asorted[int(self.count*t):int((self.count + 1)*t),:] , x_c_dsorted[int(self.count*t):int((self.count + 1)*t),:] , x_c[int(self.count*t):int((self.count + 1)*t),:] , x_c[int(self.count*t):int((self.count + 1)*t),:]] , 0)
        y_c = torch.cat([y_c[int(self.count*t):int((self.count + 1)*t),:] , y_c[int(self.count*t):int((self.count + 1)*t),:] ,y_c_asorted[int(self.count*t):int((self.count + 1)*t),:] , y_c_dsorted[int(self.count*t):int((self.count + 1)*t),:]] , 0)


        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)
        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:]


        self.count += 1
        print(self.count)

    def update_data3(self , ret = False , outward = False , single = True):
    
        t = 1/100 * self.N_f
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))

        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        x_c_asorted , idx_x_asorted = torch.sort(x_c , dim = 0) #ascending
        y_c_asorted , idx_y_asorted= torch.sort(y_c , dim = 0) #ascending
        x_c_dsorted , idx_x_dsorted = torch.sort(x_c , dim = 0 , descending = True) #descending
        y_c_dsorted , idx_y_dsorted= torch.sort(y_c , dim = 0 , descending = True) #descending





        if outward: 
            if single: s = int(self.rcount *t)
            else: s = 0
            x_c = torch.cat([x_c_asorted[s:int((self.rcount + 1)*t),:] , x_c_dsorted[s:int((self.rcount + 1)*t),:] , x_c[s:int((self.rcount + 1)*t),:] , x_c[s:int((self.rcount + 1)*t),:]] , 0)
            y_c = torch.cat([y_c[s:int((self.rcount + 1)*t),:] , y_c[s:int((self.rcount + 1)*t),:] ,y_c_asorted[s:int((self.rcount + 1)*t),:] , y_c_dsorted[s:int((self.rcount + 1)*t),:]] , 0)

        else:
            if single: s = int(self.count *t)
            else: s = 0
            x_c = torch.cat([x_c_asorted[s:int((self.count + 1)*t),:] , x_c_dsorted[s:int((self.count + 1)*t),:] , x_c[s:int((self.count + 1)*t),:] , x_c[s:int((self.count + 1)*t),:]] , 0)
            y_c = torch.cat([y_c[s:int((self.count + 1)*t),:] , y_c[s:int((self.count + 1)*t),:] ,y_c_asorted[s:int((self.count + 1)*t),:] , y_c_dsorted[s:int((self.count + 1)*t),:]] , 0)


        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)
        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:]

        if ret: return x_c, y_c
        else:

            self.rcount += -1
            self.count += 1
            print(self.count)

    def update_domain_data(self):
        self.count += 1
        div_num = int(self.epochs/15)+1
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        
        xmin, xmax, ymin, ymax = self.data_params['domain_size']

        xmin_new = round(xmin + (xmax - xmin)/10.0/div_num * self.count,4)
        xmax_new = round(xmax - (xmax - xmin)/10.0/div_num * self.count,4)
        ymin_new = round(ymin + (ymax - ymin)/10.0/div_num * self.count,4)
        ymax_new = round(ymax - (ymax - ymin)/10.0/div_num * self.count,4)

        self.new_dic_col['domain_size'] = [xmin_new, xmax_new, ymin_new, ymax_new]
        self.data_exist = True

        x_c_temp, y_c_temp, u_c_temp, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        
        indices = torch.where((x_c_temp < xmin_new) | (x_c_temp > xmax_new) | (y_c_temp < ymin_new) | (y_c_temp > ymax_new))
        
        print(f'new domain is {[xmin_new, xmax_new, ymin_new, ymax_new]}')
        self.new_dic_col['N_f'] = self.data_params['N_f'] - len(indices[0])
        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_col, train = True)

        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)

        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc,:]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:,:]

        x_data = x_c_temp[indices[0],:].detach()
        y_data = y_c_temp[indices[0], :].detach()

        size_bc = int(len(x_data) * self.train_size)

        x_data.requires_grad = True
        y_data.requires_grad = True

        u_predict = self.evaluate(x_data, y_data, include_p = True).detach()

        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]
        self.utrain_data = u_predict[:size_bc]

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]
        self.uval_data = u_predict[size_bc:]

    def update_domain_data_sequential(self):
        self.count += 1
        div_num = int(self.epochs/100)+1
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        
        xmin, xmax, ymin, ymax = self.data_params['domain_size']

        xmin_new_0 = round(xmin + (xmax - xmin)/30.0/div_num * (self.count-1),4)
        xmax_new_0 = round(xmax - (xmax - xmin)/30.0/div_num * (self.count-1),4)
        ymin_new_0 = round(ymin + (ymax - ymin)/30.0/div_num * (self.count-1),4)
        ymax_new_0 = round(ymax - (ymax - ymin)/30.0/div_num * (self.count-1),4)


        xmin_new = round(xmin + (xmax - xmin)/30.0/div_num * self.count,4)
        xmax_new = round(xmax - (xmax - xmin)/30.0/div_num * self.count,4)
        ymin_new = round(ymin + (ymax - ymin)/30.0/div_num * self.count,4)
        ymax_new = round(ymax - (ymax - ymin)/30.0/div_num * self.count,4)

        self.new_dic_col['domain_size'] = [xmin_new, xmax_new, ymin_new, ymax_new]
        if self.count > 1:
            self.data_exist = True
        
        self.col_exist = True

        x_c_temp, y_c_temp, u_c_temp, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)

        indices_col = torch.where(((x_c_temp < xmin_new) & (x_c_temp > xmin_new_0))
        | ((x_c_temp > xmax_new) & (x_c_temp < xmax_new_0)) 
        | ((y_c_temp < ymin_new) & (y_c_temp > ymin_new_0))
        | ((y_c_temp > ymax_new) & (y_c_temp < ymax_new_0)) )

        indices_data = torch.where((x_c_temp < xmin_new_0)
        | (x_c_temp > xmax_new_0)
        | (y_c_temp < ymin_new_0) 
        | (y_c_temp > ymax_new_0))

        print(f'new domain is {[xmin_new, xmax_new, ymin_new, ymax_new]}')
        
        x_c = x_c_temp[indices_col[0],:].detach()
        y_c = y_c_temp[indices_col[0], :].detach()
        u_c = u_c_temp[indices_col[0], :].detach()
        x_bc = x_bc[indices_col[0], :].detach()
        y_bc = y_bc[indices_col[0], :].detach()
        u_bc = u_bc[indices_col[0], :].detach()

        x_c.requires_grad = True
        y_c.requires_grad = True
        u_c.requires_grad = True
        x_bc.requires_grad = True
        y_bc.requires_grad = True
        u_bc.requires_grad = True


        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)

        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc,:]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:,:]

        x_data = x_c_temp[indices_data[0],:].detach()
        y_data = y_c_temp[indices_data[0], :].detach()

        size_bc = int(len(x_data) * self.train_size)

        x_data.requires_grad = True
        y_data.requires_grad = True

        u_predict = self.evaluate(x_data, y_data, include_p = False).detach()

        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]
        self.utrain_data = u_predict[:size_bc]

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]
        self.uval_data = u_predict[size_bc:]


    def update_domain_data_3x(self):
        self.count += 1
        div_num = int(self.epochs/20)+1
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        
        xmin, xmax, ymin, ymax = self.data_params['domain_size']
        # evey 15 * 3 = 45 updated
        if self.count % 3 == 0:
            xmin_new = xmin + (xmax - xmin)/3.0/div_num * self.count
            xmax_new = xmax - (xmax - xmin)/3.0/div_num * self.count
            ymin_new = ymin + (ymax - ymin)/3.0/div_num * self.count
            ymax_new = ymax - (ymax - ymin)/3.0/div_num * self.count
            self.new_dic_col['domain_size'] = [xmin_new, xmax_new, ymin_new, ymax_new]
            self.data_exist = True
        else:
            xmin_new, xmax_new, ymin_new, ymax_new = self.new_dic_col['domain_size'] 
        # self.new_dic_col['N_f'] = max(self.data_params['N_f'] - int(self.data_params['N_f']/div_num) * self.count, 10000)

        x_c_temp, y_c_temp, u_c_temp, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        
        indices = torch.where((x_c_temp < xmin_new) | (x_c_temp > xmax_new) | (y_c_temp < ymin_new) | (y_c_temp > ymax_new))
        # all_index = torch.arange(len(x_c_temp)).to(indices[0].device)
        # col_indices = all_index[~torch.isin(all_index, indices[0])]
        
        print(f'new domain is {[xmin_new, xmax_new, ymin_new, ymax_new]}')
        self.new_dic_col['N_f'] = self.data_params['N_f'] - len(indices[0])
        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_col, train = True)

        # x_c = x_c_temp[col_indices].detach()
        # y_c = y_c_temp[col_indices].detach()
        # u_c = u_c_temp[col_indices].detach()

        # x_c.requires_grad = True
        # y_c.requires_grad = True
        # u_c.requires_grad = True


        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)

        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc,:]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:,:]


        # xmax_new = xmin_new
        # ymax_new = ymin_new
        # self.new_dic_data['domain_size'] = [xmin, xmax_new, ymin, ymax_new]
        # self.new_dic_data['N_f'] = min(int(self.data_params['N_f']/div_num) * self.count, self.data_params['N_f'])
        # x_data, y_data, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_data, train = True)

        x_data = x_c_temp[indices[0],:].detach()
        y_data = y_c_temp[indices[0], :].detach()

        size_bc = int(len(x_data) * self.train_size)

        x_data.requires_grad = True
        y_data.requires_grad = True

        u_predict = self.evaluate(x_data, y_data, include_p=False).detach()

        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]
        self.utrain_data = u_predict[:size_bc]

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]
        self.uval_data = u_predict[size_bc:]

        # self.xtrain_bc = torch.vstack([self.xtrain_bc, x_data[:size_bc]])
        # self.ytrain_bc = torch.vstack([self.ytrain_bc, y_data[:size_bc]])
        # self.utrain_bc = torch.vstack([self.utrain_bc, u_predict[:size_bc,:2]])

        # self.xval_bc = torch.vstack([self.xval_bc, x_data[size_bc:]])
        # self.yval_bc = torch.vstack([self.yval_bc, y_data[size_bc:]])
        # self.uval_bc = torch.vstack([self.uval_bc, u_predict[size_bc:,:2]])

    def update_domain_data_selective_pde_loss(self):
        self.count += 1
        div_num = int(self.epochs/20)+1
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        
        xmin, xmax, ymin, ymax = self.data_params['domain_size']
        # evey 15 * 3 = 45 updated
        if self.count % 3 == 0:
            xmin_new = xmin + (xmax - xmin)/3.0/div_num * self.count
            xmax_new = xmax - (xmax - xmin)/3.0/div_num * self.count
            ymin_new = ymin + (ymax - ymin)/3.0/div_num * self.count
            ymax_new = ymax - (ymax - ymin)/3.0/div_num * self.count
            self.new_dic_col['domain_size'] = [xmin_new, xmax_new, ymin_new, ymax_new]
            self.data_exist = True
        else:
            xmin_new, xmax_new, ymin_new, ymax_new = self.new_dic_col['domain_size'] 

        x_c_temp, y_c_temp, u_c_temp, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        
        indices = torch.where((x_c_temp < xmin_new) | (x_c_temp > xmax_new) | (y_c_temp < ymin_new) | (y_c_temp > ymax_new))

        
        print(f'new domain is {[xmin_new, xmax_new, ymin_new, ymax_new]}')
        self.new_dic_col['N_f'] = self.data_params['N_f'] - len(indices[0])
        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_col, train = True)



        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)

        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc,:]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:,:]


        if self.data_exist:
            x_data = x_c_temp[indices[0],:].detach()
            y_data = y_c_temp[indices[0], :].detach()

            size_bc = int(len(x_data) * self.train_size)

            x_data.requires_grad = True
            y_data.requires_grad = True

            u_predict = self.evaluate(x_data, y_data, include_p=False).detach()
            _, uterm, vterm = self.pde_loss_function(x_data, y_data, return_pde_terms = True)
            sum_error = torch.abs(uterm) + torch.abs(vterm)
            q20 = torch.quantile(sum_error, q = 0.2)
            idx = torch.where(sum_error <= q20)[0]
            x_data.requires_grad = False
            y_data.requires_grad = False

            x_data = x_data[idx]
            y_data = y_data[idx]
            u_predict = u_predict[idx, :]


            x_data.requires_grad = True
            y_data.requires_grad = True

            self.xtrain_data = x_data[:size_bc]
            self.ytrain_data = y_data[:size_bc]
            self.utrain_data = u_predict[:size_bc]

            self.xval_data = x_data[size_bc:]
            self.yval_data = y_data[size_bc:]
            self.uval_data = u_predict[size_bc:]


    def update_selective_pde_loss(self):
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        self.data_exist = True

        x_c_temp, y_c_temp, _, _, _, _ = self.data_func(self.data_params, train = True)

        if self.data_exist:
            x_data = x_c_temp.detach()
            y_data = y_c_temp.detach()

            size_bc = int(len(x_data) * self.train_size)

            x_data.requires_grad = True
            y_data.requires_grad = True

            u_predict = self.evaluate(x_data, y_data, include_p=True).detach()
            _, uterm, vterm = self.pde_loss_function(x_data, y_data, return_pde_terms = True)
            sum_error = torch.abs(uterm) + torch.abs(vterm)
            q20 = torch.quantile(sum_error, q = 0.01)
            idx = torch.where(sum_error <= q20)[0]
            x_data.requires_grad = False
            y_data.requires_grad = False

            x_data = x_data[idx]
            y_data = y_data[idx]
            u_predict = u_predict[idx, :]


            x_data.requires_grad = True
            y_data.requires_grad = True

            self.xtrain_data = x_data[:size_bc]
            self.ytrain_data = y_data[:size_bc]
            self.utrain_data = u_predict[:size_bc]

            self.xval_data = x_data[size_bc:]
            self.yval_data = y_data[size_bc:]
            self.uval_data = u_predict[size_bc:]


    def get_w(self):
        max_loss = np.max([self.train_losses['pde'][-1] , self.train_losses['bc'][-1] , self.train_losses['ic'][-1] , self.train_losses['reconstruction'][-1]])
        w_pde = 10**np.clip(np.floor(np.log10(max_loss)-np.log10(self.train_losses['pde'][-1])),a_min = -7, a_max = 7)
        if 'convection' in self.title:
            w_ic = 10**np.clip(np.floor(np.log10(max_loss)-np.log10(self.train_losses['ic'][-1])),a_min = -7, a_max = 7)
            self.w['ic'].append(w_ic)
        else:
            w_ic = 1.
            self.w['ic'].append(1.)


        
        w_bc = 10**np.clip(np.floor(np.log10(max_loss)-np.log10(self.train_losses['bc'][-1])),a_min = -7, a_max = 7)
        w_reconst = 10**np.clip(np.floor(np.log10(max_loss)-np.log10(self.train_losses['reconstruction'][-1])),a_min = -7,a_max = 7)

        self.w['pde'].append(w_pde)
        self.w['bc'].append(w_bc)
        
        if self.loss_functions.ae:
            self.w['reconstruction'].append(w_reconst)
        else:
            self.w['reconstruction'].append(1.)

        return w_pde , w_bc , w_reconst , w_ic







    def add_col_pde_loss(self):
        # self.params2 = self.data_params.copy()
        # self.params2['seed'] = int(torch.randint(1000, 1000000, (1,)))
        self.data_exist = False
        # self.params2['N_f'] = 1000
        # self.params2['domain_size'] = [0.0, 1.0, 0.65, 1.0]

        x_c_temp, y_c_temp, u_c_temp, _, _, _ = self.data_func(self.data_params, train = True)

        x_data = x_c_temp.detach()
        y_data = y_c_temp.detach()

        x_data.requires_grad = True
        y_data.requires_grad = True

        total, N1, N2, first, diff = self.pde_loss_function(x_data, y_data, return_pde_terms = True)
        sum_error = torch.abs(N1) + torch.abs(N2)
        q20 = torch.quantile(sum_error, q = 0.9999)
        idx = torch.randperm(len(x_c_temp)) #torch.where(sum_error >= q20)[0]
        x_data.requires_grad = False
        y_data.requires_grad = False

        x_data = x_data[idx]
        y_data = y_data[idx]

        x_data.requires_grad = True
        y_data.requires_grad = True

        size_bc = int(len(x_data) * self.train_size)

        
        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]

        self.xtrain_c = torch.concat([self.xtrain_c, self.xtrain_data], axis = 0)
        self.ytrain_c = torch.concat([self.ytrain_c, self.ytrain_data], axis = 0)

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]

        self.xval_c = torch.concat([self.xval_c, self.xval_data], axis = 0)
        self.yval_c = torch.concat([self.yval_c, self.yval_data], axis = 0)


    def evo_loss(self):
        # self.params2 = self.data_params.copy()
        # self.params2['seed'] = int(torch.randint(1000, 1000000, (1,)))
        #self.data_exist = False
        # self.params2['N_f'] = 1000
        # self.params2['domain_size'] = [0.0, 1.0, 0.65, 1.0]
        if self.udata:
            x_c_temp , y_c_temp = self.update_data3(ret = True , single = True)
        else:
            x_c_temp, y_c_temp, u_c_temp, _, _, _ = self.data_func(self.data_params, train = True)


        x_data = x_c_temp.detach()
        y_data = y_c_temp.detach()

        x_data.requires_grad = True
        y_data.requires_grad = True

        abs_res = self.pde_loss_function(x_data, y_data, return_pde_terms = True)
        threshold = torch.mean(abs_res)

        x_retain = []
        y_retain = []
        for i in range(x_data.shape[0]):
            if abs_res[i] >= threshold:
                x_retain.append(x_data[i,:].reshape(-1,1))
                y_retain.append(y_data[i,:].reshape(-1,1))

        x_retain = torch.cat(x_retain)
        y_retain = torch.cat(y_retain)


        N_retain = x_retain.shape[0]

        

        x_c_rest, y_c_rest, u_c_rest, _, _, _ = self.data_func(self.data_params, train = True)
        N_rest = self.data_params['N_f'] - N_retain

        x_rest = x_c_temp.detach()[:N_rest , 0:1]
        y_rest = y_c_temp.detach()[:N_rest , 0:1]

        #print(N_retain , N_rest)


        x_rest.requires_grad = True
        y_rest.requires_grad = True



        x_data = torch.cat([x_retain , x_rest] , 0)
        y_data = torch.cat([y_retain , y_rest] , 0)

        print(x_data.shape)

        
        #x_data.requires_grad = True
        #y_data.requires_grad = True

        size_bc = int(len(x_data) * self.train_size)

        
        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]

        self.xtrain_c = torch.concat([self.xtrain_c, self.xtrain_data], axis = 0)
        self.ytrain_c = torch.concat([self.ytrain_c, self.ytrain_data], axis = 0)

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]

        self.xval_c = torch.concat([self.xval_c, self.xval_data], axis = 0)
        self.yval_c = torch.concat([self.yval_c, self.yval_data], axis = 0)



    def update_domain_data_shrink_once(self):
        self.count += 1
        div_num = int(self.epochs/20)+1
        self.data_params['seed'] = int(torch.randint(1000, 1000000, (1,)))
        self.new_dic_col = self.data_params.copy()
        
        xmin, xmax, ymin, ymax = self.data_params['domain_size']

        xmin_new = xmin + (xmax - xmin)/100.0
        xmax_new = xmax - (xmax - xmin)/100.0
        ymin_new = ymin + (ymax - ymin)/100.0
        ymax_new = ymax - (ymax - ymin)/100.0
        self.new_dic_col['domain_size'] = [xmin_new, xmax_new, ymin_new, ymax_new]

        # self.new_dic_col['N_f'] = max(self.data_params['N_f'] - int(self.data_params['N_f']/div_num) * self.count, 10000)

        x_c_temp, y_c_temp, u_c_temp, x_bc, y_bc, u_bc = self.data_func(self.data_params, train = True)
        
        indices = torch.where((x_c_temp < xmin_new) | (x_c_temp > xmax_new) | (y_c_temp < ymin_new) | (y_c_temp > ymax_new))
        # all_index = torch.arange(len(x_c_temp)).to(indices[0].device)
        # col_indices = all_index[~torch.isin(all_index, indices[0])]
        
        print(f'new domain is {[xmin_new, xmax_new, ymin_new, ymax_new]}')
        self.new_dic_col['N_f'] = self.data_params['N_f'] - len(indices[0])
        x_c, y_c, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_col, train = True)

        # x_c = x_c_temp[col_indices].detach()
        # y_c = y_c_temp[col_indices].detach()
        # u_c = u_c_temp[col_indices].detach()

        # x_c.requires_grad = True
        # y_c.requires_grad = True
        # u_c.requires_grad = True


        size_c = int(len(x_c) * self.train_size)
        size_bc = int(len(x_bc) * self.train_size)

        self.xtrain_c,  self.ytrain_c,  self.utrain_c =  x_c[:size_c],  y_c[:size_c],  u_c[:size_c]
        self.xtrain_bc,  self.ytrain_bc,  self.utrain_bc =  x_bc[:size_bc],  y_bc[:size_bc],  u_bc[:size_bc,:]
        self.xval_c,  self.yval_c,  self.uval_c =  x_c[size_c:],  y_c[size_c:],  u_c[size_c:]
        self.xval_bc,  self.yval_bc,  self.uval_bc =  x_bc[size_bc:],  y_bc[size_bc:],  u_bc[size_bc:,:]


        # xmax_new = xmin_new
        # ymax_new = ymin_new
        # self.new_dic_data['domain_size'] = [xmin, xmax_new, ymin, ymax_new]
        # self.new_dic_data['N_f'] = min(int(self.data_params['N_f']/div_num) * self.count, self.data_params['N_f'])
        # x_data, y_data, u_c, x_bc, y_bc, u_bc = self.data_func(self.new_dic_data, train = True)

        x_data = x_c_temp[indices[0],:].detach()
        y_data = y_c_temp[indices[0], :].detach()

        size_bc = int(len(x_data) * self.train_size)

        x_data.requires_grad = True
        y_data.requires_grad = True

        u_predict = self.evaluate(x_data, y_data, include_p=False).detach()

        self.xtrain_data = x_data[:size_bc]
        self.ytrain_data = y_data[:size_bc]
        self.utrain_data = u_predict[:size_bc]

        self.xval_data = x_data[size_bc:]
        self.yval_data = y_data[size_bc:]
        self.uval_data = u_predict[size_bc:]

        # self.xtrain_bc = torch.vstack([self.xtrain_bc, x_data[:size_bc]])
        # self.ytrain_bc = torch.vstack([self.ytrain_bc, y_data[:size_bc]])
        # self.utrain_bc = torch.vstack([self.utrain_bc, u_predict[:size_bc,:2]])

        # self.xval_bc = torch.vstack([self.xval_bc, x_data[size_bc:]])
        # self.yval_bc = torch.vstack([self.yval_bc, y_data[size_bc:]])
        # self.uval_bc = torch.vstack([self.uval_bc, u_predict[size_bc:,:2]])
        self.data_exist = True


    def evaluate(self, x, y, include_p = True):
        sol_p = self.model(x, y)
        psi = sol_p[:,0].reshape(-1,1)
        pp = sol_p[:,1].reshape(-1,1)
        up = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
        vp = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), False, False)[0]
        if include_p:
            return torch.hstack([up, vp, pp])
        else:
            return torch.hstack([up, vp])


    def compute_dynamic_weights(self):
        delta_pde_teta = torch.autograd.grad(self.pde_loss, self.model.parameters(),  retain_graph=True)
        values = [p.reshape(-1,).cpu().tolist() for p in delta_pde_teta]
        delta_pde_teta_abs = torch.abs(torch.tensor([v for val in values for v in val]))

        delta_bc_teta = torch.autograd.grad(self.alpha * self.bc_loss, self.model.parameters(),  retain_graph=True)
        values = [p.reshape(-1,).cpu().tolist() for p in delta_bc_teta]
        delta_bc_teta_abs = torch.abs(torch.tensor([v for val in values for v in val]))


        if self.use_data:
            delta_data_teta = torch.autograd.grad(self.beta * self.data_loss, self.model.parameters(),  retain_graph=True)
            values2 = [p.reshape(-1,).cpu().tolist() for p in delta_data_teta]
            delta_data_teta_abs = torch.abs(torch.tensor([v for val in values2 for v in val]))
            temp2 = torch.mean(delta_pde_teta_abs) / torch.mean(delta_data_teta_abs)
        else:
            temp2 = self.beta

        temp = torch.mean(delta_pde_teta_abs) / torch.mean(delta_bc_teta_abs)

        
        return (1.0 - self.lambdaa) * self.alpha + self.lambdaa * temp, (1.0 - self.lambdaa) * self.beta + self.lambdaa * temp2
    
    def compute_dynamic_weights2(self):
        #print(f"LOOK: {self.gradient_loss_total['pde'][-1]}")
        delta_pde_teta = torch.cat([p.reshape(-1).cpu() for p in self.gradient_loss_total['pde'][-1]])
        delta_bc_teta = torch.cat([p.reshape(-1).cpu() for p in self.gradient_loss_total['bc'][-1]])

        # if self.use_data:
        #     delta_data_teta = torch.autograd.grad(self.beta * self.data_loss, self.model.parameters(),  retain_graph=True)
        #     values2 = [p.reshape(-1,).cpu().tolist() for p in delta_data_teta]
        #     delta_data_teta_abs = torch.abs(torch.tensor([v for val in values2 for v in val]))
        #     temp2 = torch.mean(delta_pde_teta_abs) / torch.mean(delta_data_teta_abs)
        # else:
        #     temp2 = self.beta

        temp = torch.max(torch.abs(delta_pde_teta)) / torch.mean(torch.abs(delta_bc_teta))

        print((1.0 - self.lambdaa) * self.alpha + self.lambdaa * temp)

        
        return (1.0 - self.lambdaa) * self.alpha + self.lambdaa * temp, 1.0
    




    def closure(self):
        if self.optim_type != 'adam':
            self.optim.zero_grad()
        self.adam.zero_grad()


        if self.use_dp:
            self.dp_loss = self.loss_functions.bc_loss_dp(self.xtrain_bc, self.ytrain_bc, self.dp_bc)
        else:
            self.dp_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.use_data:
            self.data_loss = self.loss_functions.data_loss(self.x_d, self.y_d, self.u_d)
            self.pde_eraser = 0.
            #print(self.)
            
        else:
            self.data_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.bc_exist:
            if 'convection' in self.title:
                self.ic_loss , self.bc_loss = self.bc_loss_function(self.xtrain_ic, self.ttrain_ic, self.utrain_ic,  self.xtrain_bc1 , self.ttrain_bc1, self.xtrain_bc2  , self.ttrain_bc2)
                #self.bc_loss = self.bc_loss + self.ic_loss
            else:
                self.bc_loss = self.bc_loss_function(self.xtrain_bc, self.ytrain_bc, self.utrain_bc)
                self.ic_loss = torch.tensor([0.0], device = self.device, requires_grad=True)
        else:
            self.bc_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.col_exist:
            self.pde_loss = self.pde_loss_function(self.xtrain_c, self.ytrain_c)
        else:
            self.pde_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.data_exist:
                # I am adding p
            self.data_p_loss = self.loss_functions.bc_loss(self.xtrain_data, self.ytrain_data, self.utrain_data)
        else:
             self.data_p_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

        if self.loss_functions.ae:
            self.reconst_loss = self.loss_functions.reconst_loss(self.xtrain_c, self.ytrain_c)
        else:
            self.reconst_loss = torch.tensor([0.0], device = self.device, requires_grad=True)


        if self.dynamic_weights and self.iter >1 and self.iter%100 == 0:# and self.loss_functions.ae: and self.optim_type == 'lbfgs'
                #self.alpha, self.beta = self.compute_dynamic_weights2()
                

                w_pde , w_bc , w_reconst , w_ic = self.get_w()
                print('\n')
                print(w_pde , w_bc , w_reconst , w_ic)
                if self.loss_functions.ae:
                    loss = 1.*self.data_p_loss  + self.w['bc'][-1] * self.bc_loss +self.w['pde'][-1]  * self.pde_loss + self.data_loss + 1.0*self.dp_loss +self.w['reconstruction'][-1]* self.reconst_loss +self.w['ic'][-1]*self.ic_loss
                else:
                    loss = 1.*self.data_p_loss  + self.w['bc'][-1] * self.bc_loss + self.pde_eraser* self.w['pde'][-1]  * self.pde_loss + self.data_loss + 1.0*self.dp_loss +self.w['ic'][-1]*self.ic_loss

                #loss = self.alpha * self.bc_loss + self.beta * self.data_loss + self.pde_loss + self.data_p_loss + self.dp_loss
        else:
                if self.loss_functions.ae:
                    loss = 1.*self.data_p_loss  + self.w['bc'][-1] * self.bc_loss + self.w['pde'][-1]  * self.pde_loss + self.data_loss + 1.0*self.dp_loss +self.w['reconstruction'][-1]* self.reconst_loss + self.w['ic'][-1]*self.ic_loss
                else:
                    #print(type(self.w['bc'][-1]) , type(self.w['pde'][-1]))
                    loss = 1.*self.data_p_loss  + self.w['bc'][-1] * self.bc_loss + self.pde_eraser*self.w['pde'][-1] * self.pde_loss + self.w['bc'][-1]*self.data_loss + 1.0*self.dp_loss + self.w['ic'][-1]*self.ic_loss

                self.w['bc'].append(self.w['bc'][-1])
                self.w['pde'].append(self.w['pde'][-1])
                self.w['reconstruction'].append(self.w['reconstruction'][-1])
                self.w['ic'].append(self.w['ic'][-1])
                #print("here?")
                #loss = 1.0*self.data_p_loss  +  1. * self.bc_loss + 1.0  * self.pde_loss + 0.0*self.data_loss + 1.0*self.dp_loss +10000.* self.reconst_loss


        #self.gradient_loss_total['pde'].append(torch.autograd.grad(self.pde_loss, self.model.parameters(), retain_graph = True))
        #self.gradient_loss_total['bc'].append(torch.autograd.grad(self.alpha * self.bc_loss, self.model.parameters(), retain_graph = True))

        """

        if self.model.encoder1 is not None:
            self.gradient_loss_network['pde'].append(torch.autograd.grad(self.pde_loss, self.model.network.parameters(), retain_graph = True))
            self.gradient_loss_network['bc'].append(torch.autograd.grad(self.alpha * self.bc_loss, self.model.network.parameters(), retain_graph = True))
            self.gradient_loss_encoder['pde'].append(torch.autograd.grad(self.pde_loss, self.model.encoder.parameters(), retain_graph = True))
            self.gradient_loss_encoder['bc'].append(torch.autograd.grad(self.alpha * self.bc_loss, self.model.encoder.parameters(), retain_graph = True))
        """

        if self.ewc is not None:
            p = self.ewc.penalty(self.model)
            #print(loss, self.ewc.penalty(self.model))
            loss += 1000 * p



        loss.backward(retain_graph=True)

        ############################ Evaluate and save ############################################
        if self.col_exist:
            self.pde_val_loss = self.pde_loss_function(self.xval_c, self.yval_c)
            self.reconst_val_loss = torch.tensor([0.0], device = self.device, requires_grad=True)#self.loss_functions.reconst_loss(self.xval_c, self.yval_c)
        else:
            self.pde_val_loss = torch.tensor([0.0], device = self.device, requires_grad=True)
            self.reconst_val_loss  = torch.tensor([0.0], device = self.device, requires_grad=True)
        
        if self.bc_exist:
            if 'convection' in self.title:
                #print()
                self.ic_val_loss , self.bc_val_loss  = self.bc_loss_function(self.xval_ic, self.tval_ic, self.uval_ic,  self.xval_bc1 , self.tval_bc1, self.xval_bc2  , self.tval_bc2)
            else:
                self.bc_val_loss = self.bc_loss_function(self.xval_bc, self.yval_bc, self.uval_bc)
                self.ic_val_loss = torch.tensor([0.0], device = self.device, requires_grad=True)
        else:
            self.bc_val_loss = torch.tensor([0.0], device = self.device, requires_grad=True)
        
        if self.data_exist:
            self.data_p_loss = self.loss_functions.bc_loss(self.xval_data, self.yval_data, self.uval_data)
        else:
             self.data_p_loss = torch.tensor([0.0], device = self.device, requires_grad=True)

 
        self.loss_val  =     1 * self.bc_val_loss + self.pde_val_loss + self.data_p_loss 

            
        return loss
    
    def max_eig(self , LOSS , MODEL):

        gradients = torch.autograd.grad(LOSS, MODEL.parameters(), create_graph=True , retain_graph= True)
        
        grad_vector = torch.cat([grad.view(-1) for grad in gradients])
        
        jacobian = torch.autograd.grad(grad_vector, MODEL.parameters() ,torch.ones_like(grad_vector) ,create_graph=True , retain_graph = True)
        
        jacobian_vector = torch.cat([grad.view(-1) for grad in jacobian]) #[365]
        
        hessian = torch.zeros((grad_vector.shape[0], grad_vector.shape[0]))
        
        for i in range(grad_vector.size(0)):
            tmp = torch.autograd.grad(jacobian_vector[i], MODEL.parameters() ,create_graph=True , retain_graph = True) 
            hessian[i] = torch.cat([grad.view(-1) for grad in tmp]) 
            
        eig_max = torch.max(torch.real(torch.linalg.eigvals(hessian)))
        
        return eig_max

##############################################################################
############################### FORWARD ######################################
##############################################################################

    def forward(self):

        Eigs = []
        Lines = []
        if self.optim_type == 'lbfgs':
            self.time = time.time()
            self.model.train()
            # dataset_c = TensorDataset(self.xval_c, self.yval_c)
            # dataset_bc = TensorDataset(self.xval_bc, self.yval_bc, self.uval_bc)

            # dataLoader_c = DataLoader(dataset=dataset_c, batch_size=32, shuffle=True) 
            # dataLoader_bc = DataLoader(dataset=dataset_bc, batch_size=32, shuffle=True) 

            for iter in range(self.epochs):
                loss = self.optim.step(self.closure)
                self.iter = iter

                self.train_losses['pde'].append(self.pde_loss.detach().cpu().numpy())
                self.train_losses['data'].append(self.data_loss.detach().cpu().numpy())
                print(self.data_loss.detach().cpu().numpy())
                self.train_losses['bc'].append(self.bc_loss.detach().cpu().numpy())
                self.train_losses['ic'].append(self.ic_loss.detach().cpu().numpy())
                self.train_losses['total'].append(loss.data.cpu().numpy())
                self.train_losses['reconstruction'].append(self.reconst_loss.detach().cpu().numpy())
                self.val_losses['pde'].append(self.pde_val_loss.detach().cpu().numpy())
                #self.val_losses['data'].append(self.data_val_loss.detach().cpu().numpy())
                self.val_losses['bc'].append(self.bc_val_loss.detach().cpu().numpy())
                self.val_losses['ic'].append(self.ic_val_loss.detach().cpu().numpy())
                self.val_losses['total'].append(self.loss_val.detach().cpu().numpy())
                self.val_losses['reconstruction'].append(self.reconst_val_loss.detach().cpu().numpy())

                if iter > 3:
                    val_pde_ratio = self.val_losses['pde'][-1]/self.val_losses['pde'][-2]
                    val_bc_ratio = self.val_losses['bc'][-1]/self.val_losses['bc'][-2]
                    self.coeff_bc *= max(val_bc_ratio/val_pde_ratio, 1.0)**2.0
                    self.coeff_pde *= max(val_pde_ratio/val_bc_ratio, 1.0)**2.0



                temp = "{:.2e}".format((time.time() - self.time)/60)
                #self.update_data()
                if self.iter % 1== 0 and self.iter > 0:
                    print(f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter].item():.5e} \
                        V Loss: {self.val_losses['total'][self.iter].item():.5e} coeff_bc: {self.coeff_bc} \
                          coeff_pde: {self.coeff_pde}  Etime: {temp} min", end="")
                    #print(f"\n alpha{self.model.encoder.a[0]}")
                    
                if self.udata and self.iter % 10 == 0:
                    print("here?")
                    self.update_data2()
                    

                if self.iter % 10 == 0 and self.iter > 0:
                    print(f"  len = {len(self.xtrain_c)} \n")
                    

                if self.RAR:
                    if iter % 1 == 0 and iter > 5:
                        if self.val_losses['total'][-1] > 1e-4:
                            self.update_data()
                            self.add_col_pde_loss()
                        # elif iter %1 == 0:
                        #     self.update_data()
                
                else:
                    if iter % 5 == 0:
                        #self.update_data()

                        if self.EVO:
                            print('yes evo')
                            self.evo_loss()
                            
                                # elif iter %1 == 0:
                                #     self.update_data()
                                
                

            #torch.save(self.model.state_dict(), './Saved_Models/trained_'+self.title+'.pt')

        elif self.optim_type == 'adam':
            self.time = time.time()
            self.model.train()

            for iter in range(self.epochs):
                self.iter = iter
                loss = self.closure()
                self.adam.step()
                self.scheduler.step()

                self.train_losses['pde'].append(self.pde_loss.detach().cpu().numpy())
                self.train_losses['bc'].append(self.bc_loss.detach().cpu().numpy())
                self.train_losses['total'].append(loss.data.cpu().numpy())
                self.train_losses['reconstruction'].append(self.reconst_loss.detach().cpu().numpy())
                self.val_losses['pde'].append(self.pde_val_loss.detach().cpu().numpy())
                self.val_losses['bc'].append(self.bc_val_loss.detach().cpu().numpy())
                self.val_losses['total'].append(self.loss_val.detach().cpu().numpy())
                self.val_losses['reconstruction'].append(self.reconst_val_loss.detach().cpu().numpy())

                temp = "{:.2e}".format((time.time() - self.time)/60)

                if iter > 3:
                    val_pde_ratio = self.val_losses['pde'][-1]/self.val_losses['pde'][-2]
                    val_bc_ratio = self.val_losses['bc'][-1]/self.val_losses['bc'][-2]
                    self.coeff_bc *= max(val_bc_ratio/val_pde_ratio, 1.0)**2.0
                    self.coeff_pde *= max(val_pde_ratio/val_bc_ratio, 1.0)**2.0



                temp = "{:.2e}".format((time.time() - self.time)/60)
                #self.update_data()
                if self.iter % 1 == 0 and self.iter >0:
                    print(f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter].item():.5e} \
                        V Loss: {self.val_losses['total'][self.iter].item():.5e} coeff_bc: {self.coeff_bc} \
                          coeff_pde: {self.coeff_pde}  Etime: {temp} min", end="")
                    

                    Lines.append(str(self.val_losses['total'][self.iter].item()))
                    #Eigs.append(self.max_eig(self.pde_loss , self.model).item())
                    with open(f'./Text/val_loss_{self.title}_active_res{self.active_res}.txt', 'w') as f:
                        f.write('\n'.join(Lines))

                if self.iter % 50 == 0 and self.iter > 0:
                    print(f"  len = {len(self.xtrain_c)} \n")

                    text3 = f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter].item():.5e} \
                        V Loss: {self.val_losses['total'][self.iter].item():.5e} coeff_bc: {self.coeff_bc} \
                          coeff_pde: {self.coeff_pde}  Etime: {temp} min"
                    #Lines.append(text3)
                    #Eigs.append(self.max_eig(self.pde_loss , self.model).item())
                    #with open(f'./Text/history_{self.title}.txt', 'w') as f:
                        #f.write('\n'.join(Lines))
                        #f.writelines(Texts)
                if self.EVO and iter%25 == 0:
                    print("evo sampling is active.")

                    self.evo_loss()

                if self.RAR:
                    if iter % 1 == 0 and iter > 5:
                        if self.pde_loss > 5e-4:
                            self.add_col_pde_loss()
                        # elif iter %1 == 0:
                        #     self.update_data()
                elif self.udata:
                    if iter % 50 == 0:
                        print("update data is active.")
                        self.update_data3(outward = True , single = True)

            
            torch.save(self.model.state_dict(), './Saved_Models/trained_'+self.title+'.pt')
            eigs_np = np.array(Eigs)
            np.savetxt(f"./Text/eigs_{self.title}.txt", eigs_np)
        else:

            self.time = time.time()
            self.model.train()

            for iter in range(self.epochs):
                self.iter = iter
                loss = self.closure()
                self.adam.step()
                self.scheduler.step()

                self.train_losses['pde'].append(self.pde_loss.detach().cpu().numpy())
                self.train_losses['bc'].append(self.bc_loss.detach().cpu().numpy())
                self.train_losses['total'].append(loss.data.cpu().numpy())
                self.val_losses['pde'].append(self.pde_val_loss.detach().cpu().numpy())
                self.val_losses['bc'].append(self.bc_val_loss.detach().cpu().numpy())
                self.val_losses['total'].append(self.loss_val.detach().cpu().numpy())

                if iter > 3:
                    val_pde_ratio = self.val_losses['pde'][-1]/self.val_losses['pde'][-2]
                    val_bc_ratio = self.val_losses['bc'][-1]/self.val_losses['bc'][-2]
                    self.coeff_bc *= max(val_bc_ratio/val_pde_ratio, 1.0)**2.0
                    self.coeff_pde *= max(val_pde_ratio/val_bc_ratio, 1.0)**2.0



                temp = "{:.2e}".format((time.time() - self.time)/60)
                #self.update_data()
                if self.iter % 1 == 0:
                    print(f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter].item():.5e} \
                        V Loss: {self.val_losses['total'][self.iter].item():.5e} coeff_bc: {self.coeff_bc} \
                          coeff_pde: {self.coeff_pde}  Etime: {temp} min", end="")

                if self.iter % 50 == 0 and self.iter > 0:
                    print(f"  len = {len(self.xtrain_c)}")

                # if iter % 5 == 0 and iter > 0:
                #     self.update_data()

                if self.RAR:
                    if iter % 1 == 0 and iter > 5:
                        if self.pde_loss > 1e-4:
                            self.add_col_pde_loss()
                        # elif iter %1 == 0:
                        #     self.update_data()
                elif self.udata:
                    if iter % 10 == 0:
                        self.update_data()

                # if iter % 500 == 0 and iter >= 1000:
                #     for iter in range(100):
                #         loss = self.optim.step(self.closure())
                #         self.iter = iter

                #         self.train_losses['pde'].append(self.pde_loss.detach().cpu().numpy())
                #         self.train_losses['bc'].append(self.bc_loss.detach().cpu().numpy())
                #         self.train_losses['total'].append(loss.data.cpu().numpy())
                #         self.val_losses['pde'].append(self.pde_val_loss.detach().cpu().numpy())
                #         self.val_losses['bc'].append(self.bc_val_loss.detach().cpu().numpy())
                #         self.val_losses['total'].append(self.loss_val.detach().cpu().numpy())

                #         temp = "{:.2e}".format((time.time() - self.time)/60)
                #         #self.update_data()
                #         if self.iter % 1 == 0:
                #             print(f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter].item():.5e} \
                #                 V Loss: {self.val_losses['total'][self.iter].item():.5e} Etime: {temp} min", end="")

                #         if self.iter % 50 == 0 and self.iter > 0:
                #             print("")

                #     Re = self.loss_functions.original_Re
                #     self.loss_functions.Re =  min(self.loss_functions.Re * 1.5, Re)



            for iter in range(int(self.epochs/10)):
                loss = self.optim.step(self.closure)
                self.iter = iter

                self.train_losses['pde'].append(self.pde_loss.detach().cpu().numpy())
                self.train_losses['bc'].append(self.bc_loss.detach().cpu().numpy())
                self.train_losses['total'].append(loss.data.cpu().numpy())
                self.val_losses['pde'].append(self.pde_val_loss.detach().cpu().numpy())
                self.val_losses['bc'].append(self.bc_val_loss.detach().cpu().numpy())
                self.val_losses['total'].append(self.loss_val.detach().cpu().numpy())

                temp = "{:.2e}".format((time.time() - self.time)/60)
                #self.update_data()
                if self.iter % 1 == 0:
                    print(f"\rEp: {self.iter} T Loss: {self.train_losses['total'][self.iter].item():.5e} \
                        V Loss: {self.val_losses['total'][self.iter].item():.5e} Etime: {temp} min", end="")

                if self.iter % 50 == 0 and self.iter > 0:
                    print("")

                if self.RAR:
                    if iter % 1 == 0 and iter > 50:
                        if self.pde_loss > 5e-4:
                            self.add_col_pde_loss()
                        # elif iter %1 == 0:
                        #     self.update_data()
                else:
                    if iter % 1 == 0:
                        self.update_data()


        #torch.save(self.model.network.state_dict(), './Saved_Models/ZZweights_'+self.title+'.pt')
        #torch.save(self.model, './Saved_Models/ZZtrained_'+self.title+'.pt')
