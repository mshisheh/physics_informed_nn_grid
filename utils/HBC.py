import torch

def customized_2d_exact_u(x,y , data_params):

    f = torch.exp(torch.Tensor([-2*data_params['nu']*data_params['t']])).to("cuda:0")
    psi = (-torch.cos(x)*torch.cos(y)*f).to("cuda:0")
    p = (-1/4*(torch.cos(2*x) + torch.cos(2*y))*f**2.0).to("cuda:0")
    u = torch.cat([psi , p] , -1).to("cuda:0")

    return u

def apply_hbc(x,y,u , data_params):

    x_min , x_max , y_min , y_max = data_params['domain_size']
    x_min = torch.ones_like(x , device="cuda:0")*x_min
    x_max = torch.ones_like(x , device="cuda:0")*x_max
    y_min = torch.ones_like(y , device="cuda:0")*y_min
    y_max = torch.ones_like(y , device="cuda:0")*y_max


    #This would work for this particular example (Taylor-Green vortex), normally 4 different boundary functions should be defined: uL , uB , uT , uR
    uL = lambda x , y: customized_2d_exact_u(x_min , y , data_params)
    uB =  lambda x , y: customized_2d_exact_u(x , y_min , data_params)
    uT = lambda x , y: customized_2d_exact_u(x , y_max , data_params)
    uR = lambda x , y: customized_2d_exact_u(x_max , y , data_params)


    #threshold for identifying the boundaries
    eps = 1e-11

    ONE = lambda k: torch.where(torch.abs(1-k)<eps , 1. , k)
    ZERO = lambda k: torch.where(torch.abs(k)<eps , 0. , k)

    
    #rescaling the domain to [0,1]x[0,1] for easier formulation:
    x_n = (x-x_min)/(x_max-x_min)
    y_n = (y-y_min)/(y_max-y_min)

    #"VERY" close to the boundaries (i.e., closer than the threshold) = on the boundaries
    x_n , y_n = ONE(x_n) , ONE(y_n)
    x_n , y_n = ZERO(x_n) , ZERO(y_n)

    X = torch.cat([x_n , y_n] , -1) #(N,2)
    
    phi_= torch.min(torch.sign(X*(1-X)) , dim = 1)[:][0] #returns 0 on boundaries, 1 inside the domain #(N,)
    phi = torch.unsqueeze(phi_, dim = 1) #(N,1)

  
    g = (1-torch.sign(x_n))*uL(x,y)+\
        (1-torch.sign(y_n))*uB(x,y)+\
            (1-torch.sign(1-y_n))*uT(x,y)+\
              (1-torch.sign(1-x_n))*uR(x,y)-\
                (1-torch.sign(1-x_n*y_n))*uR(x,y) #to fix the value at (1,1) because it is considered twice (rescaled coordinates)
  

    u_ = u*phi + (1-phi)*g

    return u_



def apply_neural_bc(x,y, model_bc, model_g, u, data_params):

    x_min , x_max , y_min , y_max = data_params['domain_size']
    x_min = torch.ones_like(x , device="cuda:0")*x_min
    x_max = torch.ones_like(x , device="cuda:0")*x_max
    y_min = torch.ones_like(y , device="cuda:0")*y_min
    y_max = torch.ones_like(y , device="cuda:0")*y_max

    # x_n = (x-x_min)/(x_max-x_min)
    # y_n = (y-y_min)/(y_max-y_min)

    # X = torch.cat([x_n , y_n] , -1) #(N,2)
    
    # phi_= torch.min(torch.sign(X*(1-X)) , dim = 1)[:][0] #returns 0 on boundaries, 1 inside the domain #(N,)
    # phi = torch.unsqueeze(phi_, dim = 1) #(N,1)

    # D = (x - x_min) * (y - y_min) * (x - x_max) * (y - y_max) #/ ((x - x_min) * (y - y_min) * (x - x_max) * (y - y_max) + 1e-11)

    if model_g is None:
        D = (x - x_min) * (y - y_min) * (x - x_max) * (y - y_max)
        D = torch.sin(torch.pi * (x + 1) / 2) * torch.sin(torch.pi * (y) / 2)
        #D = (x - x_min) * (y - y_min) * (x - x_max) * (y - y_max) / ((x - x_min) * (y - y_min) * (x - x_max) * (y - y_max) + 1e-3)
        u_ = model_bc(x,y) + u * D
    else:
        u_ = u * model_g(x,y)  +  (1 - model_g(x,y)) * model_bc(x, y)

    return u_