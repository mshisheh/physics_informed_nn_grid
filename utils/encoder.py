import torch
import torch.nn as nn
#from cosine_sampler_2d import CosineSampler2d
from utils.grid_sample_bc import grid_sample_2d


dtype = torch.float32
device = torch.device("cuda")

class EncoderShirin (nn.Module):

    def __init__(self, n_features = 4, res = [8, 12, 20, 18, 32], n_cells = 2, domain_size = [0, 1, 0, 1] , mode = 'bilinear') -> None:
        super().__init__()
        self.mode = mode
        self.n_features = n_features 
        self.res = res
        self.n_cells = n_cells



        a = [torch.rand(size=(self.n_cells, self.n_features, self.res[i], self.res[i]),
             dtype=dtype).to(device).data.uniform_(-1e-5,1e-5) for i in range(len(self.res))]
        

        #b = [torch.empty(size = (self.n_cells[0], self.n_features, self.res[0], self.res[0]), dtype=dtype, device=device) ]
             
        self.F_active = nn.ParameterList(a)
        self.Ws = nn.ParameterList([torch.tensor([10.]).to(device) , torch.tensor([10.]).to(device)])
        self.domain_size = domain_size

    def forward(self, x, y):
        features  = []
        x_min, x_max, y_min, y_max = self.domain_size
        x = (x - x_min)/(x_max - x_min)
        y = (y - y_min)/(y_max - y_min)
        x = x*2-1
        y = y*2-1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)
        x = x.repeat([self.n_cells,1,1,1])

        for idx , alpha in enumerate(self.F_active):
            #print(alpha.shape)
            #alpha: [3 , 4, 3 , 3]
            """
            beta = alpha.clone()
            beta[:,0,0 , :] = 0
            beta[:,0,:,0] = 0
            beta[:,0,-1 , :] = 1.
            beta[:,0 , : , -1] = 0.

            beta[:,0,0 , :].detach()
            beta[:,0,:,0].detach()
            beta[:,0,-1 , :].detach()
            beta[:,0 , : , -1].detach()



            beta[:,1,0 , :] = 0
            beta[:,1,:,0] = 0
            beta[:,1,-1 , :] = 0
            beta[:,1 , : , -1] = 0

            beta[:,1,0 , :].detach()
            beta[:,1,:,0].detach()
            beta[:,1,-1 , :].detach()
            beta[:,1 , : , -1].detach()
            """

            
            #print(beta)

            beta = alpha


            #print(beta)

            F = grid_sample_2d(beta, x, step=self.mode, offset=True)
            #F =  grid_sample_2d(beta, x, step=self.mode, offset=True , shift_mode='/' , shift= 1/3)
            #F = grid_sample_2d(alpha, x, step=self.mode, offset=True , shift_mode='+' , shift= 1/3)
            #F = CosineSampler2d.apply(alpha, x, 'zeros', True, 'cosine', True)

            features.append(F.sum(0).view(self.n_features,-1).t())

            #when shifting turned off:
            #features.append(F[0,...].view(self.n_features,-1).t())
        
        F = torch.cat(tuple(features) , 1)
        return F

import torch
import torch.nn as nn
# from cosine_sampler_2d import CosineSampler2d
from utils.grid_sample import grid_sample_2d

dtype = torch.float32
device = torch.device("cuda:0")


def Gaussian_average0(alpha):
    seg = alpha[0,0,0,:].shape[-1]
    meshes = torch.meshgrid(torch.linspace(0,1, seg), torch.linspace(0,1, seg))
    weight = torch.zeros((seg,seg)).to(device)
    alpha_new = torch.zeros_like(alpha)
    sigma = 1.0/(2*seg)
    for k in range(seg):
        for l in range(seg):
            for i in range(seg):
                for j in range(seg):
                    weight[i,j] = torch.exp(-0.5 * (torch.sqrt((meshes[0][i,j] - meshes[0][k,l])**2.0 + 
                                                                (meshes[1][i,j] - meshes[1][k,l])**2.0)/sigma)**2.0)/(sigma * (2 * torch.pi)**0.5)
            weight/=torch.sum(weight)
            weight_expand = weight.repeat(2,4,1,1)
            alpha_new[...,k,l] = torch.sum(weight_expand * alpha, dim = (2,3))

    return alpha_new


def Gaussian_average(alpha):
    seg = alpha.shape[-1]
    meshes = torch.meshgrid(torch.linspace(0,1, seg), torch.linspace(0,1, seg))
    weight = torch.zeros((seg,seg)).to(device)
    alpha_new = torch.zeros_like(alpha).to(device)
    sigma = 0.1

    for k in range(seg):
        for l in range(seg):
            dist = torch.sqrt((meshes[0] - meshes[0][k,l])**2.0 + (meshes[1] - meshes[1][k,l])**2.0)
            weight = torch.exp(-0.5 * (dist / sigma)**2.0) / (sigma * (2 * torch.pi)**0.5)
            weight /= torch.sum(weight)
            weight_expand = weight.repeat(alpha.shape[0], alpha.shape[1], 1, 1).to(device)
            alpha_new[...,k,l] = torch.sum(weight_expand * alpha, dim=(2,3))

    return alpha_new





class Encoder (nn.Module):

    def __init__(self, n_features = 4, res = [8, 12, 20, 18, 32], n_cells = 2, domain_size = [0, 1, 0, 1],  mode = 'bilinear') -> None:
        super().__init__()
        self.n_features = n_features 
        self.mode = mode
        self.res = res
        self.n_cells = n_cells

        
        self.F_active = nn.ParameterList([torch.rand(size=(self.n_cells[i], self.n_features, self.res[i], self.res[i]),
             dtype=dtype , device = device).data.uniform_(-1e-5,1e-5) for i in range(len(self.res))])

        # self.F_active = nn.ParameterList([torch.rand(size=(self.n_cells, self.n_features, self.res[i], self.res[i]),
        #      dtype=dtype , device = device) for i in range(len(self.res))])

        # for f in self.F_active:
        #     nn.init.xavier_normal_(f)

        self.domain_size = domain_size
        self.active = nn.SiLU()


    def forward(self, x, y):
        features  = []
        x_min, x_max, y_min, y_max = self.domain_size
        x = (x - x_min)/(x_max - x_min)
        y = (y - y_min)/(y_max - y_min)
        x = x*2-1
        y = y*2-1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)


        for idx , alpha in enumerate(self.F_active):
            x3 = x.repeat([self.n_cells[idx],1,1,1])
            #alpha = Gaussian_average(alpha)
            F = grid_sample_2d(alpha, x3, step= self.mode, offset=True)
            #F = CosineSampler2d.apply(alpha, x, 'zeros', True, 'cosine', True)
            features.append(F.sum(0).view(self.n_features,-1).t())
        
        # sum_level = sum(self.res[i]**2 for i in range(len(self.res)))
        # F = sum(self.res[len(self.res)-1-i]**2.0/sum_level * features[i] for i in range(len(self.res))) #*  sum(tuple(features))
        F = torch.cat(tuple(features) , 1)
        return F


class Encoder_attn(nn.Module):
    def __init__(self, n_features=4, res=[8, 12, 20, 18, 32], n_cells=2, domain_size=[0, 1, 0, 1],  mode='bilinear'):
        super().__init__()
        self.n_features = n_features 
        self.mode = mode
        self.res = res
        self.n_cells = n_cells
        
        self.F_active = nn.ParameterList([torch.rand(size=(self.n_cells[i], self.n_features, self.res[i], self.res[i]),
             dtype=dtype, device=device).data.uniform_(-1e-4,1e-4) for i in range(len(self.res))])

        self.domain_size = domain_size
        self.active = nn.SiLU()
        self.attn = nn.Linear(self.n_features * len(self.res), self.n_features * len(self.res), device=device)  # Attention layer
        
        #nn.Linear(self.n_features * self.n_cells[0] * self.res[0] * self.res[0], self.n_features * self.n_cells[0] * self.res[0] * self.res[0], device=device)
        #nn.Linear(self.n_features * len(self.res), self.n_features * len(self.res), device=device)  # Attention layer

        self.grid = GridSelfAttention(device= device)

    def forward(self, x, y):
        features = []
        x_min, x_max, y_min, y_max = self.domain_size
        x = (x - x_min) / (x_max - x_min)
        y = (y - y_min) / (y_max - y_min)
        x = x*2-1
        y = y*2-1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)

        for idx, alpha in enumerate(self.F_active):
            # alpha_temp = nn.functional.softmax(self.attn(alpha.reshape(1,-1)), dim=1)
            # alpha_temp = alpha_temp * alpha.reshape(1,-1)
            # alpha = alpha_temp.reshape(alpha.shape)
            alpha = self.grid(alpha)
            x3 = x.repeat([self.n_cells[idx], 1, 1, 1])
            F = grid_sample_2d(alpha, x3, step=self.mode, offset=True)
            features.append(F.sum(0).view(self.n_features, -1).t())

        F = torch.cat(tuple(features), 1)

        #Attention mechanism
        attn_weights = nn.functional.softmax(self.attn(F), dim=1)
        F = (F * attn_weights) #.sum(dim=1).reshape(-1,1)

        return F


################################################################
class GridSelfAttention(nn.Module):
    def __init__(self, device = 'cuda'):
        super(GridSelfAttention, self).__init__()
        
        # This will produce a single value for each grid location
        d_k = 32  # Query and Key dimensions
        self.d_k = d_k
        self.query = nn.Linear(1, d_k).to(device)
        self.key = nn.Linear(1, d_k).to(device)
        self.value = nn.Linear(1, d_k).to(device)
        
        # Positional encoding
        x = torch.linspace(0, 10, 11).repeat(11, 1)
        y = torch.linspace(0, 10, 11).repeat(11, 1).t()
        self.pos = torch.stack((x, y), dim=-1).view(-1, 2).to(device)  # 121x2
        self.pos_embedding = nn.Linear(2, 1).to(device) # 
        self.device = device
        self.reduce_dim = nn.Linear(d_k, 1).to(device)

    def forward(self, x):
        # x shape: [batch_size, feature_dim, 11, 11]
        b, f, _, _ = x.shape
        
        # Aggregating information across the feature dimension
        aggregated = x.mean(dim=1, keepdim=True)  # [batch_size, 1, 11, 11]
        
        # Adding positional encoding
        aggregated = aggregated.view(b, 11 * 11, 1) + self.pos_embedding(self.pos)
        
        q = self.query(aggregated)
        k = self.key(aggregated)
        v = self.value(aggregated)
        
        attn_scores = torch.bmm(q, k.transpose(1, 2)) / (self.d_k ** 0.5)
        attn_scores = nn.functional.softmax(attn_scores, dim=-1)
        
        importance_scores = attn_scores.mean(dim=1) 
        
        importance_scores = importance_scores.view(b, 1, 11, 11)
        
        out = x * importance_scores  # Broadcasting to re-weight the original grid
        
        return out

# Usage:
# x is your grid tensor with shape [batch_size, 4, 11, 11]
# attention_layer = GridSelfAttention()
# output = attention_layer(x)





class GridSelfAttention_grid(nn.Module):
    def __init__(self, n_features, device):
        super(GridSelfAttention_grid, self).__init__()
        self.device = device
        
        # Query, Key, Value transformation matrices
        self.query = nn.Linear(n_features, n_features, bias=False).to(device)
        self.key = nn.Linear(n_features, n_features, bias=False).to(device)
        self.value = nn.Linear(n_features, n_features, bias=False).to(device)
        
        self.softmax = nn.Softmax(dim=-1)
    
    def forward(self, x):
        # Assuming x of shape [batch_size, n_features, height, width]
        
        # Reshape x to be [batch_size, n_features, height*width]
        x_reshape = x.view(x.size(0), -1, x.size(1))
        
        # Calculate queries, keys and values
        Q = self.query(x_reshape)
        K = self.key(x_reshape)
        V = self.value(x_reshape)
        
        # Compute attention scores
        attn_scores = torch.bmm(K.transpose(1, 2), Q) / (self.key.in_features ** 0.5)
        attn_probs = self.softmax(attn_scores)
        
        # Apply attention to values
        attn_output = torch.bmm(attn_probs, V.transpose(1,2))
        
        # Reshape back to original shape
        attn_output = attn_output.view(x.size(0), x.size(1), x.size(2), x.size(3))
        
        return attn_output


class conv_att(nn.Module):
    def __init__(self, n_features):
        super(conv_att, self).__init__()
        self.n_features = n_features

        self.conv = nn.Conv2d(in_channels=self.n_features, out_channels=self.n_features, kernel_size=3, padding=1, bias=False).to(device)
        self.maxpool = nn.MaxPool2d(kernel_size=2, stride=2).to(device)
        # self.batch_norm = nn.BatchNorm2d(num_features=n_features).to(device)
        self.active = nn.Tanh()
        self.grid = GridSelfAttention_grid(self.n_features, device= device)
    
    def forward(self, alpha):
        alpha = self.grid(alpha)
        alpha = self.conv(alpha)
        # alpha = self.batch_norm(alpha)
        alpha = self.active(alpha)
        alpha = self.maxpool(alpha)
        ###########################
        

        return alpha


class Encoder_conv(nn.Module):

    def __init__(self, n_features=4, res=[8, 12, 20, 18, 32], n_cells=2, domain_size=[0, 1, 0, 1], mode='bilinear') -> None:
        super().__init__()
        self.n_features = n_features
        self.mode = mode
        self.res = res
        self.n_cells = n_cells
        self.multiplier = None
        self.F_active = nn.ParameterList([torch.rand(size=(self.n_cells[i], self.n_features, self.res[i], self.res[i]),
                                                    dtype=dtype, device=device).data.uniform_(-1e-5, 1e-5) for i in range(len(self.res))])


        # self.lambda_param = nn.ParameterDict({f'lambda{i}':torch.rand(size = (1,), \
        #     dtype=dtype, device=device).data.uniform_(0.8, 0.99) for i in range(len(self.res))})


        self.domain_size = domain_size
        self.active = nn.Tanh()
        #self.attn = nn.Linear(self.n_features * len(self.res), self.n_features * len(self.res), device=device) 
        
        # Add a convolution layer for feature extraction; choose appropriate out_channels and kernel_size
        
        self.conv = nn.Conv2d(in_channels=self.n_features, out_channels=self.n_features, kernel_size=3, padding=1, bias=False).to(device)
        self.maxpool = nn.MaxPool2d(kernel_size=2, stride=2).to(device)
        
        # padding_size = (3 - 1) // 2
        # self.conv1d = nn.Conv1d(in_channels= 1, 
        #                         out_channels=1, kernel_size=15, padding=padding_size, bias=False).to(device)

        #self.conv_att = nn.ParameterDict({f'conv_att{i}':conv_att(self.n_features) for i in range(len(self.res))})

        # self.batch_norm = nn.BatchNorm2d(num_features=n_features).to(device)

        # self.gradient_history = {idx: [] for idx in range(len(self.res))}
        # self.moving_average = {idx: 0.0 for idx in range(len(self.res))}
        # self.window_size = 25  # N is the number of past gradients you want to consider. 


    def forward(self, x, y):
        features = []
        x_min, x_max, y_min, y_max = self.domain_size
        x = (x - x_min) / (x_max - x_min)
        y = (y - y_min) / (y_max - y_min)
        x = x * 2 - 1
        y = y * 2 - 1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)

        # def mish(x):
        #     return x * torch.tanh(nn.functional.softplus(x))

        # if len(self.gradient_history[0]) > 0:
        #     summ = sum(self.moving_average.values())
        #     values = [ma / summ for ma in self.moving_average.values()]

        for idx, alpha in enumerate(self.F_active):
            # Apply convolution to extract important features from the grid
            
            alpha = self.conv(alpha)
            alpha = self.active(alpha)
            alpha = self.maxpool(alpha)

            x3 = x.repeat([self.n_cells[idx], 1, 1, 1])
            F = grid_sample_2d(alpha, x3, step=self.mode, offset=True)

            features.append(F.sum(0).view(self.n_features, -1).t())

        F = torch.cat(tuple(features), 1)

        self.F = F
        return F



class Encoder_conv_skip(nn.Module):

    def __init__(self, n_features=4, res=[64], n_cells=[2], q = 3, domain_size=[0, 1, 0, 1], mode='bilinear') -> None:
        super().__init__()
        self.n_features = n_features
        self.mode = mode
        self.res = res
        self.n_cells = n_cells
        self.multiplier = None
        self.F_active = nn.ParameterList([torch.rand(size=(self.n_cells[i], self.n_features, self.res[i], self.res[i]),
                                                    dtype=dtype, device=device).data.uniform_(-1e-5, 1e-5) for i in range(len(self.res))])

        for param in self.F_active:
            nn.init.xavier_uniform_(param)

        self.attention = nn.MultiheadAttention(n_features * len(res) * q, num_heads=1, batch_first=True).to(device)

        self.domain_size = domain_size
        self.active = nn.Tanh()
        
        self.conv = nn.ModuleList([nn.Conv2d(in_channels=self.n_features, 
                                             out_channels=self.n_features,
                                               kernel_size=3, padding=1,
                                                 bias=False).to(device) for _ in range(q)]) 
        
        self.maxpool = nn.ModuleList([nn.MaxPool2d(kernel_size=2, stride=2).to(device) for _ in range(q)])
        

    def forward(self, x, y):
        features = []
        x_min, x_max, y_min, y_max = self.domain_size
        x = (x - x_min) / (x_max - x_min)
        y = (y - y_min) / (y_max - y_min)
        x = x * 2 - 1
        y = y * 2 - 1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)


        for idx, alpha in enumerate(self.F_active):
            # Apply convolution to extract important features from the grid

            # x3 = x.repeat([self.n_cells[idx], 1, 1, 1])
            # F = grid_sample_2d(alpha, x3, step=self.mode, offset=True)
            # features.append(F.sum(0).view(self.n_features, -1).t())

            for conv, maxpool in zip(self.conv, self.maxpool):

                alpha = conv(alpha)
                alpha = self.active(alpha)
                alpha = maxpool(alpha)

                # if alpha.shape[-1] < 4:
                #     break

                x3 = x.repeat([self.n_cells[idx], 1, 1, 1])
                F = grid_sample_2d(alpha, x3, step=self.mode, offset=True)
            features.append(F.sum(0).view(self.n_features, -1).t())

        F = torch.cat(tuple(features), 1)

        #F, _ = self.attention(F.unsqueeze(1), F.unsqueeze(1), F.unsqueeze(1))

        self.F = F #F.squeeze(1)
        return self.F




class Encoder_multi_conv_attn(nn.Module):

    def __init__(self, n_features=4, res=[64], n_cells=[2], q = 3, domain_size=[0, 1, 0, 1], mode='bilinear') -> None:
        super().__init__()
        self.n_features = n_features
        self.mode = mode
        self.res = res
        self.n_cells = n_cells
        self.multiplier = None
        self.F_active = nn.ParameterList([torch.rand(size=(self.n_cells[i], self.n_features, self.res[i], self.res[i]),
                                                    dtype=dtype, device=device).data.uniform_(-1e-5, 1e-5) for i in range(len(self.res))])

        # for param in self.F_active:
        #     nn.init.xavier_uniform_(param)


        attention_dim = 32

        self.query_projection = nn.Linear(n_features * len(res) * q, attention_dim).to(device)
        self.key_projection = nn.Linear(n_features * len(res) * q, attention_dim).to(device)
        self.value_projection = nn.Linear(n_features * len(res) * q, attention_dim).to(device)


        self.attention = nn.MultiheadAttention(attention_dim, num_heads=1, batch_first=True).to(device)




        self.domain_size = domain_size
        self.active = nn.Tanh()
        
        self.conv = nn.ModuleList([nn.Conv2d(in_channels=self.n_features, 
                                             out_channels=self.n_features,
                                               kernel_size=3, padding=1,
                                                 bias=False).to(device) for _ in range(len(res))]) 
        
        self.maxpool = nn.ModuleList([nn.MaxPool2d(kernel_size=2, stride=2).to(device) for _ in range(len(res))])
        

    def forward(self, x, y):
        features = []
        x_min, x_max, y_min, y_max = self.domain_size
        x = (x - x_min) / (x_max - x_min)
        y = (y - y_min) / (y_max - y_min)
        x = x * 2 - 1
        y = y * 2 - 1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)


        for idx, alpha in enumerate(self.F_active):


            alpha = self.conv[idx](alpha)
            alpha = self.active(alpha)
            alpha = self.maxpool[idx](alpha)

            # if alpha.shape[-1] <= 4:
            #     break

            x3 = x.repeat([self.n_cells[idx], 1, 1, 1])
            F = grid_sample_2d(alpha, x3, step=self.mode, offset=True)
            features.append(F.sum(0).view(self.n_features, -1).t())

        F = torch.cat(tuple(features), 1)

        Q = self.query_projection(F)
        K = self.key_projection(F)
        V = self.value_projection(F)


        F, _ = self.attention(Q.unsqueeze(1), K.unsqueeze(1), V.unsqueeze(1))

        self.F = F.squeeze(1)
        return self.F




class Encoder_conv_shirink_kernel(nn.Module):

    def __init__(self, n_features=4, res=[64], n_cells=[2], q = 3, domain_size=[0, 1, 0, 1], mode='bilinear') -> None:
        super().__init__()
        self.n_features = n_features
        self.mode = mode
        self.res = res
        self.n_cells = n_cells
        self.multiplier = None
        self.inherit = []
        self.F_active = nn.ParameterList([torch.rand(size=(self.n_cells[i], self.n_features, self.res[i], self.res[i]),
                                                    dtype=dtype, device=device).data.uniform_(-1e-5, 1e-5) for i in range(len(self.res))])

        ksize = [ 12 , 7 , 3]

        self.domain_size = domain_size
        self.active = nn.Tanh()
        
        self.conv = nn.ModuleList([nn.Conv2d(in_channels=self.n_features, 
                                             out_channels=self.n_features,
                                               kernel_size=ksize[i], padding=1,
                                                 bias=False).to(device) for i in range(q)]) 
        
        self.maxpool = nn.ModuleList([nn.MaxPool2d(kernel_size=2, stride=2).to(device) for _ in range(q)])
        

    def forward(self, x, y , inference = False):
        
        features = []
        x_min, x_max, y_min, y_max = self.domain_size
        x = (x - x_min) / (x_max - x_min)
        y = (y - y_min) / (y_max - y_min)
        x = x * 2 - 1
        y = y * 2 - 1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)


        for idx, alpha in enumerate(self.F_active):

            x3 = x.repeat([self.n_cells[idx], 1, 1, 1])
            if inference: 
                self.inherit.append(grid_sample_2d(alpha, x3, step=self.mode, offset=True))
            # Apply convolution to extract important features from the grid

            # x3 = x.repeat([self.n_cells[idx], 1, 1, 1])
            # F = grid_sample_2d(alpha, x3, step=self.mode, offset=True)
            # features.append(F.sum(0).view(self.n_features, -1).t())

            for conv, maxpool in zip(self.conv, self.maxpool):

                
                alpha = conv(alpha)
                alpha = self.active(alpha)
                alpha = maxpool(alpha)
                if inference:

                    self.inherit.append(grid_sample_2d(alpha, x3, step=self.mode, offset=True))


                if alpha.shape[-1] <= 4:
                    break

            if  inference:
                Fs = self.inherit
                for f in Fs:
                    features.append(f.sum(0).view(self.n_features, -1).t())

            else:
                F = grid_sample_2d(alpha, x3, step=self.mode, offset=True) #grid_sample_2d(alpha, x3, step=self.mode, offset=True)
                features.append(F.sum(0).view(self.n_features, -1).t())


        if inference:
            #print("oh no" , features[1].shape)
            F = features #[torch.cat(tuple(feature), 1) for feature in features]

        else:
            F = torch.cat(tuple(features), 1)

        self.F = F
        return F




class Encoder_conv_ave(nn.Module):

    def __init__(self, n_features=4, res=[64], n_cells=[2], q = 3, domain_size=[0, 1, 0, 1], mode='bilinear') -> None:
        super().__init__()
        self.n_features = n_features
        self.mode = mode
        self.res = res
        self.n_cells = n_cells
        self.multiplier = None
        self.inherit = []
        self.F_active = nn.ParameterList([torch.rand(size=(self.n_cells[i], self.n_features, self.res[i], self.res[i]),
                                                    dtype=dtype, device=device).data.uniform_(-1e-5, 1e-5) for i in range(len(self.res))])

        ksize = [ 3 , 3 , 3]

        self.domain_size = domain_size
        self.active = nn.Tanh()
        
        self.conv = nn.ModuleList([nn.Conv2d(in_channels=self.n_features, 
                                             out_channels=self.n_features,
                                               kernel_size=ksize[i], padding=1,
                                                 bias=False).to(device) for i in range(q)]) 
        
        self.maxpool = nn.ModuleList([nn.MaxPool2d(kernel_size=2, stride=2).to(device) for _ in range(q)])
        

    def forward(self, x, y , inference = False):
        
        features = []
        x_min, x_max, y_min, y_max = self.domain_size
        x = (x - x_min) / (x_max - x_min)
        y = (y - y_min) / (y_max - y_min)
        x = x * 2 - 1
        y = y * 2 - 1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)

        counter = 0
        for idx, alpha in enumerate(self.F_active):

            x3 = x.repeat([self.n_cells[idx], 1, 1, 1])
            F = grid_sample_2d(alpha, x3, step=self.mode, offset=True)
            features = F.sum(0).view(self.n_features, -1).t() #append(F.sum(0).view(self.n_features, -1).t())

            for conv, maxpool in zip(self.conv, self.maxpool):

                counter += 1
                alpha = conv(alpha)
                alpha = self.active(alpha)
                alpha = maxpool(alpha)

                F = grid_sample_2d(alpha, x3, step=self.mode, offset=True) #grid_sample_2d(alpha, x3, step=self.mode, offset=True)
                features += F.sum(0).view(self.n_features, -1).t() #.append(F.sum(0).view(self.n_features, -1).t())


        #F = torch.cat(tuple(features), 1)

        self.F = features/counter
        return self.F













if __name__ == '__main__':
    encoder = Encoder()
    features = encoder(torch.rand(1,1).to(device), torch.rand(1,1).to(device))
    print(features)
    print(features.shape)