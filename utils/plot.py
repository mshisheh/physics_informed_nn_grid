import torch
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.colors import Normalize
import pandas as pd
from torch.autograd import grad
import seaborn as sns
from sklearn.metrics import r2_score

class Derivatives():
    def __init__(self, model ,name = 'NS', params = None) -> None:
        self.name = name
        self.model = model
        self.params = params
    
    def get(self, x, y):
        if self.name == 'NS':
            print(x.shape)
            #x = x.requires_grad
            #y = y.requires_grad

            S = self.model(x,y)
            psi = S[:,0].reshape(-1,1)
            p = S[:,1].reshape(-1,1)

            u = torch.autograd.grad(psi, y, torch.ones_like(psi), True, True)[0]
            v = -1*torch.autograd.grad(psi, x, torch.ones_like(psi), True, True)[0]

            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]

            v_y = torch.autograd.grad(v, y, torch.ones_like(v), True, True)[0]
            v_yy = torch.autograd.grad(v_y, y, torch.ones_like(v_y), True, True)[0]

            p_x = torch.autograd.grad(p, x, torch.ones_like(p), True, True)[0]


            return u_x ,  u_xx , v_y , v_yy , p_x
        
        if self.name == 'Sinusoid':
            u = self.model(x,y)
            u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
            u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u), True, True)[0]
            return u_x, u_xx

        else:
            raise ValueError('Wrong self.name')

    def exact(self, x, y):
        if self.name == 'NS':
            f = torch.exp(torch.Tensor([-2*0.01*1])).to('cuda:0')

            u_x = -torch.sin(x)*torch.sin(y)*f
            u_xx = -torch.cos(x)*torch.sin(y)*f

            v_y = -u_x
            v_yy = torch.sin(x)*torch.cos(y)*f

            p_x = 0.5*torch.sin(2*x)*f**2


            return u_x ,  u_xx , v_y , v_yy , p_x

        if self.name == 'Sinusoid':
            u_x = torch.cos(self.params['omega']*x)
            u_xx = -1.0 * self.params['omega'] * torch.sin(self.params['omega']*x)
            
            return u_x, u_xx
            




        if self.name == 'Sinusoid':
            f = torch.Tensor([1]).to('cuda:0')

            u_x = -torch.sin(x)*torch.sin(y)*f
            u_xx = -torch.cos(x)*torch.sin(y)*f

            return u_x ,  u_xx , v_y , v_yy , p_x



def contour_helper(grid, x, y, z, title, levels=100):
    # getting the value range
    vmin = np.min(z)
    vmax = np.max(z)
    # plotting a contour
    plt.subplot(grid)
    plt.tricontour(x, y, z, colors='k', linewidths=0.2, levels=levels)
    plt.tricontourf(x, y, z, cmap = 'rainbow',levels=levels, norm=Normalize(vmin=vmin, vmax=vmax))
    plt.title(title)
    cbar = plt.colorbar(pad=0.02, aspect=22, format='%.6f')   # '%.0e''%.3f'
    cbar.mappable.set_clim(vmin, vmax)
    
# plotting streamlines
def streamplot_helper(grid, x, y, zx, zy, title, density=1.2):
    plt.subplot(grid)
    plt.streamplot(x, y, zx, zy, color=torch.sqrt(zx**2 + zy**2).numpy(), cmap='rainbow', density=density, linewidth=1.2)
    plt.title(title)
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    cbar = plt.colorbar(pad=0.02, aspect=22, format='%.2f')

# plotting test results
def plot_uvp(xp_mesh, yp_mesh, u, v, p):
    fig = plt.figure(figsize=(15, 5))
    gs = GridSpec(1, 3)
    #self.contour_helper(gs[0, 0], self.xp_mesh, self.yp_mesh, self.psip_mesh, '$\psi$')
    contour_helper(gs[0, 0], xp_mesh, yp_mesh, u, '$u$')
    contour_helper(gs[0, 1], xp_mesh, yp_mesh, v, '$v$')
    contour_helper(gs[0, 2], xp_mesh, yp_mesh, p, '$p$')
    #contour_helper(gs[2, 0], self.xp_mesh, self.yp_mesh, self.velocity_mesh, '$\sqrt{u^2 + v^2}$')
    #self.streamplot_helper(gs[2, 1], self.xp_mesh, self.yp_mesh, self.up_mesh, self.vp_mesh, '$\sqrt{u^2 + v^2}$')
    plt.tight_layout()
    plt.show()

def plot_u(xp_mesh, yp_mesh, u):
    fig = plt.figure(figsize=(5, 5))
    gs = GridSpec(1, 1)
    contour_helper(gs[0, 0], xp_mesh, yp_mesh, u, '$u$')
    plt.tight_layout()
    plt.show()

def compare_plot(xp_mesh, yp_mesh, sol, sol_p, ntest = 100, title = 'default'):
    u,v,p = sol[:,0].cpu().numpy(), sol[:,1].cpu().numpy(), sol[:,2].cpu().numpy()
    psi = sol_p[:,0].reshape(-1,1)
    pp = sol_p[:,1].reshape(-1,1).detach().cpu().numpy()
    up = torch.autograd.grad(psi, yp_mesh, torch.ones_like(psi), True, True)[0].detach().cpu().numpy()
    vp = -1*torch.autograd.grad(psi, xp_mesh, torch.ones_like(psi), True, True)[0].detach().cpu().numpy()
    subplot_size = 3.
    (width, height) = (3.6*subplot_size, 3*subplot_size) #5 col and 3 rows, each of size subplot_size
    fig = plt.figure(figsize = (width, height))
    gs = GridSpec(3, 3)
    #self.contour_helper(gs[0, 0], self.xp_mesh, self.yp_mesh, self.psip_mesh, '$\psi$')
    xp_mesh = xp_mesh.detach().cpu().numpy()
    yp_mesh = yp_mesh.detach().cpu().numpy()
    contour_helper(gs[0, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,), '$u$')
    contour_helper(gs[0, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), v.reshape(-1,), '$v$')
    contour_helper(gs[0, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), p.reshape(-1,), '$p$')
    contour_helper(gs[1, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), up.reshape(-1,), '$up$')
    contour_helper(gs[1, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), vp.reshape(-1,), '$vp$')
    contour_helper(gs[1, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), pp.reshape(-1,), '$pp$')
    contour_helper(gs[2, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), up.reshape(-1,)-u.reshape(-1,), '$u$ error')
    contour_helper(gs[2, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), vp.reshape(-1,)-v.reshape(-1,), '$v$ error')
    contour_helper(gs[2, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), pp.reshape(-1,)-p.reshape(-1,), '$p$ error')

    plt.tight_layout()
    plt.savefig('./plots/predictions/jpgs/'+title+ 'solution_.jpg', dpi = 300)
    #plt.show()

    plt.figure(figsize = (8,6))
    _ = plt.plot(u.reshape(-1,), up.reshape(-1,), label = f'R = {np.round(r2_score(u.reshape(-1,), up.reshape(-1,)),3)}')
    _ = plt.plot([u.reshape(-1,).min(), u.reshape(-1,).max()], [u.reshape(-1,).min(), u.reshape(-1,).max()], c= 'r')
    plt.xlabel('True Value', fontsize = 14)
    plt.ylabel('Predicted Value', fontsize = 14)
    plt.legend()
    plt.savefig('./plots/predictions/jpgs/'+title+'_u_vs_up.jpg', dpi = 300)

    plt.figure(figsize = (8,6))
    _ = plt.plot(v.reshape(-1,), vp.reshape(-1,), label = f'R = {np.round(r2_score(v.reshape(-1,), vp.reshape(-1,)),3)}')
    _ = plt.plot([v.reshape(-1,).min(), v.reshape(-1,).max()], [v.reshape(-1,).min(), v.reshape(-1,).max()], c= 'r')
    plt.xlabel('True Value', fontsize = 14)
    plt.ylabel('Predicted Value', fontsize = 14)
    plt.legend()
    plt.savefig('./plots/predictions/jpgs/'+title+'_v_vs_vp.jpg', dpi = 300)

    RMSE = (np.sum((u.reshape(-1,) - up.reshape(-1,))**2.0))/(np.sum((u.reshape(-1,))**2.0)) + \
                             (np.sum((v.reshape(-1,) - vp.reshape(-1,))**2.0))/(np.sum((v.reshape(-1,))**2.0)) + \
                             (np.sum((p.reshape(-1,) - pp.reshape(-1,))**2.0))/(np.sum((p.reshape(-1,))**2.0))
    print("____________________________________________________________________________________________________________")
    print(f"Relative MSE is {RMSE}")
    print("____________________________________________________________________________________________________________")

    solution = pd.DataFrame({'x':xp_mesh.reshape(-1,), 
                  'y':yp_mesh.reshape(-1,), 'u':u.reshape(-1,), 'up':up.reshape(-1,), 
                  'v':v.reshape(-1,), 'vp':vp.reshape(-1,), 'p':p.reshape(-1,), 'pp':pp.reshape(-1,)})

    #solution.to_csv(f'./plots/predictions/csvs/solution{title}.csv')



def compare_plot_u(xp_mesh, yp_mesh, sol, sol_p, ntest = 100, title = 'default'):
    u = sol[:,0].cpu().numpy()
    up = sol_p[:,0].reshape(-1,1).detach().cpu().numpy()
    subplot_size = 3.
    (width, height) = (3*subplot_size, 1*subplot_size) #5 col and 3 rows, each of size subplot_size
    fig = plt.figure(figsize = (width, height))
    gs = GridSpec(1, 3)
    #self.contour_helper(gs[0, 0], self.xp_mesh, self.yp_mesh, self.psip_mesh, '$\psi$')
    xp_mesh = xp_mesh.detach().cpu().numpy()
    yp_mesh = yp_mesh.detach().cpu().numpy()
    contour_helper(gs[0, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,), '$u$')
    contour_helper(gs[0, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), up.reshape(-1,), '$u_p$')
    contour_helper(gs[0, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,) - up.reshape(-1,), '$Error$')

    print("____________________________________________________________________________________________________________")
    print(f"Relative MSE is {(np.sum((u.reshape(-1,) - up.reshape(-1,))**2.0))/(np.sum((u.reshape(-1,))**2.0))}")
    print("____________________________________________________________________________________________________________")

    solution = pd.DataFrame({'x':xp_mesh.reshape(-1,), 
                  'y':yp_mesh.reshape(-1,), 'u':u.reshape(-1,), 'up':up.reshape(-1,), 'Error':u.reshape(-1,) - up.reshape(-1,)})

    #solution.to_csv(f'./plots/predictions/csvs/solution{title}.csv')

        # Open a file with access mode 'a'       
    with open( f'./log/history_{title}.txt', 'a') as file_object:
        # Append 'hello' at the end of file
        file_object.write('\n'+f"Relative MSE is {(np.sum((u.reshape(-1,) - up.reshape(-1,))**2.0))/(np.sum((u.reshape(-1,))**2.0))}")

    plt.tight_layout()
    plt.savefig('./plots/predictions/jpgs/'+title+ '_solution.jpg', dpi = 300)
    #plt.show()

    plt.figure(figsize = (8,6))
    _ = plt.plot(u.reshape(-1,), up.reshape(-1,), label = f'R = {np.round(r2_score(u.reshape(-1,), up.reshape(-1,)),3)}')
    _ = plt.plot([u.reshape(-1,).min(), u.reshape(-1,).max()], [u.reshape(-1,).min(), u.reshape(-1,).max()], c= 'r')
    plt.xlabel('True Value', fontsize = 14)
    plt.ylabel('Predicted Value', fontsize = 14)
    plt.legend()
    plt.savefig('./plots/predictions/jpgs/'+title+'_y_vs_yp.jpg', dpi = 300)

def compare_plot_uv(xp_mesh, yp_mesh, sol, sol_p, ntest = 100, title = 'default'):
    u,v = sol[:,0].cpu().numpy(), sol[:,1].cpu().numpy()
    up = sol_p[:,0].reshape(-1,1).detach().cpu().numpy()
    vp = sol_p[:,1].reshape(-1,1).detach().cpu().numpy()
    subplot_size = 3.
    (width, height) = (2*subplot_size, 3*subplot_size) #5 col and 3 rows, each of size subplot_size
    fig = plt.figure(figsize = (width, height))
    gs = GridSpec(3, 2)
    #self.contour_helper(gs[0, 0], self.xp_mesh, self.yp_mesh, self.psip_mesh, '$\psi$')
    xp_mesh = xp_mesh.detach().cpu().numpy()
    yp_mesh = yp_mesh.detach().cpu().numpy()
    contour_helper(gs[0, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,), '$u$')
    contour_helper(gs[0, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), v.reshape(-1,), '$v$')
    contour_helper(gs[1, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), up.reshape(-1,), '$up$')
    contour_helper(gs[1, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), vp.reshape(-1,), '$vp$')
    contour_helper(gs[2, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,)-up.reshape(-1,), '$u-up$')
    contour_helper(gs[2, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), v.reshape(-1,)-vp.reshape(-1,), '$v-vp$')

    plt.tight_layout()
    plt.savefig('./plots/predictions/jpgs/'+title+'_solution.jpg', dpi = 300)
    #plt.show()


def compare_plot_si(xp_mesh, yp_mesh, sol, sol_p, ntest = 100, title = 'default'):
    u,v = sol[:,0].cpu().numpy(), sol[:,1].cpu().numpy()
    up = torch.autograd.grad(sol_p, yp_mesh, torch.ones_like(sol_p), True, True)[0].detach().cpu().numpy()
    vp = -1*torch.autograd.grad(sol_p, xp_mesh, torch.ones_like(sol_p), True, True)[0].detach().cpu().numpy()
    subplot_size = 3.
    (width, height) = (2*subplot_size, 3*subplot_size) #5 col and 3 rows, each of size subplot_size
    fig = plt.figure(figsize = (width, height))
    gs = GridSpec(3, 2)
    #self.contour_helper(gs[0, 0], self.xp_mesh, self.yp_mesh, self.psip_mesh, '$\psi$')
    xp_mesh = xp_mesh.detach().cpu().numpy()
    yp_mesh = yp_mesh.detach().cpu().numpy()
    contour_helper(gs[0, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,), '$u$')
    contour_helper(gs[0, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), v.reshape(-1,), '$v$')
    contour_helper(gs[1, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), up.reshape(-1,), '$up$')
    contour_helper(gs[1, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), vp.reshape(-1,), '$vp$')
    contour_helper(gs[2, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,)-up.reshape(-1,), '$u-up$')
    contour_helper(gs[2, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), v.reshape(-1,)-vp.reshape(-1,), '$v-vp$')

    plt.tight_layout()
    plt.savefig('./plots/predictions/jpgs/solution_'+title+ '.jpg', dpi = 300)
    #plt.show()


def compare_plot_vorticity(xp_mesh, yp_mesh, sol, sol_p, ntest = 100, title = 'default'):
    u,v,p = sol[:,0].cpu().numpy(), sol[:,1].cpu().numpy(), sol[:,2].cpu().numpy()
    pp = sol_p[...,2].reshape(-1,1).detach().cpu().numpy()
    dsi_dx = sol_p[...,1].reshape(-1,1).detach().cpu().numpy()
    dsi_dy = sol_p[...,2].reshape(-1,1).detach().cpu().numpy()

    up = dsi_dy
    vp = -1 * dsi_dx

    subplot_size = 3.
    (width, height) = (3*subplot_size, 2*subplot_size) #5 col and 3 rows, each of size subplot_size
    fig = plt.figure(figsize = (width, height))
    gs = GridSpec(2, 3)
    #self.contour_helper(gs[0, 0], self.xp_mesh, self.yp_mesh, self.psip_mesh, '$\psi$')
    xp_mesh = xp_mesh.detach().cpu().numpy()
    yp_mesh = yp_mesh.detach().cpu().numpy()
    contour_helper(gs[0, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,), '$u$')
    contour_helper(gs[0, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), v.reshape(-1,), '$v$')
    contour_helper(gs[0, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), p.reshape(-1,), '$p$')
    contour_helper(gs[1, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), up.reshape(-1,), '$up$')
    contour_helper(gs[1, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), vp.reshape(-1,), '$vp$')
    contour_helper(gs[1, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), pp.reshape(-1,), '$pp$')

    plt.tight_layout()
    plt.savefig('./plots/predictions/jpgs/solution_'+title+ '.jpg', dpi = 300)
    #plt.show()

def compare_plot_1D(xp_mesh, yp_mesh, sol, sol_p, ntest = 100, title = 'default'):
    u,v,p = sol[:,0].cpu().numpy(), sol[:,1].cpu().numpy(), sol[:,2].cpu().numpy()
    up = sol_p.reshape(-1,1).detach().cpu().numpy()
    vp = sol_p.reshape(-1,1).detach().cpu().numpy()
    pp = sol_p.reshape(-1,1).detach().cpu().numpy()
    subplot_size = 3.
    (width, height) = (3*subplot_size, 2*subplot_size) #5 col and 3 rows, each of size subplot_size
    fig = plt.figure(figsize = (width, height))
    gs = GridSpec(2, 3)
    #self.contour_helper(gs[0, 0], self.xp_mesh, self.yp_mesh, self.psip_mesh, '$\psi$')
    xp_mesh = xp_mesh.detach().cpu().numpy()
    yp_mesh = yp_mesh.detach().cpu().numpy()
    contour_helper(gs[0, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,), '$u$')
    contour_helper(gs[0, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), v.reshape(-1,), '$v$')
    contour_helper(gs[0, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), p.reshape(-1,), '$p$')
    contour_helper(gs[1, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), up.reshape(-1,), '$up$')
    contour_helper(gs[1, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), vp.reshape(-1,), '$vp$')
    contour_helper(gs[1, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), pp.reshape(-1,), '$pp$')

    plt.tight_layout()
    plt.savefig('./plots/predictions/jpgs/solution_'+title+ '.jpg', dpi = 300)
    #plt.show()

def der_plot(xp_mesh, yp_mesh, model, ntest = 100, title = 'default',pde_name = 'NS', params = None):
    to_numpy = lambda x : x[:,0].cpu().detach().numpy()
    
    Der = Derivatives(model = model, name= pde_name, params = params)
    u_x ,  u_xx , v_y , v_yy , p_x = Der.get(xp_mesh , yp_mesh)
    u_x ,  u_xx , v_y , v_yy , p_x = to_numpy(u_x) , to_numpy(u_xx) , to_numpy(v_y) , to_numpy(v_yy) , to_numpy(p_x)

    U_x ,  U_xx , V_y , V_yy , P_x = Der.exact(xp_mesh , yp_mesh)
    U_x ,  U_xx , V_y , V_yy , P_x = to_numpy(U_x) , to_numpy(U_xx) , to_numpy(V_y) , to_numpy(V_yy) , to_numpy(P_x)
    
    subplot_size = 3.
    (width, height) = (6*subplot_size, 3*subplot_size) #5 col and 3 rows, each of size subplot_size
    fig = plt.figure(figsize = (width, height))
    gs = GridSpec(3, 6)
    #self.contour_helper(gs[0, 0], self.xp_mesh, self.yp_mesh, self.psip_mesh, '$\psi$')
    xp_mesh = xp_mesh.detach().cpu().numpy()
    yp_mesh = yp_mesh.detach().cpu().numpy()
    contour_helper(gs[0, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), U_x.reshape(-1,), title = 'u_x')
    contour_helper(gs[0, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), U_xx.reshape(-1,), title = 'u_xx')
    contour_helper(gs[0, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), V_y.reshape(-1,), title = 'v_y')
    contour_helper(gs[0, 3], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), V_yy.reshape(-1,), title = 'v_yy')
    contour_helper(gs[0, 4], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), P_x.reshape(-1,), title = 'p_x')

    contour_helper(gs[1, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u_x.reshape(-1,),title = 'predicted u_x')
    contour_helper(gs[1, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u_xx.reshape(-1,) ,title = 'predicted u_xx')
    contour_helper(gs[1, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), v_y.reshape(-1,), title = 'predicted v_y')
    contour_helper(gs[1, 3], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), v_yy.reshape(-1,), title = 'predicted v_yy')
    contour_helper(gs[1, 4], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), p_x.reshape(-1,), title = 'predicted p_x')

    contour_helper(gs[2, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), (u_x-U_x).reshape(-1,), title = 'u_x Error')
    contour_helper(gs[2, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), (u_xx-U_xx).reshape(-1,), title = 'u_xx Error')
    contour_helper(gs[2, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), (v_y-V_y).reshape(-1,), title = 'v_y Error')
    contour_helper(gs[2, 3], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), (v_yy-V_yy).reshape(-1,), title = 'v_yy Error')
    contour_helper(gs[2, 4], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), (p_x-P_x).reshape(-1,), title = 'p_x Error')


    # contour_helper(gs[1, 5], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), (u_x+v_y).reshape(-1,), title = 'Continuity Check')

    plt.tight_layout()
    plt.savefig('./plots/deriv_'+title+ '.jpg', dpi = 300)
    #plt.show()


    plt.figure(figsize = (8,6))
    _ = plt.plot(U_x.reshape(-1,), u_x.reshape(-1,), label = f'R = {np.round(r2_score(U_x.reshape(-1,), u_x.reshape(-1,)),3)}')
    _ = plt.plot([U_x.reshape(-1,).min(), U_x.reshape(-1,).max()], [U_x.reshape(-1,).min(), U_x.reshape(-1,).max()], c= 'r')
    plt.xlabel('u_x True Value', fontsize = 14)
    plt.ylabel('u_x Predicted Value', fontsize = 14)
    plt.legend()
    plt.savefig('./plots/predictions/jpgs/'+title+'_ux_vs_uxp.jpg', dpi = 300)


    plt.figure(figsize = (8,6))
    _ = plt.plot(U_xx.reshape(-1,), u_xx.reshape(-1,), label = f'R = {np.round(r2_score(U_xx.reshape(-1,), u_xx.reshape(-1,)),3)}')
    _ = plt.plot([U_xx.reshape(-1,).min(), U_xx.reshape(-1,).max()], [U_xx.reshape(-1,).min(), U_xx.reshape(-1,).max()], c= 'r')
    plt.xlabel('u_xx True Value', fontsize = 14)
    plt.ylabel('u_xx Predicted Value', fontsize = 14)
    plt.legend()
    plt.savefig('./plots/predictions/jpgs/'+title+'_uxx_vs_uxxp.jpg', dpi = 300)


def der_plot_u(xp_mesh, yp_mesh, model, ntest = 100, title = 'default', pde_name = 'NS', params = None):
    to_numpy = lambda x : x[:,0].cpu().detach().numpy()
    
    Der = Derivatives(model = model, name= pde_name, params = params)
    u_x ,  u_xx = Der.get(xp_mesh , yp_mesh)
    u_x ,  u_xx = to_numpy(u_x) , to_numpy(u_xx) 

    U_x ,  U_xx = Der.exact(xp_mesh , yp_mesh)
    U_x ,  U_xx = to_numpy(U_x) , to_numpy(U_xx) 

    
    subplot_size = 3.
    (width, height) = (3*subplot_size, 2*subplot_size) #5 col and 3 rows, each of size subplot_size
    fig = plt.figure(figsize = (width, height))
    gs = GridSpec(2, 3)
    #self.contour_helper(gs[0, 0], self.xp_mesh, self.yp_mesh, self.psip_mesh, '$\psi$')
    xp_mesh = xp_mesh.detach().cpu().numpy()
    yp_mesh = yp_mesh.detach().cpu().numpy()
    contour_helper(gs[0, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), U_x.reshape(-1,), title = 'u_x')
    contour_helper(gs[0, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), U_xx.reshape(-1,), title = 'u_xx')
    contour_helper(gs[0, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), (u_x-U_x).reshape(-1,), title = 'u_x Error')

    contour_helper(gs[1, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u_x.reshape(-1,), title = 'predicted u_x')
    contour_helper(gs[1, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u_xx.reshape(-1,), title = 'predicted u_xx')
    contour_helper(gs[1, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), (u_xx-U_xx).reshape(-1,), title = 'u_xx Error')

    


    plt.tight_layout()
    plt.savefig('./plots/deriv_'+title+ '.jpg', dpi = 300)
    #plt.show()


    plt.figure(figsize = (8,6))
    _ = plt.plot(U_x.reshape(-1,), u_x.reshape(-1,), label = f'R = {np.round(r2_score(U_x.reshape(-1,), u_x.reshape(-1,)),3)}')
    _ = plt.plot([U_x.reshape(-1,).min(), U_x.reshape(-1,).max()], [U_x.reshape(-1,).min(), U_x.reshape(-1,).max()], c= 'r')
    plt.xlabel('u_x True Value', fontsize = 14)
    plt.ylabel('u_x Predicted Value', fontsize = 14)
    plt.legend()
    plt.savefig('./plots/predictions/jpgs/'+title+'_ux_vs_uxp.jpg', dpi = 300)


    plt.figure(figsize = (8,6))
    _ = plt.plot(U_xx.reshape(-1,), u_xx.reshape(-1,), label = f'R = {np.round(r2_score(U_xx.reshape(-1,), u_xx.reshape(-1,)),3)}')
    _ = plt.plot([U_xx.reshape(-1,).min(), U_xx.reshape(-1,).max()], [U_xx.reshape(-1,).min(), U_xx.reshape(-1,).max()], c= 'r')
    plt.xlabel('u_xx True Value', fontsize = 14)
    plt.ylabel('u_xx Predicted Value', fontsize = 14)
    plt.legend()
    plt.savefig('./plots/predictions/jpgs/'+title+'_uxx_vs_uxxp.jpg', dpi = 300)






def plot_helper(grid, x, y, color = 'k', label = None, title = None):
    # getting the value range
    vmin = np.min(y)
    vmax = np.max(y)
    # plotting a contour
    plt.subplot(grid)
    plt.semilogy(x[20:], y[20:], color= color, label = label)
    plt.title(title)
    plt.legend()
    plt.ylim((vmin, vmax))



def loss_plot(train_loss, val_loss, title = 'default'):
    fig = plt.figure(figsize=(15,5))
    gs = GridSpec(1,3)
    
    epochs = len(train_loss['pde'])

    plot_helper(gs[0,0], x = range(epochs), y = train_loss['pde'], color = 'b', label='$train loss$')
    plot_helper(gs[0,0], x = range(epochs), y = val_loss['pde'], color = 'r', label='$val loss$', title = 'PDE loss')

    plot_helper(gs[0,1], x = range(epochs), y = train_loss['bc'], color = 'b', label='$train loss$')
    plot_helper(gs[0,1], x = range(epochs), y = val_loss['bc'], color = 'r', label='$val loss$', title = 'BC loss')

    plot_helper(gs[0,2], x = range(epochs), y = train_loss['total'], color = 'b', label='$train loss$')
    plot_helper(gs[0,2], x = range(epochs), y = val_loss['total'], color = 'r', label='$val loss$', title = 'Total loss')

    fig.supxlabel('Epochs')
    fig.supylabel('MSE Error')

    losses = pd.DataFrame({'PDE_loss_train_loss':train_loss['pde'], 'PDE loss_val_loss':val_loss['pde'],
                            'BC_loss_train_loss':train_loss['bc'], 'BC_loss_val_loss':val_loss['bc'],
                            'total_loss_train_loss':train_loss['total'], 'total_loss_val_loss':val_loss['total']})
    
    # columns = pd.MultiIndex.from_tuples(
    # [(name, sub_name) for name in losses.columns for sub_name in losses.index])
    # losses.columns = columns
    #losses.to_csv(f'./plots/loss/csvs/losses_{title}.csv')


    plt.savefig('./plots/loss/jpgs/loss_'+title+'.jpg', dpi = 300)
    #plt.show()


def hist_helper(grid, bc_values, pde_values, label = None, title = None):
    plt.subplot(grid)
    sns.kdeplot(bc_values, shade = False, label = 'BC')
    sns.kdeplot(pde_values, shade = False, label = 'PDE')
    plt.title(title)
    plt.legend()

def histlog_helper(grid, bc_values, pde_values, label = None, title = None):
    plt.subplot(grid)
    sns.kdeplot(bc_values, shade = False, label = 'BC' , log_scale = True)
    sns.kdeplot(pde_values, shade = False, label = 'PDE', log_scale = True)
    plt.title(title)
    plt.legend()


def grad_hist_plot(grad_loss, title = 'default'):
    fig = plt.figure(figsize = (10,5))
    gs = GridSpec(1, 2)

    pde_grad_first = torch.cat([tensor.view(-1) for tensor in grad_loss['pde'][0]])
    bc_grad_first = torch.cat([tensor.view(-1) for tensor in grad_loss['bc'][0]])

    pde_grad_last = torch.cat([tensor.view(-1) for tensor in grad_loss['pde'][-1]])
    bc_grad_last = torch.cat([tensor.view(-1) for tensor in grad_loss['bc'][-1]])


    hist_helper(gs[0,0], bc_values= bc_grad_first.cpu().numpy(), pde_values=pde_grad_first.cpu().numpy(), title = '$First\ Epoch$')
    hist_helper(gs[0,1], bc_values= bc_grad_last.cpu().numpy(), pde_values=pde_grad_last.cpu().numpy(), title = '$Last\ Epoch$')
    
    plt.savefig('./plots/grad_'+title+'.jpg', dpi = 300)


def grad_hist_plot_log(grad_loss, title = 'default'):
    fig = plt.figure(figsize = (10,5))
    gs = GridSpec(1, 2)

    pde_grad_first = torch.cat([tensor.view(-1) for tensor in grad_loss['pde'][0]])
    bc_grad_first = torch.cat([tensor.view(-1) for tensor in grad_loss['bc'][0]])

    pde_grad_last = torch.cat([tensor.view(-1) for tensor in grad_loss['pde'][-1]])
    bc_grad_last = torch.cat([tensor.view(-1) for tensor in grad_loss['bc'][-1]])


    histlog_helper(gs[0,0], bc_values= bc_grad_first.cpu().numpy(), pde_values=pde_grad_first.cpu().numpy(), title = '$First\ Epoch$')
    histlog_helper(gs[0,1], bc_values= bc_grad_last.cpu().numpy(), pde_values=pde_grad_last.cpu().numpy(), title = '$Last\ Epoch$')
    
    plt.savefig('./plots/gradlog_'+title+'.jpg', dpi = 300)



def f_plot(F_active , title):
    zz = []
    for Z in (F_active):
        zz.append(Z.detach().cpu().numpy())
    #zz = trained_features

    MIN = 100
    for z in zz:
        if np.min(z) <= MIN:
            MIN = np.min(z)
    MAX = 0
    for z in zz:
        if np.max(z) >= MAX:
            MAX = np.max(z)
    print(min , max)
    n_f = 4

    fig, ax = plt.subplots(n_f,2, figsize = (3*2 , n_f*2))
    cm = plt.cm.bwr

    #for j , z in enumerate(zz[0]):
    for j in range(2):
        for i in range(n_f): 
            p = ax[i,j].pcolormesh(zz[0][j,i,:,:], cmap=cm, vmin = MIN , vmax = MAX)
            ax[i,j].set_title(f'Feature{i}, channel{j}')
            ax[i,j].set_box_aspect(1)
            plt.colorbar(p,ax=ax[i,j])

    plt.tight_layout()
    #fig.suptitle('First resolution' , fontsize=20)
    plt.savefig('./plots/features_'+title+'.png' , dpi = 300)


def compare_plot_h(xp_mesh, yp_mesh, sol, sol_p, ntest = 100, title = 'default'):
    u = sol[:,0].cpu().numpy()
    up = sol_p[:,0].reshape(-1,1).detach().cpu().numpy()
    print(up.shape)
    
    subplot_size = 3.
    (width, height) = (3*subplot_size, 2*subplot_size) #5 col and 3 rows, each of size subplot_size
    fig = plt.figure(figsize = (width, height))
    gs = GridSpec(1, 3)
    #self.contour_helper(gs[0, 0], self.xp_mesh, self.yp_mesh, self.psip_mesh, '$\psi$')
    xp_mesh = xp_mesh.detach().cpu().numpy()
    yp_mesh = yp_mesh.detach().cpu().numpy()
    contour_helper(gs[0, 0], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,), 0,0, '$u$')
    contour_helper(gs[0, 1], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), up.reshape(-1,), 0,0,'$u_p$')
    contour_helper(gs[0, 2], xp_mesh.reshape(-1,), yp_mesh.reshape(-1,), u.reshape(-1,) - up.reshape(-1,), 0,0,'$Error$')

    print("____________________________________________________________________________________________________________")
    print(f"Relative MSE is {(np.sum((u.reshape(-1,) - up.reshape(-1,))**2.0))/(np.sum((u.reshape(-1,))**2.0))}")
    print("____________________________________________________________________________________________________________")


        # Open a file with access mode 'a'
       
    with open( f'./log/history_{title}.txt', 'w') as file_object:
        # Append 'hello' at the end of file
        file_object.write('\n'+f"Relative MSE is {(np.sum((u.reshape(-1,) - up.reshape(-1,))**2.0))/(np.sum((u.reshape(-1,))**2.0))}")

    
    plt.tight_layout()
    plt.savefig('./plots/predictions/jpgs/'+title+ '_compare.jpg', dpi = 300)
    #plt.show()
    plt.figure(figsize = (8,6))
    _ = plt.plot(u.reshape(-1,), up.reshape(-1,))
    _ = plt.plot([u.reshape(-1,).min(), u.reshape(-1,).max()], [u.reshape(-1,).min(), u.reshape(-1,).max()], c= 'r')
    plt.xlabel('True Value', fontsize = 14)
    plt.ylabel('Predicted Value', fontsize = 14)
    plt.savefig('./plots/predictions/jpgs/'+title+'_y_vs_yp.jpg', dpi = 300)





if __name__ == '__main__':
    
    from network import Network
    from model import Model
    from Data import get_data_spatial_Taylor_green
    from utils.encoder import Encoder

    # paramters
    seed = 1234
    features = 4
    n_cells = 2
    res = [3, 7, 11, 23]
    domain_size = [0.0, 2.0 * torch.pi, 0.0, 2.0 * torch.pi]
    data_params = {'N_f': 50000, 'N_bc':50000, 't':1.0, 'nu':0.01, 'domain_size':domain_size, 'ntest': 100, 'seed': seed}
    title = 'only_plot_update_bc'
    net = Network(input_dim= len(res) * features)
    # make encoder
    encoder = Encoder(n_features= features, res = res, n_cells= n_cells, domain_size= domain_size)
    # make model
    model = Model(network=net, encoder= encoder)
    #pinn.load_state_dict(torch.load(f"./best_sofar.pt"))
    model.load_state_dict(torch.load(f"./Saved_Models/trained_Taylor_MR_n_cell_2_epochs_1000_update_bc.pt"))
    model.eval()

    x, y, u_test = \
    get_data_spatial_Taylor_green(params=data_params, train=False)

    u_predict = model(x, y)


    #plotting
    compare_plot(x, y, u_test, u_predict, title=title)

