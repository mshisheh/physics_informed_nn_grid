import torch
import torch.nn as nn
import numpy as np
from utils.encoder import *

class Network(nn.Module):
    def __init__(self, input_dim = 2, output_dim = 2, layers = [16, 16], activation = 'tanh', encoder = 'cosine') -> None:
        super(Network, self).__init__()
        activation_list = {'tanh':nn.Tanh(), 'Silu':nn.SiLU(), 'Sigmoid':nn.Sigmoid()}
        activation = activation_list[activation]
        l = nn.ModuleList()
        l.append(nn.Linear(input_dim, layers[0]))
        l.append(activation)
        for i in range(0,len(layers)-1):
            l.append(nn.Linear(layers[i], layers[i+1]))
            l.append(activation )
        l.append(nn.Linear(layers[-1], output_dim, bias=False))
        self.layers = nn.Sequential(*l).to(device)

    def forward(self, input):
        out = self.layers[0](input)
        for layer in self.layers[1:]:
            out = layer(out)
        return out
    

class Conv(nn.Module):
    def __init__(self, input_channel = 8, out_channel = 32) -> None:
        super().__init__()
        self.conv2d_1 = nn.Conv2d(input_channel= input_channel, out_channel = out_channel, kernel_size=3)
        self.rel = torch.nn.ReLU()
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.flat = nn.Flatten()


    def forward(self, input):
        pass    

if __name__ == '__main__':
    net = Network()
    print(net.layers)