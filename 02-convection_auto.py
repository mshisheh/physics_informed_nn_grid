import torch
import os
import numpy as np
import time

from utils.initialize import initialize
from utils.Data import get_convection
from utils.network import Network , AE
from utils.encoder0 import Encoder
from utils.encoder_F import Encoder_f
from utils.model import Model , Model_ae
from utils.losses import Loss_Functions
from utils.trainer3 import Trainer
from utils.plot import compare_plot_h, loss_plot, der_plot, grad_hist_plot , f_plot , w_plot

#import inspect
#import utils.Data

from datetime import date

today = date.today()



# Textual month, day and year	
d2 = today.strftime("%B%d")


#---------------------------------------------
# paramters
seed = 456
features = 4
n_cells = 3#[11, 3]
res = [11]#[ 3, 4, 5, 7, 9, 11 , 13 , 15 , 17, 19 , 21]
pde_name = 'convection' # or 
network_name = 'MR' # or MR
domain_size = [0.,2*np.pi,0.,1.]
data_params = {'N_f': 10000, 'N_bc':10000, 
               'domain_size':domain_size,'ntest': 1000, 'seed': seed, 
               'Neuman_bc': False,  'beta':10.0 , 'nu':0.}
epoch_test = 200
epochs = 200
optimizer_test = 'adam'
optimizer = 'lbfgs'
dynamic_weight_flag = False
hbc_flag = False
evo = False

ae = False
#------------------------------------------------------------------------------
if network_name == 'PINN':
    title = f"{d2}_{pde_name}_{network_name}_{epochs}_{optimizer}_{data_params['N_f']}_dw_{dynamic_weight_flag}"
else:
    title = f"{d2}_{pde_name}_{network_name}_{epochs}_{optimizer}_{data_params['N_f']}_{res}_features_{features}_cells_{n_cells}_dw_{dynamic_weight_flag}_ae{ae}_evo{evo}_beta{data_params['beta']}"
data_func = get_convection
#------------------------------------------------------------------------------

# initialize
dtype, device = initialize(seed = data_params['seed'])




if ae:

    for r in res:
        encoder = Encoder(n_features= features, res = [r], n_cells= n_cells, domain_size= domain_size , mode = 'cosine')

        autoencoder = AE(input_dim= features , layers=[16 , 16 , 16], output_dim=1)
        model = Model_ae(encoder=encoder , autoencoder=autoencoder , data_params=data_params , hbc = False)
        # make loss_functions
        loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse' , ae=True)

        trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                bc_loss_function = loss_functions.ic_bc, 
                epochs=epoch_test, title=title, data_params = data_params, 
                data_func = data_func, update_data = False, use_data=True, 
                optimizer=optimizer_test, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False , evo = False , active_res= r)
# start fitting:
        trainer()


    last_val = []
    for r in res:
        a = np.loadtxt(f'./Text/val_loss_{title}_active_res{r}.txt')
        
        last_val.append((r ,a[-1]))
    #tuples = [(2500, 'Hadoop'), (2200, 'Spark'), (3000, 'Python')]
    last_val.sort(key=lambda x: x[1])
    sorted_ = last_val[:1]
    final_res = []
    
    for tup in sorted_:
        final_res.append(tup[0])
    print(final_res)
    title = f"{d2}_{pde_name}_{network_name}_{epochs}_{optimizer}_{data_params['N_f']}_{final_res}_features_{features}_cells_{n_cells}_dw_{dynamic_weight_flag}_ae{ae}_evo{evo}_hbc{hbc_flag}_beta{data_params['beta']}_data"


    encoder = Encoder(n_features= features, res = final_res, n_cells= n_cells, domain_size= domain_size , mode = 'cosine')
    autoencoder = AE(input_dim= features*len(final_res) , layers=[16 , 16 , 16], output_dim=1)
    model = Model_ae(encoder=encoder , autoencoder=autoencoder , data_params = data_params , hbc = hbc_flag)
    # make loss_functions
    loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse' , ae=True)
    trainer2 = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                bc_loss_function = loss_functions.ic_bc, 
                epochs=epochs, title=title, data_params = data_params, 
                data_func = data_func, update_data = False, use_data=True, 
                optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False , evo = evo , active_res= final_res)
    trainer2()
    







    # make trainer, 

else:
    for r in res:
        encoder = Encoder(n_features= features, res = [r], n_cells= n_cells, domain_size= domain_size , mode = 'cosine')
        network = Network(input_dim= features , layers=[16, 16 , 16], output_dim=1)
        model = Model(encoder=encoder , network=network , data_params=data_params , hbc = False)
        # make loss_functions
        loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse' , ae=ae)

        trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                bc_loss_function = loss_functions.ic_bc, 
                epochs=epoch_test, title=title, data_params = data_params, 
                data_func = data_func, update_data = False, use_data=True, 
                optimizer=optimizer_test, loss_functions = loss_functions, dynamic_weights=False, RAR=False , evo = evo , active_res= r)
# start fitting:
        trainer()


    last_val = []
    for r in res:
        a = np.loadtxt(f'./Text/val_loss_{title}_active_res{r}.txt')
        
        last_val.append((r ,a[-1]))
    #tuples = [(2500, 'Hadoop'), (2200, 'Spark'), (3000, 'Python')]
    last_val.sort(key=lambda x: x[1])
    sorted_ = last_val[:3]
    final_res = []
    for tup in sorted_:
        final_res.append(tup[0])
    print(final_res)
    title = f"{d2}_{pde_name}_{network_name}_{epochs}_{optimizer}_{data_params['N_f']}_{final_res}_features_{features}_cells_{n_cells}_dw_{dynamic_weight_flag}_ae{ae}_evo{evo}_hbc{hbc_flag}_beta{data_params['beta']}"

    print(f"title before encoder:    {title}")
    encoder = Encoder(n_features= features, res = final_res, n_cells= n_cells, domain_size= domain_size , mode = 'cosine')
    network = Network(input_dim= len(final_res)*features , layers=[16, 16 , 16], output_dim=1)
    model = Model(encoder=encoder , network=network , data_params=data_params , hbc = False)
    # make loss_functions
    loss_functions = Loss_Functions(model=model, name = pde_name, params=data_params, loss_type='mse' , ae=ae)
    trainer2 = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                bc_loss_function = loss_functions.ic_bc, 
                epochs=epochs, title=title, data_params = data_params, 
                data_func = data_func, update_data = False, use_data=True, 
                optimizer=optimizer, loss_functions = loss_functions, dynamic_weights=False, RAR=False , evo = evo , active_res= final_res)
    
    trainer2()
    #
    feature_map = [(f).clone().detach() for f in encoder.F_active]

    #print(f"encoder_f:    {feature_map}")


    enc_f =  Encoder_f(n_features= features, res = final_res, n_cells= n_cells, domain_size= domain_size ,F=feature_map, mode = 'cosine')
    network = Network(input_dim= len(final_res)*features , layers=[32, 32 , 32], output_dim=1)
    model = Model(encoder=enc_f , network=network , data_params=data_params , hbc = False)
    trainer3 = Trainer(model=model, pde_loss_function=loss_functions.pde_loss, 
                bc_loss_function = loss_functions.ic_bc, lr = 1e-1,
                epochs= 300, title=title, data_params = data_params, 
                data_func = data_func, update_data = False, use_data=False, 
                optimizer='lbfgs', loss_functions = loss_functions, dynamic_weights=dynamic_weight_flag, RAR=False , evo = evo , active_res= final_res)
    
    trainer3()
    


# testing
# Generate data
x, y, u_test = \
    data_func(params=data_params, train=False)
print(x.shape , y.shape , u_test.shape)

if ae:
    if hbc_flag:
     _ ,  _ , u_predict  = model(x, y)
    else:  u_predict ,  _ , _  = model(x, y)
else:
    if hbc_flag:
        _ , u_predict = model(x, y)
    else:
        u_predict , _ = model(x, y)





#plotting
compare_plot_h(x, y, u_test, u_predict, title=title)
time.sleep(3.0)
loss_plot(trainer3.train_losses, trainer3.val_losses, title=title)



w_plot(trainer3.w,title=title)
time.sleep(3.0)
#grad_hist_plot(trainer.gradient_loss_total, title = title)

f_plot(encoder.F_active , title = title)



#der_plot(x, y , model, title=title)