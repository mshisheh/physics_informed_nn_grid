import torch
import os
import numpy as np
import time

from utils.initialize import initialize
from utils.Data import get_data_spatial_lid_cavity
from utils.network import Network
from utils.encoder import Encoder
from utils.model import Model
from utils.losses import Loss_Functions
from utils.trainer import Trainer
from utils.plot import compare_plot, loss_plot

#---------------------------------------------
# paramters
seed = 2456
features = 4
res = [3, 4, 5]
n_cells = 2
domain_size = [-1,1,0,2]
data_params = {'N_f': 50000, 'N_bc':10000, 'Re':50, 'domain_size':domain_size, 'ntest': 100, 'seed': seed}
epochs = 250
optimizer = 'lbfgs'
title = f"cavity_MR_{epochs}_{optimizer}_{data_params['N_f']}_{res}_features_{features}_Re_{data_params['Re']}"
data_func = get_data_spatial_lid_cavity
#---------------------------------------------

# initialize
dtype, device = initialize(seed = data_params['seed'])

# make network
net = Network(input_dim= len(res) * features, output_dim=5)
# make encoder
encoder = Encoder(n_features= features, res = res, n_cells= n_cells, domain_size= domain_size)
# make models
model = Model(network=net, encoder= encoder)
# make loss_functions
loss_functions = Loss_Functions(model=model, name = 'NS', params=data_params, loss_type='logcosh')
# make trainer, 
trainer = Trainer(model=model, pde_loss_function=loss_functions.pde_loss_derivitives_input, 
                  bc_loss_function = loss_functions.bc_loss_one_p, 
                  epochs=epochs, title=title, data_params = data_params, 
                  data_func = data_func, update_data = True, use_data=False, 
                  optimizer= optimizer, loss_functions = loss_functions, dynamic_weights=False, device=device, RAR=False)
# start fitting:
trainer()

# testing
# Generate data
# testing
# Generate data
x, y, u_test = \
    get_data_spatial_lid_cavity(params=data_params, train=False)

u_predict = model(x, y)


#plotting
compare_plot(x, y, u_test, u_predict, title=title)
time.sleep(3.0)
loss_plot(trainer.train_losses, trainer.val_losses, title=title)