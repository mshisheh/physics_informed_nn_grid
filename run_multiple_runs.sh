#!/bin/bash

# Set up the environment
conda activate your-environment

# Array of parameter values
pde_names=("Helmholtz" "Example2" "Example3")
optim_methods=("lbfgs" "adam", "mix")

rm submit*
rm error*
rm output*

conda init bash
conda activate myenv


# Loop over parameter values
for a2 in {1..10}; do
  for optim in "${optim_methods[@]}"; do
      # Create a submit file for each case
      submit_file="submit_${pde_name}_${optim}_${a2}.sh"
      echo "#!/bin/bash" > "$submit_file"
      echo "#SBATCH --job-name=Helmholtz" >> "$submit_file"
      echo "#SBATCH -A raminb_lab_gpu" >> "$submit_file"
      echo "#SBATCH -p free-gpu" >> "$submit_file"
      echo "#SBATCH -N 1" >> "$submit_file"
      echo "#SBATCH --gres=gpu:V100:1" >> "$submit_file"
      echo "#SBATCH -t 03:00:00" >> "$submit_file"
      echo "#SBATCH --output=output_${pde_name}_${optim}_${a2}.log" >> "$submit_file"
      echo "#SBATCH --error=error_${pde_name}_${optim}_${a2}.log" >> "$submit_file"
      echo "" >> "$submit_file"
      echo "python main.py --model MR --pde-name Helmholtz --optim \"$optim\" --epochs 100 --a2 \"$a2\"" >> "$submit_file"

      # Submit the job using sbatch
      sbatch "$submit_file"

      # Wait for a few seconds before submitting the next job (optional)
      sleep 3
  done
done

